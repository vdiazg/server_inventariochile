<?php
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UserNameUsuario = $_SESSION["datos_usuario_logueado"][10];

$id = filter_input(INPUT_GET, 'id');

if (!isset($IDUsuario)) {
    header('Location: login.php');
}

require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="dashboard.php"> Home</a></li>
                        <li class="active">Añadir Sucursal</li>	 
                    </ul>
                </div>
                <form class="form-login paddingTB-md" action="" id="JSV_ModificarSucursal" method="POST">
                    <div class="panel panel-default table-responsive ">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-4">
                                    <i class="fa fa-plus-circle"></i> EDITAR INFORMACIÓN DE SURCURSAL
                                </div>
                                <div class="col-md-8">
                                    <div id="msj_respuesta"></div>
                                </div>
                            </div>
                        </div>
                        <div class="padding-md clearfix">
                            <!-- INICIO Código dentro cuadro blanco -->
                            <div class="panel-body">
                                <?php
                                $SQL_DATA_SELECT_SUCURSALES = $db->query("SELECT * FROM Sucursales WHERE sucursal_id = '$id'");
                                foreach ($SQL_DATA_SELECT_SUCURSALES as $row):
                                    $SUCURSAL_ID_BD = $row["sucursal_id"];
                                    $SUCURSAL_NOMBRE = $row["sucursal_nombre"];
                                    $SUCURSAL_REF = $row["sucursal_ref"];
                                    $SUCURSAL_DIRECCION = $row["sucursal_direccion"];
                                    $SUCURSAL_TEL = $row["sucursal_tel"];
                                endforeach;
                                ?>
                                <div class="row">
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="socur_ref">Referencia</label><input id="Update_socur_ref" name="Update_socur_ref" value='<?php echo $SUCURSAL_REF ?>' type="text" placeholder="" class="form-control input-sm"></div></div>
                                    <input id="id" name="id" value="<?php echo $SUCURSAL_ID_BD ?>" class="hidden">
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="socur_nombre">Nombre</label><input id="Update_socur_nombre" name="Update_socur_nombre" value='<?php echo $SUCURSAL_NOMBRE ?>' type="text" placeholder="" class="form-control input-sm"></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="socur_direccion">Dirección</label><input id="Update_socur_direccion" name="Update_socur_direccion" value='<?php echo $SUCURSAL_DIRECCION ?>' type="text" placeholder="" class="form-control input-sm"></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="socur_tel">Teléfono de Contacto</label><input id="Update_socur_tel" name="Update_socur_tel" type="text" value='<?php echo $SUCURSAL_TEL ?>' placeholder="" class="form-control input-sm"></div></div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Encargado de Sucursal</label>
                                            <select class="form-control input-sm" id="Update_socur_encargado" name="Update_socur_encargado">
                                                <option value="">Seleccione</option>
                                                <?php
                                                $DATA_USUARIOS_SQL = $db->query("SELECT user_id, user_nombre, user_ap FROM Usuarios");
                                                foreach ($DATA_USUARIOS_SQL as $row):
                                                    echo "<option value=" . $row["user_id"] . ">" . $row["user_nombre"] . ' ' . $row["user_ap"] . "</option>";
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Seleccione la empresa</label>
                                            <select class="form-control input-sm" id="Update_selecc_empresa" name="Update_selecc_empresa">
                                                <option value="">Seleccione</option>
                                                <?php
                                                $DATA_EMPRESAS_SQL = $db->query("SELECT empresa_id, empresa_razon_social FROM Empresas");
                                                foreach ($DATA_EMPRESAS_SQL as $row):
                                                    echo "<option value=" . $row["empresa_id"] . ">" . $row["empresa_razon_social"] . "</option>";
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <!-- BOTON GUARDAR -->
                                <div class="row">                                        
                                    <div class="col-md-12">
                                        <button type="button" id="modificar_sucursales_boton_cancel" class="btn btn-default pull-left"><i class="fa fa-times fa-2x"></i> <br/>Cancelar</button>
                                        <button type="submit" class="btn btn-default pull-right"><i class="fa fa-save fa-2x"></i> <br/>Guardar</button>
                                    </div>
                                </div>
                                <!-- FIN BOTON GUARDAR -->
                            </div>
                            <!-- FIN  Código dentro cuadro blanco -->
                        </div>
                    </div><!-- /panel -->
                    <!-- FIN INFORMACION PERSONAL -->
                </form>
            </div>
        </div>

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <?php require_once './includes/footer.inc.php'; ?>

        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!--Bootstrap-->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src="js/js-nfconnection/jquery.validate.min.js"></script>
        <script src="js/js-nfconnection/custom_jquery_validate.js"></script>

        <script src="js/menu-active-class.js"></script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Bootstrap Switch -->
        <!--<script src='js/js-nfconnection/bootstrap-switch.js'></script>-->

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

        <script> //Script que al presionar "cancelar" devuelve a la pagina anterior
            $("#modificar_sucursales_boton_cancel").click(function () {
                window.location.replace("./sucursales.php");
            });
        </script>

    </body>
</html>