<?php
require_once '../controlador/Db.class.php';
$db = new Db();

$emp_id = trim(filter_input(INPUT_POST, 'emp_id'));
$emp_rut = trim(filter_input(INPUT_POST, 'emp_rut'));
$emp_razon = trim(filter_input(INPUT_POST, 'emp_razon'));
$emp_giro = trim(filter_input(INPUT_POST, 'emp_giro'));
$emp_direccion = trim(filter_input(INPUT_POST, 'emp_direccion'));
$emp_tel = trim(filter_input(INPUT_POST, 'emp_tel'));
$emp_fijo = trim(filter_input(INPUT_POST, 'emp_fijo'));

//Para eliminar los puntos y comas
if ($emp_rut){ $emp_rut = str_replace(array('.', ','), '', $emp_rut); }
    
//Se verifica que tenga el formato adecuado ej: 12345678-9
if (!$emp_rut || !preg_match("/\b\d{7,8}\-[K|k|0-9]/", $emp_rut)) { echo "3"; return; }

//Se verifica que tenga un largo entre 8 y 9 digitos
if (!$emp_fijo || !preg_match("/^[0-9]{8,9}$/", $emp_fijo)) { echo "4"; return; }

//Se realiza una validacion extra en el lado del servidor
if (!$emp_razon || !$emp_giro || !$emp_direccion) { echo "5"; return; }

if ($db->single("SELECT count(*) FROM Empresas WHERE empresa_rut = '$emp_rut' ") != 0) {
    $sql = "UPDATE Empresas SET "
            . "empresa_razon_social = " . "'" . $emp_razon . "'" . ", "
            . "empresa_giro = " . "'" . $emp_giro . "'" . ", "
            . "empresa_direccion = " . "'" . $emp_direccion . "'" . ", "
            . "empresa_tel_movil = " . "'" . $emp_tel . "'" . ", "
            . "empresa_tel_fijo = " . "'" . $emp_fijo . "'" . " "
            . "WHERE empresa_rut = " . "'" . $emp_rut . "'" . "";

    if($db->query($sql)){
        //SQL Ejecutada Exitosamente
        echo "1";
        return;
    }else{
        //Error en Ejecutar SQL
        echo "2";
        return;
    }
    
} else {
    echo "0";
    return;
}
$db->CloseConnection();
?>