 <?php
// Tiempo de espera del script
// Este lo usamos para emular mas o menos el comportamiento en un servidor web no local
// Ya que muchas veces al ejecutarlo de fomra local no se aprecia bien el funcionamiento.
sleep(3);


// ini_set("display_errors", 1);
// Definimos variables generales

define("maxUpload", 600000); 
define("maxWidth", 5000);
define("maxHeight", 5000);
define("uploadURL", '../img/avatars_users/');
define("fileName", 'foto_');


// Tipos MIME
$fileType = array('image/jpeg', 'image/pjpeg', 'image/png', 'image/jpg');

// Bandera para procesar imagen
$pasaImgSize = false;

//bandera de error al procesar la imagen
$respuestaFile = false;

// nombre por default de la imagen a subir
$fileName = '';
// error del lado del servidor
$mensajeFile = 'ERROR EN EL SCRIPT';

// Obtenemos los datos del archivo
$tamanio = $_FILES['userfile']['size'];
$tipo = $_FILES['userfile']['type'];
$archivo = $_FILES['userfile']['name'];

// Tamaño de la imagen
$imageSize = getimagesize($_FILES['userfile']['tmp_name']);

// Verificamos la extensión del archivo independiente del tipo mime
$extension = explode('.', $_FILES['userfile']['name']);
$num = count($extension) - 1;


// Creamos el nombre del archivo dependiendo la opción
$imgFile = fileName . time() . '.' . $extension[$num];

// Verificamos el tamaño válido para los logotipos
if ($imageSize[0] <= maxWidth && $imageSize[1] <= maxHeight){
    $pasaImgSize = true;
}
// Verificamos el status de las dimensiones de la imagen a publicar
if ($pasaImgSize == true) {
    // Verificamos Tamaño y extensiones
    if (in_array($tipo, $fileType) && $tamanio > 0 && $tamanio <= maxUpload && ($extension[$num] == 'jpg' || $extension[$num] == 'png' || $extension[$num] == 'jpeg')) {
        
        // Intentamos copiar el archivo
        if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
            if (move_uploaded_file($_FILES['userfile']['tmp_name'], uploadURL . $imgFile)) {
                //Ruta de la original
                $rtOriginal = uploadURL . $imgFile;

                //Crear variable de imagen a partir de la original
                if($extension[$num] == 'jpg'){
                    $original = imagecreatefromjpeg($rtOriginal);
                }
                if($extension[$num] == 'png'){
                    $original = imagecreatefrompng($rtOriginal); 
                }
                if($extension[$num] == 'jpeg'){
                    $original = imagecreatefromjpeg($rtOriginal); 
                }
                //Definir tamaño máximo y mínimo
                $ancho_final = 150;
                $alto_final = 150;

                //Recoger ancho y alto de la original
                list($ancho, $alto) = getimagesize($rtOriginal);

                $lienzo = imagecreatetruecolor($ancho_final, $alto_final); 

                //Copiar $original sobre la imagen que acabamos de crear en blanco ($tmp)
                imagecopyresampled($lienzo, $original,0,0,0,0, $ancho_final, $alto_final, $ancho,$alto);

                //Limpiar memoria
                imagedestroy($original);

                //Definimos la calidad de la imagen final
                $cal = 80;

                //Se crea la imagen final en el directorio indicado
                imagejpeg($lienzo, uploadURL . $imgFile, $cal);
                
                $respuestaFile = 'done';
                $fileName = $imgFile;
                $mensajeFile = $imgFile;
                
            } else
            // error del lado del servidor
                $mensajeFile = 'No se pudo subir el archivo';
        } else
        // error del lado del servidor
            $mensajeFile = 'No se pudo subir el archivo';
    } else
    // Error en el tamaño y tipo de imagen
        $mensajeFile = 'Verifique el tamaño y tipo de imagen';
} else
// Error en las dimensiones de la imagen
    $mensajeFile = '( Tamaño Máximo 512px x 512px ) - Verifique las dimensiones de la Imagen';

$salidaJson = array("respuesta" => $respuestaFile, "mensaje" => $mensajeFile, "fileName" => $fileName);

echo json_encode($salidaJson);
?>