<?php
require_once '../controlador/Db.class.php';
$db = new Db();

$socur_ref = trim(filter_input(INPUT_POST, 'Update_socur_ref'));
$socur_nombre = trim(filter_input(INPUT_POST, 'Update_socur_nombre'));
$socur_direccion = trim(filter_input(INPUT_POST, 'Update_socur_direccion'));
$socur_tel = trim(filter_input(INPUT_POST, 'Update_socur_tel'));
$IDencargado = trim(filter_input(INPUT_POST, 'Update_socur_encargado'));
$IDempresa = trim(filter_input(INPUT_POST, 'Update_selecc_empresa'));
$id = trim(filter_input(INPUT_POST, 'id'));

/* Se verifica que su tamaño sea de al menos 3 caracteres */
if (!$socur_nombre || !preg_match("/^[a-zA-Z0-9]+$/", $socur_nombre) || strlen($socur_nombre) < 3) { echo "3"; return; }

//Se realiza una validacion extra en el lado del servidor
if (!$socur_ref || !$socur_direccion) { echo "5"; return; }

if ($db->single("SELECT count(*) FROM Sucursales WHERE sucursal_id = '$id'") != 0) {
    $sql = 'UPDATE `Sucursales` SET '
            . '`sucursal_nombre`= "'.$socur_nombre.'",'
            . '`sucursal_ref`= "'.$socur_ref.'",'
            . '`sucursal_direccion`= "'.$socur_direccion.'",'
            . '`sucursal_tel`= "'.$socur_tel.'",'
            . '`Usuarios_user_id`= "'.$IDencargado.'",'
            . '`Paises_pais_id`= "1",'
            . '`Empresas_empresa_id`= "'.$IDempresa.'" '
            . 'WHERE sucursal_id = "'.$id.'" ';
    
    $sql = $db->query($sql);
    
    if($sql){
        //SQL Ejecutada Exitosamente
        echo "1";
    }else{
        //Error en Ejecutar SQL
        echo "2";
    }
}else{
    echo '0';
}

?>