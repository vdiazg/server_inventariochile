<?php

require_once '../controlador/Db.class.php';
$db = new Db();
$SUBTOTAL = trim(filter_input(INPUT_POST, 'SUBTOTAL'));
$IVA = trim(filter_input(INPUT_POST, 'IVA'));
$TOTAL = trim(filter_input(INPUT_POST, 'TOTAL'));
$BARCODE = trim(filter_input(INPUT_POST, 'BARCODE'));
$IDUSER = trim(filter_input(INPUT_POST, 'IDUSER'));
$IDSUCURSAL = trim(filter_input(INPUT_POST, 'IDSUCURSAL'));
$NUM_ORDEN = trim(filter_input(INPUT_POST, 'NUM_ORDEN'));
$FECHA_HORA = trim(filter_input(INPUT_POST, 'FECHA_HORA'));
// Unescape the string values in the JSON array
$tableData = stripcslashes($_POST['pTableData']);

// Decode the JSON array
$tableData = json_decode($tableData, TRUE);

$sql = "INSERT INTO `Orden_Despacho`("
        . "`orden_despacho_num`, "
        . "`orden_despacho_fecha_hora`, "
        . "`orden_despacho_subtotal`, "
        . "`orden_despacho_iva`, "
        . "`orden_despacho_total`, "
        . "`Sucursales_sucursal_id`, "
        . "`Usuarios_user_id`) VALUES ("
        . "'" . $NUM_ORDEN . "',"
        . "'" . $FECHA_HORA . "',"
        . "'" . $SUBTOTAL . "',"
        . "'" . $IVA . "',"
        . "'" . $TOTAL . "',"
        . "'" . $IDSUCURSAL . "',"
        . "'" . $IDUSER . "')";

$sql = $db->query($sql);
$lastInsertId = $db->lastInsertId($sql);
if ($sql) {
/*
    echo "<br>**********************************<br>";
    echo "Subtotal: " . $SUBTOTAL . '<br>';
    echo "IVA: " . $IVA . '<br>';
    echo "TOTAL: " . $TOTAL . '<br>';
    echo "IDUSER: " . $IDUSER . '<br>';
    echo "IDSUCURSAL: " . $IDSUCURSAL . '<br>';
    echo "NUM_ORDEN: " . $NUM_ORDEN . '<br>';
    echo "FECHA_HORA: " . $FECHA_HORA . '<br>';*/
    foreach ($tableData as $trend) {
        //SQL Ejecutada Exitosamente
        $sql_orden_detalle = "INSERT INTO `Orden_Detalle`("
                . "`orden_detalle_product_nombre`, "
                . "`orden_detalle_product_ean13`, "
                . "`orden_detalle_product_cantidad`, "
                . "`orden_detalle_product_precio`, "
                . "`Orden_Despacho_orden_despacho_id`) VALUES ("
                . "'" . $trend['item'] . "',"
                . "'" . $trend['ean13'] . "',"
                . "'" . $trend['cantidad'] . "',"
                . "'" . substr($trend['precio'], 1) . "',"
                . "'" . $lastInsertId . "')";
        $sql_orden_detalle = $db->query($sql_orden_detalle);
        if ($sql_orden_detalle) {
            echo "Registro Exitoso Orden Detalle";
            /*
            echo "----------------------------------<br>";
            echo "EAN13: " . $trend['ean13'] . "<br>";
            echo "Producto: " . $trend['item'] . "<br>";
            echo "Precio: " . $trend['precio'] . "<br>";
            echo "Cantidad: " . $trend['cantidad'] . "<br>";
            echo "Total: " . $trend['total'] . "<br>";*/
        } else {
            //Error en Ejecutar SQL
            echo "Error en el Registro Orden Detalle";
        }
    }
} else {
    //Error en Ejecutar SQL
    echo "Error en el Registro Orden Despacho";
}
/*
*/
