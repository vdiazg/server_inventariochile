<?php

header('Content-Type: text/html; charset=UTF-8');
require_once '../controlador/Db.class.php';
$db = new Db();

$Username_Registro = trim(filter_input(INPUT_POST, 'username_registro'));
$Password_Registro = trim(filter_input(INPUT_POST, 'password_registro'));
$Perfil_Registro = "Cliente";

$keyword = trim(filter_input(INPUT_POST, 'keyword'));
$num_serie_tag = trim(filter_input(INPUT_POST, 'num_serie_tag'));

$user_nombre = trim(filter_input(INPUT_POST, 'user_nombre'));
$user_segundo_nombre = trim(filter_input(INPUT_POST, 'user_segundo_nombre'));
$user_apell_paterno = trim(filter_input(INPUT_POST, 'user_ap'));
$user_apell_materno = trim(filter_input(INPUT_POST, 'user_am'));
$user_run = trim(filter_input(INPUT_POST, 'user_run'));
$user_fecha_nacimiento = trim(filter_input(INPUT_POST, 'user_fecha_nacimiento'));
$select_genero_user = trim(filter_input(INPUT_POST, 'select_genero_user'));

$input_enfermedad_1 = trim(filter_input(INPUT_POST, 'input_enfermedad_1'));
$descrip_enfermedad_1 = trim(filter_input(INPUT_POST, 'descrip_enfermedad_1'));

$input_enfermedad_2 = trim(filter_input(INPUT_POST, 'input_enfermedad_2'));
$descrip_enfermedad_2 = trim(filter_input(INPUT_POST, 'descrip_enfermedad_2'));

$input_enfermedad_3 = trim(filter_input(INPUT_POST, 'input_enfermedad_3'));
$descrip_enfermedad_3 = trim(filter_input(INPUT_POST, 'descrip_enfermedad_3'));

$input_enfermedad_4 = trim(filter_input(INPUT_POST, 'input_enfermedad_4'));
$descrip_enfermedad_4 = trim(filter_input(INPUT_POST, 'descrip_enfermedad_4'));

$user_email_1 = trim(filter_input(INPUT_POST, 'user_email_1'));
$user_email_2 = trim(filter_input(INPUT_POST, 'user_email_2'));

$user_tel_movil_1 = trim(filter_input(INPUT_POST, 'user_tel_movil_1'));
$user_tel_movil_2 = trim(filter_input(INPUT_POST, 'user_tel_movil_2'));

$Patch_Img = trim(filter_input(INPUT_POST, 'src_img'));

$user_nota = trim(filter_input(INPUT_POST, 'user_nota'));
$select_grup_sanguineo_user = trim(filter_input(INPUT_POST, 'select_grup_sanguineo_user'));

if ($db->single("SELECT count(*) FROM Usuarios WHERE user_nombre = '$user_nombre' AND user_ap = '$user_apell_paterno' AND user_am = '$user_apell_materno'") == 0) {
    if ($db->single("SELECT count(*) FROM Usuarios WHERE user_rut = '$user_run'") == 0) {
        
        // INSERT DE USUARIOS 
        $STRING_SQL_USUARIOS = "INSERT INTO `Usuarios`"
                . "(`user_nombre`, "
                . "`user_segundo_nombre`, "
                . "`user_ap`, "
                . "`user_am`, "
                . "`user_rut`, "
                . "`user_fecha_nacimiento`, "
                . "`user_genero`, "
                . "`user_ruta_img`,"
                . "`user_grupo_sanguineo`, "
                . "`user_nota`,"
                . "`yourls_url_keyword`) VALUES ("
                . "'" . trim(ucwords(strtolower($user_nombre))) . "',"
                . "'" . trim(ucwords(strtolower($user_segundo_nombre))) . "',"
                . "'" . trim(ucwords(strtolower($user_apell_paterno))) . "',"
                . "'" . trim(ucwords(strtolower($user_apell_materno))) . "',"
                . "'" . trim($user_run) . "',"
                . "'" . trim($user_fecha_nacimiento) . "',"
                . "'" . $select_genero_user . "',"
                . "'" . trim($Patch_Img) . "',"
                . "'" . $select_grup_sanguineo_user . "',"
                . "'" . $user_nota . "',"
                . "'" . $keyword . "')";
        
        $INSERT_SQL_USUARIOS = $db->query($STRING_SQL_USUARIOS);
        $lastInsertId = $db->lastInsertId($INSERT_SQL_USUARIOS);
        session_start();
        $_SESSION['lastInsertIdUser'] = $lastInsertId;
        // INSERT DE ENFERMEDADES
        if (!empty($input_enfermedad_1)) {
            $STRING_SQL_ENFERMEDAD_1 = "INSERT INTO `Enfermedades`("
                    . "`enfermedad_nombre`, "
                    . "`enfermedad_tratamiento`, "
                    . "`Usuarios_user_id`) VALUES ("
                    . "'" . trim(strtoupper($input_enfermedad_1)) . "',"
                    . "'" . $descrip_enfermedad_1 . "',"
                    . "'" . $lastInsertId . "')";
            $INSERT_SQL_ENFERMEDAD_1 = $db->query($STRING_SQL_ENFERMEDAD_1);
        }
        if (!empty($input_enfermedad_2)) {
            $STRING_SQL_ENFERMEDAD_2 = "INSERT INTO `Enfermedades`("
                    . "`enfermedad_nombre`, "
                    . "`enfermedad_tratamiento`, "
                    . "`Usuarios_user_id`) VALUES ("
                    . "'" . trim(strtoupper($input_enfermedad_2)) . "',"
                    . "'" . $descrip_enfermedad_2 . "',"
                    . "'" . $lastInsertId . "')";
            $INSERT_SQL_ENFERMEDAD_2 = $db->query($STRING_SQL_ENFERMEDAD_2);
        }
        if (!empty($input_enfermedad_3)) {
            $STRING_SQL_ENFERMEDAD_3 = "INSERT INTO `Enfermedades`("
                    . "`enfermedad_nombre`, "
                    . "`enfermedad_tratamiento`, "
                    . "`Usuarios_user_id`) VALUES ("
                    . "'" . trim(strtoupper($input_enfermedad_3)) . "',"
                    . "'" . $descrip_enfermedad_3 . "',"
                    . "'" . $lastInsertId . "')";
            $INSERT_SQL_ENFERMEDAD_3 = $db->query($STRING_SQL_ENFERMEDAD_3);
        }
        if (!empty($input_enfermedad_4)) {
            $STRING_SQL_ENFERMEDAD_4 = "INSERT INTO `Enfermedades`("
                    . "`enfermedad_nombre`, "
                    . "`enfermedad_tratamiento`, "
                    . "`Usuarios_user_id`) VALUES ("
                    . "'" . trim(strtoupper($input_enfermedad_4)) . "',"
                    . "'" . $descrip_enfermedad_4 . "',"
                    . "'" . $lastInsertId . "')";
            $INSERT_SQL_ENFERMEDAD_4 = $db->query($STRING_SQL_ENFERMEDAD_4);
        }

        // INSERT DE CORREOS ELECTRONICOS
        if (!empty($user_email_1)) {
            $STRING_SQL_EMAIL_1 = "INSERT INTO `CorreosElectronicos`("
                    . "`correo_electronico_nombre`, "
                    . "`Usuarios_user_id`) VALUES ("
                    . "'" . trim(strtolower($user_email_1)) . "',"
                    . "'" . $lastInsertId . "')";
            $INSERT_SQL_EMAIL_1 = $db->query($STRING_SQL_EMAIL_1);
        }
        if (!empty($user_email_2)) {
            $STRING_SQL_EMAIL_2 = "INSERT INTO `CorreosElectronicos`("
                    . "`correo_electronico_nombre`, "
                    . "`Usuarios_user_id`) VALUES ("
                    . "'" . trim(strtolower($user_email_2)) . "',"
                    . "'" . $lastInsertId . "')";
            $INSERT_SQL_EMAIL_2 = $db->query($STRING_SQL_EMAIL_2);
        }

        // INSERT DE TELEFONOS
        if (!empty($user_tel_movil_1)) {
            $STRING_SQL_TELEFONO_1 = "INSERT INTO `NumerosTelefonos`("
                    . "`numero_telefono_num`, "
                    . "`Usuarios_user_id`) VALUES ("
                    . "'" . trim(strtolower($user_tel_movil_1)) . "',"
                    . "'" . $lastInsertId . "')";
            $INSERT_SQL_TELEFONO_1 = $db->query($STRING_SQL_TELEFONO_1);
        }
        if (!empty($user_tel_movil_2)) {
            $STRING_SQL_TELEFONO_2 = "INSERT INTO `NumerosTelefonos`("
                    . "`numero_telefono_num`, "
                    . "`Usuarios_user_id`) VALUES ("
                    . "'" . trim(strtolower($user_tel_movil_2)) . "',"
                    . "'" . $lastInsertId . "')";
            $INSERT_SQL_TELEFONO_2 = $db->query($STRING_SQL_TELEFONO_2);
        }
        
        $STRING_UPDATE_STATUS_KEY = "UPDATE `Keys` SET  `key_status` =  'true' WHERE  `yourls_url_keyword` = '" . $keyword . "'";
        $db->query($STRING_UPDATE_STATUS_KEY);
        
        $STRING_UPDATE_STATUS_TAG = "UPDATE `Tag` SET  `tag_estado` =  'Habilitado' WHERE  `yourls_url_keyword` = '" . $keyword . "'";
        $db->query($STRING_UPDATE_STATUS_TAG);
        
        $STRING_SQL_LOGIN = "INSERT INTO `Login`(`login_username`, `login_password`, `Usuarios_user_id`) VALUES ('$Username_Registro','".  md5($Password_Registro)."','$lastInsertId')";
        $db->query($STRING_SQL_LOGIN);
        
        $STRING_SQL_PERFIL = "INSERT INTO `Perfiles`(`perfil_nombre`, `perfil_descripcion`, `Usuarios_user_id`) VALUES ('$Perfil_Registro','','$lastInsertId')";
        $db->query($STRING_SQL_PERFIL);
        
        if ($INSERT_SQL_USUARIOS == true) {
            /***************************************************************************************************************************************/
            //ENVIO DE USUARIO Y CONTRASEÑA AL CORREO DEL USUARIO
            $headers = "From: NFConnection <idband@nfconnection.cl>\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $subject = '[IDBand] Activación Correctamente';
            
            
            $email = $Username_Registro;
            //date_default_timezone_set("America/Santiago");
            //$fecha = date("d/m/y - H:i:s");

            /* ---------------------------------------------------------------------- */
            //Seccion que envia correo de respuesta satisfactoria a la persona que se contacto.
            $to = $email;
            $message_email = '<!DOCTYPE html>';
            $message_email .= '<html><body>';
            $message_email .= '<table border="0" style="background-color: #eeebe3;" Width=100%>
                                    <thead>                                
                                    </thead>
                                    <tbody>
                                        <tr>            
                                            <td style="text-align: center;"><img style="width: 50%;" src="http://nfconnection.cl/assets/img/nfconnection/logo03.png"/></td>
                                            <td>
                                                <h1>Estimado(a), </h1>
                                                <h2>' . $user_nombre . ' ' . $user_segundo_nombre . ' ' . $user_apell_paterno . ' ' . $user_apell_materno . ' </h2>
                                                <h2>Sus credenciales de Acceso para modificar su información son las siguientes: </h2>
                                                <p>USUARIO: '.$Username_Registro.'</p>
                                                <p>CONTRASEÑA: '.$Password_Registro.'</p>
                                                <a href="http://idband.nfconnection.cl/login_mod.php">Modificar Información</a>
                                                <p style="vertical-align: text-bottom;"><a href="http://nfconnection.cl/">www.nfconnection.cl</a> - <b>EFICIENCIA</b> Y <b>SEGURIDAD</b> PARA TU <b>INFORMACIÓN</b></p>
                                            </td>
                                        </tr>
                                    </tbody>
                                    </table>';
            $message_email .= '</body></html>';
            if(mail($to, $subject, $message_email, $headers)){
                echo '1';
            }else{
                
            }
            /***************************************************************************************************************************************/                      
        } else {
            echo '0';
        }
    } else {
        echo '2';
        //echo '<div class="alert alert-danger"><strong>Ya existe este RUT de Usuario</strong> You successfully read this important alert message.</div>';
    }
} else {
    //echo '3';
    echo '<div class="alert alert-danger"><strong>Ya existe el Usuario: ' . $user_nombre . ' ' . $user_apell_paterno . ' ' . $user_apell_materno . ' </strong></div>';
}
$db->CloseConnection();