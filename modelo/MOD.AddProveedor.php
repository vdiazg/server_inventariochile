<?php

require_once '../controlador/Db.class.php';
$db = new Db();


$provee_razon_social = trim(filter_input(INPUT_POST, 'provee_razon_social'));
$provee_rut = trim(filter_input(INPUT_POST, 'provee_rut'));
$provee_giro = trim(filter_input(INPUT_POST, 'provee_giro'));
$provee_direccion = trim(filter_input(INPUT_POST, 'provee_direccion'));
$provee_tel_fijo = trim(filter_input(INPUT_POST, 'provee_tel_fijo'));
$provee_tel_movil = trim(filter_input(INPUT_POST, 'provee_tel_movil'));



if($provee_rut) $provee_rut = str_replace(array('.', ','), '' , $provee_rut); //Para eliminar los puntos y comas

//Se verifica que el rut exista
//Se verifica que tenga el formato adecuado ej: 12345678-9
if(!$provee_rut || !preg_match("/\b\d{7,8}\-[K|k|0-9]/" , $provee_rut) ){
	echo "3";
	return;
}

//Se verifica que el numero exista
//Se verifica que tenga un largo entre 8 y 9 digitos
if(!$provee_tel_fijo || !preg_match("/^[0-9]{8,9}$/" , $provee_tel_fijo) ){
	echo "4";
	return;
}

//Se verifica que el numero exista
//Se verifica que tenga un largo entre 8 y 9 digitos
if(!$provee_tel_movil || !preg_match("/^[0-9]{8,9}$/" , $provee_tel_movil) ){
	echo "4";
	return;
}

if( //Se realiza una validacion extra en el lado del servidor
	!$provee_razon_social ||
	!$provee_giro ||
	!$provee_direccion
){
	echo "5";
	return;
}

if ($db->single("SELECT count(*) FROM proveedores WHERE proveedor_rut = '$provee_rut' ") == 0) {
    $sql = "INSERT INTO `proveedores`(`proveedor_razon_social`, `proveedor_rut`, `proveedor_giro`, `proveedor_direccion`,
            `proveedor_tel_fijo`, `proveedor_tel_movil`) VALUES ("
                    . "'" . $provee_razon_social . "',"
                    . "'" . $provee_rut  . "',"
                    . "'" . $provee_giro . "',"
                    . "'" . $provee_direccion . "',"
                    . "'" . $provee_tel_fijo . "',"
                    . "'" . $provee_tel_movil. "')"; 
					
	$sql = $db->query($sql);
	$lastInsertId = $db->lastInsertId($sql);
	echo "1";
	return;
}else{
    echo "0";
	return;
}
      

?>

