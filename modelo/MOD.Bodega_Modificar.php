<?php

require_once '../controlador/Db.class.php';
$db = new Db();

$bod_ref = trim(filter_input(INPUT_POST, 'bod_ref'));
$bod_nom = trim(filter_input(INPUT_POST, 'bod_nom'));
$bod_admin = trim(filter_input(INPUT_POST, 'bod_admin'));
$bod_localizacion = trim(filter_input(INPUT_POST, 'bod_localizacion'));
$bod_num_tel_movil = trim(filter_input(INPUT_POST, 'bod_num_tel_movil'));
$bod_num_tel_fijo = trim(filter_input(INPUT_POST, 'bod_num_tel_fijo'));
$bod_direccion = trim(filter_input(INPUT_POST, 'bod_direccion'));
$bod_direccion2 = trim(filter_input(INPUT_POST, 'bod_direccion2'));
$bod_cod_postal = trim(filter_input(INPUT_POST, 'bod_cod_postal'));
$bod_sucursal = trim(filter_input(INPUT_POST, 'bod_sucursal'));
$bod_id = trim(filter_input(INPUT_POST, 'bod_id'));

if ($db->single("SELECT count(*) FROM Bodegas WHERE bodega_id = '$bod_id'") != 0) {
    $sql = 'UPDATE `Bodegas` SET '
            . '`bodega_nombre`="' . $bod_nom . '",'
            . '`bodega_ref`="' . $bod_ref . '",'
            . '`bodega_telefono`="' . $bod_num_tel_fijo . '",'
            . '`bodega_telefono_movil`="' . $bod_num_tel_movil . '",'
            . '`bodega_direccion`="' . $bod_direccion . '",'
            . '`bodega_direccion2`="' . $bod_direccion2 . '",'
            . '`bodega_codigopostal`="' . $bod_cod_postal . '",'
            . '`Paises_pais_id`="1",'
            . '`Sucursales_sucursal_id`="' . $bod_sucursal . '",'
            . '`Usuarios_user_id`="' . $bod_admin . '" '
            . 'WHERE bodega_id = "' . $bod_id . '"';

// insertar la sentencia en la bd .-.
    $sql = $db->query($sql);
    if($sql){
        //SQL Ejecutada Exitosamente
        echo "1";
        return;
    }else{
        //Error en Ejecutar SQL
        echo "2";
        return;
    }
} else {
    echo "0";
}
?>