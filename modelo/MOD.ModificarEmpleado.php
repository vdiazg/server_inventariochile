<?php

require_once '../controlador/Db.class.php';
$db = new Db();

$emple_run = trim(filter_input(INPUT_POST, 'emple_run'));
$emple_nombre = trim(filter_input(INPUT_POST, 'emple_nombre'));
$emple_segundo_nombre = trim(filter_input(INPUT_POST, 'emple_segundo_nombre'));
$emple_ap = trim(filter_input(INPUT_POST, 'emple_ap'));
$emple_am = trim(filter_input(INPUT_POST, 'emple_am'));
$emple_genero = trim(filter_input(INPUT_POST, 'emple_genero'));
$emple_fecha_nacimiento = trim(filter_input(INPUT_POST, 'emple_fecha_nacimiento'));
$emple_perfil = trim(filter_input(INPUT_POST, 'emple_perfil'));
$emple_nota = trim(filter_input(INPUT_POST, 'emple_nota'));
$emple_ruta_img = trim(filter_input(INPUT_POST, 'src_img'));

$emple_username = trim(filter_input(INPUT_POST, 'emple_username'));
$emple_contrasena = trim(filter_input(INPUT_POST, 'emple_contrasena'));

//Para eliminar los puntos y comas
if ($emple_run) {
    $emple_run = str_replace(array('.', ','), '', $emple_run);
}

if (!$emple_run || !preg_match("/\b\d{7,8}\-[K|k|0-9]/", $emple_run)) {
    echo "4";
    return;
}
$Usuario_ID = $db->single("SELECT user_id FROM Usuarios WHERE user_rut = '" . $emple_run . "'");
if ($db->single("SELECT count(*) FROM Usuarios WHERE user_id = '" . $Usuario_ID . "'") != 0) {
    $sql = "UPDATE `Usuarios` SET "
            . "`user_nombre`='" . $emple_nombre . "',"
            . "`user_segundo_nombre`='" . $emple_segundo_nombre . "',"
            . "`user_am`='" . $emple_am . "',"
            . "`user_ap`='" . $emple_ap . "',"
            . "`user_genero`='" . $emple_genero . "',"
            . "`user_fecha_nacimiento`='" . $emple_fecha_nacimiento . "',"
            . "`user_ruta_img`='" . $emple_ruta_img . "',"
            . "`user_nota`='" . $emple_nota . "',"
            . "`Perfiles_perfil_nombre`='" . $emple_perfil . "' "
            . "WHERE user_id = '" . $Usuario_ID . "'";

    
// insertar la sentencia en la bd .-.
    $sql = $db->query($sql);

    $STRING_SQL_LOGIN = "UPDATE `Login` SET "
    . "`login_username`='".$emple_username."',"
    . "`login_password`='".$emple_contrasena."' "
    . "WHERE Usuarios_user_id = '".$Usuario_ID."'";
    $db->query($STRING_SQL_LOGIN);
    echo "1";
    return;
} else {
    echo "0";
    return;
}
?>

