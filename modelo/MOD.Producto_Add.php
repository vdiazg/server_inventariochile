<?php

require_once '../controlador/Db.class.php';
$db = new Db();

$product_nom = trim(filter_input(INPUT_POST, 'product_nom'));
$product_marca = trim(filter_input(INPUT_POST, 'product_marca'));
$product_cod_ean13 = trim(filter_input(INPUT_POST, 'product_cod_ean13'));
$product_cod_JAN = trim(filter_input(INPUT_POST, 'product_cod_JAN'));
$product_cod_upc = trim(filter_input(INPUT_POST, 'product_cod_upc'));
$product_cod_ref = trim(filter_input(INPUT_POST, 'product_cod_ref'));
$product_descrip_corta = trim(filter_input(INPUT_POST, 'product_descrip_corta'));
$product_descrip_larga = trim(filter_input(INPUT_POST, 'product_descrip_larga'));
$product_imagen_ref = trim(filter_input(INPUT_POST, 'src_img'));
$product_status = trim(filter_input(INPUT_POST, 'product_status'));
//falta uno en la vista
$product_catego = trim(filter_input(INPUT_POST, 'product_catego'));
$product_bodega = trim(filter_input(INPUT_POST, 'product_bodega'));
$product_precio = trim(filter_input(INPUT_POST, 'product_precio'));
$product_cantidad = trim(filter_input(INPUT_POST, 'product_cantidad'));

if ($db->single("SELECT count(*) FROM Productos WHERE producto_nombre = '$product_nom'") == 0) {
    $sql = "INSERT INTO `Productos`(`producto_nombre`, `producto_marca`, `producto_ean13`, `producto_cod_jan`, `producto_cod_upc`, `producto_cod_ref`, `producto_status`, `producto_descrip_corta`, `producto_descrip_larga`, `producto_precio`, `producto_ruta_img`, `producto_cantidad`, `Bodegas_bodega_id`, `ProductCategorias_productcategorias_id`) VALUES ("
            . "'" . $product_nom . "',"
            . "'" . $product_marca . "',"
            . "'" . $product_cod_ean13 . "',"
            . "'" . $product_cod_JAN . "',"
            . "'" . $product_cod_upc . "',"
            . "'" . $product_cod_ref . "',"
            . "'" . $product_status . "',"
            . "'" . $product_descrip_corta . "',"
            . "'" . $product_descrip_larga . "',"
            . "'" . $product_precio . "', "
            . "'" . $product_imagen_ref . "', "
            . "'" . $product_cantidad . "',"
            . "'" . $product_bodega . "',"
            . "'" . $product_catego . "')";

    $sql = $db->query($sql);
    if ($sql) {
        //SQL Ejecutada Exitosamente
        echo "1";
        return;
    } else {
        //Error en Ejecutar SQL
        echo "2";
        return;
    }
} else {
    echo "0";
}
$db->CloseConnection();
?>