<?php

require_once '../controlador/Db.class.php';
$db = new Db();

$product_nom = trim(filter_input(INPUT_POST, 'product_nom'));
$product_marca = trim(filter_input(INPUT_POST, 'product_marca'));
$product_cod_ean13 = trim(filter_input(INPUT_POST, 'product_cod_ean13'));
$product_cod_JAN = trim(filter_input(INPUT_POST, 'product_cod_JAN'));
$product_cod_upc = trim(filter_input(INPUT_POST, 'product_cod_upc'));
$product_cod_ref = trim(filter_input(INPUT_POST, 'product_cod_ref'));
$product_descrip_corta = trim(filter_input(INPUT_POST, 'product_descrip_corta'));
$product_descrip_larga = trim(filter_input(INPUT_POST, 'product_descrip_larga'));
$product_imagen_ref = trim(filter_input(INPUT_POST, 'src_img'));
$product_status = trim(filter_input(INPUT_POST, 'product_status'));
//falta uno en la vista
$product_catego = trim(filter_input(INPUT_POST, 'product_catego'));
$product_bodega = trim(filter_input(INPUT_POST, 'product_bodega'));
$product_precio = trim(filter_input(INPUT_POST, 'product_precio'));
$product_cantidad = trim(filter_input(INPUT_POST, 'product_cantidad'));
$id = trim(filter_input(INPUT_POST, 'id'));

if ($db->single("SELECT count(*) FROM Productos WHERE producto_id = '" . $id . "'") == 1) {

    $sql = "UPDATE `Productos` SET "
            . "`producto_nombre`='" . $product_nom . "',"
            . "`producto_marca`='" . $product_marca . "',"
            . "`producto_ean13`='" . $product_cod_ean13 . "',"
            . "`producto_cod_jan`='" . $product_cod_JAN . "',"
            . "`producto_cod_upc`='" . $product_cod_upc . "',"
            . "`producto_cod_ref`='" . $product_cod_ref . "',"
            . "`producto_status`='" . $product_status . "',"
            . "`producto_descrip_corta`='" . $product_descrip_corta . "',"
            . "`producto_descrip_larga`='" . $product_descrip_larga . "',"
            . "`producto_precio`='" . $product_precio . "',"
            . "`producto_ruta_img`='" . $product_imagen_ref . "',"
            . "`producto_cantidad`='" . $product_cantidad . "',"
            . "`Bodegas_bodega_id`='" . $product_bodega . "',"
            . "`ProductCategorias_productcategorias_id`='" . $product_catego . "' "
            . " WHERE producto_id = '" . $id . "'";


// insertar la sentencia en la bd .-.
    $sql = $db->query($sql);
    if ($sql) {
        //SQL Ejecutada Exitosamente
        echo "1";
        return;
    } else {
        //Error en Ejecutar SQL
        echo "2";
        return;
    }
} else {
    echo "0";
    return;
}
$db->CloseConnection();
?>