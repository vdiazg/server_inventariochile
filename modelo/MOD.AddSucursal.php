<?php
require_once '../controlador/Db.class.php';
$db = new Db();

$socur_ref = trim(filter_input(INPUT_POST, 'socur_ref'));
$socur_nombre = trim(filter_input(INPUT_POST, 'socur_nombre'));
$socur_direccion = trim(filter_input(INPUT_POST, 'socur_direccion'));
$socur_tel = trim(filter_input(INPUT_POST, 'socur_tel'));
$IDencargado = trim(filter_input(INPUT_POST, 'socur_encargado'));
$IDempresa = trim(filter_input(INPUT_POST, 'selecc_empresa'));

/* Se verifica que su tamaño sea de al menos 3 caracteres */
if (!$socur_nombre || !preg_match("/^[a-zA-Z0-9]+$/", $socur_nombre) || strlen($socur_nombre) < 3) { echo "3"; return; }

//Se realiza una validacion extra en el lado del servidor
if (!$socur_ref || !$socur_direccion) { echo "5"; return; }

if ($db->single("SELECT count(*) FROM Sucursales WHERE sucursal_nombre = '$socur_nombre' ") == 0) {
    $sql = "INSERT INTO `Sucursales`(`sucursal_nombre`, `sucursal_ref`, `sucursal_direccion`, `sucursal_tel`, `Usuarios_user_id`, `Paises_pais_id`, `Empresas_empresa_id`) VALUES ("
            . "'" . $socur_nombre . "',"
            . "'" . $socur_ref . "',"
            . "'" . $socur_direccion . "',"
            . "'" . $socur_tel . "',"
            . "'" . $IDencargado . "',"
            . "'1',"
            . "'" . $IDempresa."')";
    
    $sql = $db->query($sql);
    if($sql){
        //SQL Ejecutada Exitosamente
        echo "1";
        return;
    }else{
        //Error en Ejecutar SQL
        echo "2";
        return;
    }
} else {
    echo "0";
}
?>