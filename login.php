<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body>
        <div class="login-wrapper" style="top: 10px;">
            <div class="text-center">
                <!--<img class="" style="max-width: 16%;" src="./img/logo_inventory2.jpg" alt="User Avatar">    -->
                <img class="bounceIn" style="max-width: 16%;" src="./img/your-logo-here.png" alt="User Avatar">
            </div>
            <div class="login-widget animation-delay1">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <i class="fa fa-lock fa-lg"></i> <span style="color:#f89406; text-shadow:0 1px #ffffff;"> ACCESO AL INVENTARIO</span>
                    </div>
                    <div class="panel-body">
                        <form class="form-login" name="form_login_usuario" id="form_login_usuario" method="POST" action="">
                            <div class="form-group">
                                <label class="control-label">Correo Electrónico</label>
                                <input type="text" name="username" id="username" placeholder="Nombre de Usuario" class="form-control input-sm " >
                            </div>
                            <div class="form-group">
                                <label class="control-label">Contraseña</label>
                                <input type="password" name="password" id="password" placeholder="Contraseña" class="form-control input-sm ">
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-default btn-lg login-link btn-block">Entrar <i class="fa fa-arrow-right"></i></button></div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="msj_respuesta_login_usuario"></div>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col-md-4 col-xs-4 text-right">
                                    <img class="" style="max-width: 50%;" src="./img/logo03.png" alt="User Avatar">
                                </div>
                                <div class="col-md-8 col-xs-8 text-left" style="vertical-align: middle;">
                                    <p>© 2015 <span class="xirodregular">NF</span>Connection.<br/>Todos los Derechos Reservados.</p>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
                
            </div><!-- /login-widget -->
        </div><!-- /login-wrapper -->

        <!-- Le javascript ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src="js/js-nfconnection/jquery.validate.min.js"></script>
        <script src="js/js-nfconnection/custom_jquery_validate.js"></script>        
    </body>
</html>
