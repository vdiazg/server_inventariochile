<?php
error_reporting(error_reporting() & ~E_NOTICE);
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UsernameUsuario = $_SESSION["datos_usuario_logueado"][10];

if (!isset($IDUsuario)) {
    header('Location: login.php');
}
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php //require_once './includes/menubar.inc.php'; ?>
            <div id="main-container" style="margin-left: 0px;">
                <div class="padding-md">
                    <div class="panel panel-default">
                        <form class="form-horizontal no-margin form-border" id="formWizard1" novalidate>
                            <div class="panel-heading">
                                Form Wizard
                            </div>
                            <div class="panel-tab">
                                <ul class="wizard-steps wizard-demo" id="wizardDemo1"> 
                                    <li class="active">
                                        <a href="#wizardContent1" data-toggle="tab">Step 1</a>
                                    </li> 
                                    <li>
                                        <a href="#wizardContent2" data-toggle="tab">Step 2</a>
                                    </li> 
                                    <li>
                                        <a href="#wizardContent3" data-toggle="tab">Step 3</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="wizardContent1">
                                        <div class="form-group">
                                            <label class="control-label col-lg-2">RUT</label>
                                            <div class="col-lg-6">
                                                <input type="text" placeholder="" class="form-control input-sm" data-required="true">
                                            </div><!-- /.col -->
                                        </div><!-- /form-group -->
                                        <div class="form-group">
                                            <label class="control-label col-lg-2">Razon Social</label>
                                            <div class="col-lg-6">
                                                <input type="text" placeholder="" class="form-control input-sm" data-required="true">
                                            </div><!-- /.col -->
                                        </div><!-- /form-group -->                                        
                                        <div class="form-group">
                                            <label class="control-label col-lg-2">Giro</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control input-sm" placeholder="" data-required="true" >
                                            </div><!-- /.col -->
                                        </div><!-- /form-group -->		
                                        <div class="form-group">
                                            <label class="control-label col-lg-2">Dirección</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control input-sm" placeholder="" data-required="true" >
                                            </div><!-- /.col -->
                                        </div><!-- /form-group -->		
                                        <div class="form-group">
                                            <label class="control-label col-lg-2">Teléfono Móvil</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control input-sm" placeholder="" data-required="true" >
                                            </div><!-- /.col -->
                                        </div><!-- /form-group -->		
                                        <div class="form-group">
                                            <label class="control-label col-lg-2">Teléfonno Fijo</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control input-sm" placeholder="" data-required="true" >
                                            </div><!-- /.col -->
                                        </div><!-- /form-group -->		
                                    </div>
                                    <div class="tab-pane fade" id="wizardContent2">
                                        <div class="form-group">
                                            <label class="control-label col-lg-2">Phone</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control input-sm" placeholder="(XXX) XXXX XXX" data-required="true" data-type="phone">
                                            </div><!-- /.col -->
                                        </div><!-- /form-group -->		
                                        <div class="form-group">
                                            <label class="control-label col-lg-2">Website</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control input-sm" placeholder="Website url" data-required="true" data-type="urlstrict">
                                            </div><!-- /.col -->
                                        </div><!-- /form-group -->
                                    </div>
                                    <div class="tab-pane fade padding-md" id="wizardContent3">
                                        <h4>Finish!</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer clearfix">
                                <div class="pull-left">
                                    <button class="btn btn-success btn-sm disabled" id="prevStep1" disabled>Anterior</button>
                                    <button type="submit" class="btn btn-sm btn-success" id="nextStep1">Siguiente</button>
                                </div>

                                <div class="pull-right" style="width:30%">
                                    <div class="progress progress-striped active m-top-sm m-bottom-none">
                                        <div class="progress-bar progress-bar-success" id="wizardProgress" style="width:33%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- /panel -->
                </div>
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <?php require_once './includes/footer.inc.php'; ?>
        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>
        <!-- Eliminar Tag confirmation -->
        <?php require_once './includes/eliminar_tag_confim.inc.php'; ?>


        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <!-- Parsley -->
        <script src="js/parsley.min.js"></script>

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless_wizard.js"></script>
        <script src="js/endless/endless.js"></script>

    </body>
</html>


