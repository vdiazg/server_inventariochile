<?php
error_reporting(error_reporting() & ~E_NOTICE);
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UsernameUsuario = $_SESSION["datos_usuario_logueado"][10];
$id = filter_input(INPUT_GET, 'id');


if (!isset($IDUsuario)) {
    header('Location: login.php');
}
require_once './controlador/Db.class.php';
$db = new Db();
$SQL_DATA_SELECT_ORDEN_Despacho = $db->query("SELECT * FROM Orden_Despacho WHERE orden_despacho_id = '" . $id . "'");

foreach ($SQL_DATA_SELECT_ORDEN_Despacho as $row):

    $ORDEN_ID = $row['orden_despacho_id'];
    $ORDEN_NUMERO = $row['orden_despacho_num'];
    $ORDEN_FECHA_HORA = $row['orden_despacho_fecha_hora'];
    $ORDEN_SUBTOTAL = number_format($row['orden_despacho_subtotal'], 0, ',', '.');
    $ORDEN_IVA = number_format($row['orden_despacho_iva'], 0, ',', '.');
    $ORDEN_TOTAL = number_format($row['orden_despacho_total'], 0, ',', '.');

    $ORDEN_FK_IDSUCURSAL = $row['Sucursales_sucursal_id'];
    $NOMBRE_SUCURSAL = $db->single("SELECT sucursal_nombre FROM Sucursales WHERE sucursal_id = '" . $ORDEN_FK_IDSUCURSAL . "'");

    $ORDEN_FK_IDUSUARIO = $row['Usuarios_user_id'];
    $NOMBRE_USUARIO_nombre = $db->single("SELECT user_nombre FROM Usuarios WHERE user_id = '" . $ORDEN_FK_IDUSUARIO . "'");
    $NOMBRE_USUARIO_ap = $db->single("SELECT user_ap FROM Usuarios WHERE user_id = '" . $ORDEN_FK_IDUSUARIO . "'");

endforeach;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="dashboard.php"> Home</a></li>
                        <li class="active">Productos</li>	 
                    </ul>
                </div><!-- breadcrumb -->
                <div class="padding-md" id="dvContainer">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Detalle de la Orden N°: <span class="badge m-left-xs"><?php echo $ORDEN_NUMERO; ?></span><br>
                            Emitida por: <span class="badge m-left-xs"><?php echo $NOMBRE_USUARIO_nombre . ' ' . $NOMBRE_USUARIO_ap; ?></span><br>
                            Dirigida a Sucursal: <span class="badge m-left-xs"><?php echo $NOMBRE_SUCURSAL; ?></span><br>
                            Fecha y Hora de emision: <span class="badge m-left-xs"><?php echo $ORDEN_FECHA_HORA; ?></span><br>
                        </div>
                        <div class="panel-body pull-right">

                        </div>
                        <table class="table table-bordered table-condensed table-hover table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">EAN13</th>
                                    <th class="text-center">Item</th>
                                    <th class="text-center">Cantidad</th>
                                    <th class="text-center">Precio</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $SQL_DATA_SELECt = $db->query("SELECT * FROM Orden_Detalle WHERE Orden_Despacho_orden_despacho_id = '" . $id . "'");

                                foreach ($SQL_DATA_SELECt as $row):

                                    $ORDEN_DETALLE_PRODUCTO_NOMBRE = $row['orden_detalle_product_nombre'];
                                    $ORDEN_DETALLE_PRODUCTO_EAN13 = $row['orden_detalle_product_ean13'];
                                    $ORDEN_DETALLE_PRODUCTO_CANTIDAD = $row['orden_detalle_product_cantidad'];
                                    $ORDEN_DETALLE_PRODUCTO_PRECIO = $row['orden_detalle_product_precio'];

                                    echo '<tr>';
                                    echo '<td class="text-center">' . $ORDEN_DETALLE_PRODUCTO_EAN13 . '</td>';
                                    echo '<td class="text-center">' . $ORDEN_DETALLE_PRODUCTO_NOMBRE . '</td>';
                                    echo '<td class="text-center">' . $ORDEN_DETALLE_PRODUCTO_CANTIDAD . '</td>';
                                    echo '<td class="text-center">$' . number_format($ORDEN_DETALLE_PRODUCTO_PRECIO, 0, ',', '.') . '</td>';
                                    echo '</tr>';

                                endforeach;
                                ?>
                            </tbody>
                        </table>
                        <div class="panel-body pull-right">
                            <span>Subtotal: </span>$ <?php echo $ORDEN_SUBTOTAL; ?><br>
                            <span>IVA(19%): </span>$ <?php echo $ORDEN_IVA; ?><br>
                            <span>Total: </span>$ <?php echo $ORDEN_TOTAL; ?><br>
                        </div>

                    </div>
                </div>

                <div class="row ">
                    <div class="col-md-12">
                        <hr>
                        <a href="VIEW.OrdenDespacho.Lista.php" class="btn btn-default quick-btn pull-left margin-sm"><i class="fa fa-reply"></i><span>Volver</span></a>
                        <a id="btnPrint" class="btn btn-default quick-btn pull-right margin-sm"><i class="fa fa-print"></i><span>Imprimir</span></a>
                    </div>
                </div>
            </div><!-- /main-container -->

        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>
        <!-- Eliminar Tag confirmation -->
        <?php require_once './includes/eliminar_tag_confim.inc.php'; ?>


        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src='js/jquery.dataTables.min.js'></script>
        <script type="text/javascript">
            $(document).ready(function () {
                /* IMPRESION */
                $("#btnPrint").click(function () {
                    var divContents = $("#dvContainer").html();
                    var printWindow = window.open('', 'my div', 'height=400,width=600');
                    printWindow.document.write('<html><head><title>DIV Contents</title>');
                    printWindow.document.write('<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">');
                    printWindow.document.write('</head><body>');
                    printWindow.document.write(divContents);
                    printWindow.document.write('</body></html>');
                    printWindow.print();
                    printWindow.close();
                });
                /* IMPRESION */
                $('#dataTable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "asStripClasses": null
                });
                $('#refrescar_pagina').click(function () {
                    location.reload();
                });
                $('#a_btn_genera_pdf').on('click', function (e) {
                    window.open('./exportar_pdf/pdf_keys_tag.php', '_blank');
                });
                $('#a_btn_genera_excel').on('click', function (e) {
                    window.open('./exportar_excel/excel_keys_tags.php', '_blank');
                });
            });
        </script>
        <script src="js/menu-active-class.js"></script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Bootstrap Switch -->
        <script src='js/js-nfconnection/bootstrap-switch.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

    </body>
</html>