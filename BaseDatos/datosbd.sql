-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 16-08-2016 a las 09:51:04
-- Versión del servidor: 5.5.46
-- Versión de PHP: 5.4.45-0+deb7u2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `inventario_cafes`
--

--
-- Volcado de datos para la tabla `Bodegas`
--

INSERT INTO `Bodegas` (`bodega_id`, `bodega_nombre`, `bodega_ref`, `bodega_telefono`, `bodega_telefono_movil`, `bodega_direccion`, `bodega_direccion2`, `bodega_codigopostal`, `Sucursales_sucursal_id`, `Usuarios_user_id`) VALUES
(1, 'BODEGA DE VILLA ALEMANA', 'BO VILLA', '', '+56 9 9491 989', 'Jupiter #64', NULL, NULL, 0, 0);

--
-- Volcado de datos para la tabla `Empleados`
--

INSERT INTO `Empleados` (`empleado_id`, `empleado_cargo`, `Sucursales_sucursal_id`, `Usuarios_user_id`) VALUES
(1, 'Jefe Sucursal', 1, 1);

--
-- Volcado de datos para la tabla `Empresas`
--

INSERT INTO `Empresas` (`empresa_id`, `empresa_rut`, `empresa_razon_social`, `empresa_giro`, `empresa_direccion`, `empresa_tel_movil`, `empresa_tel_fijo`) VALUES
(1, '76541729-5', 'DIAZ NAVARRO Y CIA LIMITADA', 'Desarrollo de Software y Hardw', 'Jupiter #64', '+56977740488', NULL);

--
-- Volcado de datos para la tabla `Login`
--

INSERT INTO `Login` (`login_id`, `login_username`, `login_password`, `Usuarios_user_id`) VALUES
(2, 'demo@demo.cl', 'fe01ce2a7fbac8fafaed7c982a04e229', 1);

--
-- Volcado de datos para la tabla `Perfiles`
--

INSERT INTO `Perfiles` (`perfil_nombre`, `perfil_descripcion`) VALUES
('ADMINISTRADOR', NULL),
('SUPERADMIN', NULL);

--
-- Volcado de datos para la tabla `Sucursales`
--

INSERT INTO `Sucursales` (`sucursal_id`, `sucursal_nombre`, `sucursal_ref`, `sucursal_direccion`, `sucursal_tel`, `sucursal_encargado`, `Paises_pais_id`, `Empresas_empresa_id`) VALUES
(1, 'NFConnection', 'SUNFCVIÑA', 'Arlegui #1175, Local 5','', '+56994919896','',1, 1);

INSERT INTO  `nfconnec_inventariochile`.`Usuarios` (

`user_id` ,
`user_nombre` ,
`user_segundo_nombre` ,
`user_am` ,
`user_ap` ,
`user_rut` ,
`user_genero` ,
`user_fecha_nacimiento` ,
`user_ruta_img` ,
`user_nota` ,
`Perfiles_perfil_nombre` ,
`Sucursales_sucursal_id`
)
VALUES (
NULL ,  'Victor',  'Manuel ',  'Galdames',  'Diaz', '17945182-4',  'Masculino',  '1991-12-12', NULL , NULL , 'SUPERADMIN',  '1'
)
--
-- Volcado de datos para la tabla `Usuarios`
--

INSERT INTO `Usuarios` (`user_id`, `user_nombre`, `user_segundo_nombre`, `user_am`, `user_ap`, `user_rut`, `user_genero`, `user_fecha_nacimiento`, `user_ruta_img`, `user_nota`, `Perfiles_perfil_nombre`) VALUES
(1, 'Víctor', 'Manuel', 'Galdames', 'Díaz', '17945182-4', 'Masculino', '0000-00-00', 'img/avatars_users/Web_VD.png', NULL, 'SUPERADMIN');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
