<?php

error_reporting(0);
//header('Content-Type: text/html; charset=UTF-8');
require_once './conexion_bd_excel.php';
require_once('./lib/PHPExcel.php');
session_start();
$fecha_diario = $_SESSION["reporte_diario_numero_mes"];
$con = mysql_connect($host, $user, $pass) or die("Problema para establecer la conexión");
mysql_select_db($db, $con)or die("Problema al Conectar a la Base Datos");
mysql_query("SET NAMES 'utf8'");
$resultado = mysql_query("SELECT DISTINCT(user_id), user_rut, user_ap, user_am, user_nombre, user_genero, user_carrera, user_correo_electronico  FROM Usuarios U,Perfiles P, Tag T, Registros R WHERE T.tag_id = U.Tag_tag_id AND P.perfil_id = U.Perfiles_perfil_id AND P.perfil_nombre = 'ALUMNO' AND R.registro_fecha = STR_TO_DATE('$fecha_diario','%d-%m-%Y') AND R.Usuarios_user_id = U.user_id", $con) or die('Inavlid query: ' . mysql_error());
if (!$resultado) {
    //die('Inavlid query: ' . mysql_error());
    header('Location: ../error404.php');
} else {
    $registros = mysql_num_rows($resultado);

    if ($registros > 0) {

        $objPHPExcel = new PHPExcel();

        //Informacion del excel
        $objPHPExcel->
                getProperties()
                ->setCreator("nfconnection.cl")
                ->setLastModifiedBy("nfconnection.cl")
                ->setTitle("Inscritos a ViveDuoc")
                ->setSubject("Inscritos Vive Duoc")
                ->setDescription("Documento generado con PHPExcel")
                ->setKeywords("nfconnection.cl  con  phpexcel")
                ->setCategory("Inscritos");

        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(9);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);

        $i = 2;
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'RUT')
                ->setCellValue('B1', 'Apellido Paterno')
                ->setCellValue('C1', 'Apellido Materno')
                ->setCellValue('D1', 'Nombre')
                ->setCellValue('E1', 'Genero')
                ->setCellValue('F1', 'Carrera')
                ->setCellValue('G1', 'Correo Electrónico')
                ->setCellValue('H1', 'Accesos (Entradas)')
                ->setCellValue('I1', 'Accesos (Salidas)');

        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()
                ->getStyle('A1:I1')
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('f89406');
        require_once '../controlador/Db.class.php';
        $db = new Db();
        while ($registro = mysql_fetch_object($resultado)) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $registro->user_rut)
                    ->setCellValue('B' . $i, $registro->user_ap)
                    ->setCellValue('C' . $i, $registro->user_am)
                    ->setCellValue('D' . $i, $registro->user_nombre)
                    ->setCellValue('E' . $i, $registro->user_genero)
                    ->setCellValue('F' . $i, $registro->user_carrera)
                    ->setCellValue('G' . $i, $registro->user_correo_electronico)
                    ->setCellValue('H' . $i, $db->single("SELECT COUNT(registro_acceso) FROM Usuarios U, Registros R WHERE U.user_id = '$registro->user_id' AND R.Usuarios_user_id = '$registro->user_id' AND registro_acceso = 'Entrada' AND R.registro_fecha = STR_TO_DATE('$fecha_diario','%d-%m-%Y')"))
                    ->setCellValue('I' . $i, $db->single("SELECT COUNT(registro_acceso) FROM Usuarios U, Registros R WHERE U.user_id = '$registro->user_id' AND R.Usuarios_user_id = '$registro->user_id' AND registro_acceso = 'Salida' AND R.registro_fecha = STR_TO_DATE('$fecha_diario','%d-%m-%Y')"));
            $i++;
        }

        date_default_timezone_set("America/Santiago");
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Datos de Reporte Diario_' . date('YmdHis') . '.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
        mysql_close();
    } else {
        header('Location: ../error404.php');
    }
}
?>