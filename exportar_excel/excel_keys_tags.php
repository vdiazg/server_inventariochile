<?php
header('Content-Type: text/html; charset=UTF-8');
require_once './conexion_bd_excel.php';
require_once('./lib/PHPExcel.php');
$con = mysql_connect($host, $user, $pass) or die("Problema para establecer la conexión");
mysql_select_db($db, $con)or die("Problema al Conectar a la Base Datos");
mysql_query("SET NAMES 'utf8'");
$resultado = mysql_query("SELECT * FROM Usuarios", $con);
$registros = mysql_num_rows ($resultado);

 if ($registros > 0) {
   
   $objPHPExcel = new PHPExcel();
    
   //Informacion del excel
   $objPHPExcel->
    getProperties()
        ->setCreator("nfconnection.cl")
        ->setLastModifiedBy("nfconnection.cl")
        ->setTitle("Inscritos a ViveDuoc")
        ->setSubject("Inscritos Vive Duoc")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("nfconnection.cl  con  phpexcel")
        ->setCategory("Inscritos");    
    
    $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
    $objPHPExcel->getDefaultStyle()->getFont()->setSize(9);
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);

   $i = 2;
   $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Nombre')
    ->setCellValue('B1', 'Apellido Paterno')
    ->setCellValue('C1', 'Apellido Materno')
    ->setCellValue('D1', 'RUT')
    ->setCellValue('E1', 'Genero')
    ->setCellValue('F1', 'Correo Electrónico');
   
   $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
   $objPHPExcel->getActiveSheet()
    ->getStyle('A1:F1')
    ->getFill()
    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
    ->getStartColor()->setARGB('f89406');
   
   while ($registro = mysql_fetch_object ($resultado)) {
        
      $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $registro->user_nombre)
            ->setCellValue('B'.$i, $registro->user_ap)
            ->setCellValue('C'.$i, $registro->user_am)
            ->setCellValue('D'.$i, $registro->user_rut)
            ->setCellValue('E'.$i, $registro->user_genero)
            ->setCellValue('F'.$i, $registro->user_correo_electronico);
      $i++;
       
   }
}
date_default_timezone_set('Etc/GMT+4');
//date_default_timezone_set("America/Buenos_Aires");
$fecha = date("d/m/y");
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Inscritos CITT '.$fecha.'.xlsx"');
header('Cache-Control: max-age=0');
 
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
mysql_close ();
?>