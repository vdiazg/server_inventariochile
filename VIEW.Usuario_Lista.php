<?php
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UsernameUsuario = $_SESSION["datos_usuario_logueado"][10];

if (!isset($IDUsuario)) {
    header('Location: login.php');
}
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="index.php"> Home</a></li>
                        <li class="active">Empleados</li>	 
                    </ul>
                </div><!-- breadcrumb -->
                <br/>
                <!--<div class="row paddingLR-md">
                    <div class="col-md-12 ">
                        <a href="./VIEW.Usuario_Add.php" class="btn btn-success pull-right"><span class="fa fa-plus-circle"></span> Añadir Nuevo Empleado</a>
                    </div>                    
                </div>-->
                <div class="padding-md">
                    <div class="panel panel-default table-responsive">
                        <div class="panel-heading">                            
                            <div class="row">
                                <div class="col-md-6"><i class="fa fa-list-alt"></i> EMPLEADOS <span class="badge badge-info"><?php echo $db->single("SELECT COUNT(*) FROM Usuarios"); ?></span></div>
                                
                            </div>
                        </div>
                        <div class="padding-md clearfix">
                            <table class="table table-striped" id="dataTable" style="font-size: 12px;">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Imagen</th>
                                        <th class="text-center">Nombres</th>
                                        <th class="text-center">Apellidos</th>
                                        <th class="text-center">Género</th>
                                        <th class="text-center">Fecha de Nacimiento</th>
                                        <th class="text-center">Perfil</th>
                                        <th class="text-center">Editar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $SQL_DATA_SELECT_USUARIOS = $db->query("SELECT * FROM Usuarios");
                                    foreach ($SQL_DATA_SELECT_USUARIOS as $row):

                                        $USUARIO_ID_BD = $row["user_id"];
                                        $USUARIO_RUT = $row["user_rut"];
                                        $USUARIO_NOMBRES = $row["user_nombre"] . ' ' . $row["user_segundo_nombre"];
                                        $USUARIO_APELLIDOS = $row["user_ap"] . ' ' . $row["user_am"];
                                        $USUARIO_GENERO = $row["user_genero"];
                                        $USUARIO_FECHA_NACIMIENTO = $row["user_fecha_nacimiento"];
                                        $USUARIO_PERFIL = $row["Perfiles_perfil_nombre"];
                                        $USUARIO_NOTA = $row["user_nota"];
                                        $USUARIO_IMG = $row["user_ruta_img"];

                                        echo '<tr>';
                                        echo '<td class="text-center">' . $USUARIO_ID_BD . '</td>';
                                        echo '<td class="text-center"><img class="" style="width: 44px; height: 44px;" src="' . $USUARIO_IMG . '" /></td>';
                                        echo '<td class="text-center">' . $USUARIO_NOMBRES . '</td>';
                                        echo '<td class="text-center">' . $USUARIO_APELLIDOS . '</td>';
                                        echo '<td class="text-center">' . $USUARIO_GENERO . '</td>';
                                        echo '<td class="text-center">' . $USUARIO_FECHA_NACIMIENTO . '</td>';
                                        echo '<td class="text-center">' . $USUARIO_PERFIL . '</td>';
                                        echo "<td class='text-center'>
                                            <div class='btn-group'>
                                                <a href='VIEW.Usuario_Modificar.php?id=" . $USUARIO_ID_BD . "' class='btn btn-default dropdown-toggle btn-xs'><i class='fa fa-edit fa-lg'></i> Modificar </a>
                                                <a href='#' class='btn btn-default dropdown-toggle btn-xs' data-toggle='dropdown'><span class='caret'></span></a>
                                                <ul class='dropdown-menu slidedown'>
                                                    <li><a href='./modelo/MOD.Usuario_Eliminar.php?id=" . $USUARIO_ID_BD . "'><i class='fa fa-trash-o fa-lg'></i> Eliminar</a></li>
                                                </ul>
                                            </div>
                                        </td>";
                                        echo '</tr>';
                                    endforeach;
                                    ?>      
                                </tbody>
                            </table>
                        </div><!-- /.padding-md -->
                    </div><!-- /panel -->
                </div>
            </div><!-- /main-container -->
        </div><!-- /wrapper -->
        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <?php require_once './includes/footer.inc.php'; ?>
        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>
        <!-- Le javascript ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src="js/js-nfconnection/jquery.validate.min.js"></script>
        <script src="js/js-nfconnection/custom_jquery_validate.js"></script>

        <script src='js/jquery.dataTables.min.js'></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#dataTable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "asStripClasses": null
                });
                $('#refrescar_pagina').click(function () {
                    location.reload();
                });
            });
        </script>
        <script src="js/menu-active-class.js"></script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Bootstrap Switch -->
        <script src='js/js-nfconnection/bootstrap-switch.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

    </body>
</html>