// Para sobreescribir los mensajes de error de jquery-validation-plugin
jQuery.extend(jQuery.validator.messages, {
    required: "Debe completar este campo.",
    remote: "Please fix this field.",
    email: "Correo eletrónico es inválido.",
    url: "Please enter a valid URL.",
    date: "Por favor, introduzca una fecha válida.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Debe ingresar un número válido.",
    digits: "Debe ingresar números enteros (dí­gitos).",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Debe ingresar la misma contraseña nuevamente.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Largo maximo del campo: {0}."),
    minlength: jQuery.validator.format("Largo minimo del campo: {0}."),
    rangelength: jQuery.validator.format("Debe ingresar un valor con un largo entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Debe ingresar un valor menor o igual a {0}."),
    min: jQuery.validator.format("Debe ingresar un valor mayor o igual a {0}.")
});

// Para validar un rut Chileno con Jquery-Validate
function validaRut(campo) {
    if (campo.length == 0) {
        return false;
    }
    if (campo.length < 8) {
        return false;
    }

    // Chequea que tenga el guion antes del digito verificador
    if (campo.charAt(campo.length - 2) != '-') {
        return false;
    }

    // Chequea que no tenga puntos (esto se puede quitar si deseas que el rut contenga puntos)
    if (campo.indexOf('.') != -1) {
        return false;
    }

    campo = campo.replace('-', '')
    campo = campo.replace(/\./g, '')

    var suma = 0;
    var caracteres = "1234567890kK";
    var contador = 0;
    for (var i = 0; i < campo.length; i++) {
        u = campo.substring(i, i + 1);
        if (caracteres.indexOf(u) != -1)
            contador++;
    }
    if (contador == 0) {
        return false
    }

    var rut = campo.substring(0, campo.length - 1)
    var drut = campo.substring(campo.length - 1)
    var dvr = '0';
    var mul = 2;

    for (i = rut.length - 1; i >= 0; i--) {
        suma = suma + rut.charAt(i) * mul
        if (mul == 7)
            mul = 2
        else
            mul++
    }
    res = suma % 11
    if (res == 1)
        dvr = 'k'
    else if (res == 0)
        dvr = '0'
    else {
        dvi = 11 - res
        dvr = dvi + ""
    }
    if (dvr != drut.toLowerCase()) {
        return false;
    }
    else {
        return true;
    }
}

$.validator.addMethod("rut_required", function (value, element) {
    return this.optional(element) || validaRut(value);
}, "El rut ingresado es inválido.");

$.validator.addMethod("letras_espacios_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-ZáéíóúñÁÉÍÓÚ ]+" + "$"));
}, "En este campo sólo se permiten letras y espacios.");

$.validator.addMethod("letras_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-ZáéíóúñÁÉÍÓÚ]+" + "$"));
}, "En este campo sólo se permiten letras");

$.validator.addMethod("letras_numeros_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-z0-9]+" + "$"));
}, "En este campo sólo se permiten letras minúsculas y números.");

$.validator.addMethod("letras_numeros_espacios_guion_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-Z- 0-9]+" + "$"));
}, "En este campo sólo se permiten letras mayusculas, minusculas, guiones y números.");

$.validator.addMethod("letras_espacios_comas_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-Z, ]+" + "$"));
}, "En este campo sólo se permiten letras mayusculas, minusculas y comas.");

$.validator.addMethod("dos_decimales_required", function (value, element) {
    return value.match(new RegExp("^([0-9]+)|([0-9]+\.([0-9]|[0-9][0-9]))$"));
}, "En este campo debe ingresar un número con 1 o 2 decimales.");

$.validator.addMethod("letras_espacios_puntos_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-ZáéíóúñÁÉÍÓÚ .]+" + "$"));
});

$.validator.addMethod("validacion_email", function (value, element) {
    return value.match(new RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i));
}, "Correo eletrónico es inválido.");

$.validator.addMethod("numeros_letras_guion", function (value, element) {
    return value.match(new RegExp(/^[0-9-]{1,10}$/));
});

$.validator.addMethod("numeros_required", function (value, element) {
    return value.match(new RegExp("^" + "[0-9]+" + "$"));
}, "En este campo sólo se permiten números.");

$.validator.addMethod('IP4Checker', function (value) {
    return value.match(new RegExp(/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/));
}, 'Dirección IP Inválida');

$.validator.addMethod('url_validation', function (value) {
    return value.match(new RegExp(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \?=.-]*)*\/?$/));
}, 'URL Inválida');

$.validator.addMethod('numero_celular_chileno', function (value) {
    return value.match(new RegExp("^[+][0-9]{3,3}-? ?[0-9]{8,8}$"));
}, 'Número inválido, Formato Correcto: +56912345678');

$(document).ready(function () {

    $("#id_panel_form").hide();
    $("#form_login_usuario").validate({
        rules: {
            username: {required: true},
            password: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            username: {required: "Campo obligatorio."},
            password: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_login_usuario').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/verificar_usuario.php",
                data: $(form).serialize(),
                success: function (data) {

                    if (data == "1") {
                        location.href = "dashboard.php";
                    } else if (data == "0") {
                        $('#msj_respuesta_login_usuario').fadeIn(1000).html('<div class="alert alert-danger"><strong>Error en el Usuario o Contraseña</strong></div>');
                    }
                }
            });
        }
    });
    $("#form_registro_Usuario").validate({
        rules: {
            username_registro: {required: true, validacion_email: true},
            password_registro: {required: true},
            repet_password: {required: true, equalTo: "#password_registro"},
            keyword: {required: true},
            num_serie_tag: {required: true},
            user_nombre: {required: true, letras_espacios_required: true},
            user_segundo_nombre: {required: true},
            user_ap: {required: true, letras_espacios_required: true},
            user_am: {required: true, letras_espacios_required: true},
            select_genero_user: {required: true},
            user_run: {required: true, rut_required: true},
            user_fecha_nacimiento: {required: true},
            input_enfermedad_1: {required: false},
            user_email_1: {required: true, validacion_email: true},
            user_email_2: {required: false, validacion_email: false},
            user_tel_movil_1: {required: true, numero_celular_chileno: true},
            user_tel_movil_2: {required: false, numero_celular_chileno: false}

        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            username_registro: {required: "Campo obligatorio.", validacion_email: "Correo eletrónico es inválido."},
            password_registro: {required: "Campo obligatorio."},
            repet_password: {required: "Campo obligatorio."},
            keyword: {required: "Campo obligatorio."},
            num_serie_tag: {required: "Campo obligatorio."},
            user_nombre: {required: "Campo obligatorio."},
            user_segundo_nombre: {required: "Campo obligatorio."},
            user_ap: {required: "Campo obligatorio."},
            user_am: {required: "Campo obligatorio."},
            select_genero_user: {required: "Campo obligatorio."},
            user_run: {required: "Campo obligatorio."},
            user_fecha_nacimiento: {required: "Campo obligatorio."},
            input_enfermedad_1: {required: "Campo obligatorio."},
            user_email_1: {required: "Campo obligatorio.", validacion_email: "Correo eletrónico es inválido."},
            user_email_2: {required: "Campo obligatorio.", validacion_email: "Correo eletrónico es inválido."},
            user_tel_movil_1: {required: "Campo obligatorio.", numero_celular_chileno: "Número inválido."},
            user_tel_movil_2: {required: "Campo obligatorio.", numero_celular_chileno: "Número inválido."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_usuarios').html('<img src="img/ajax-loader.gif" alt="" />');
            var src_img = $('#fotografia').attr('src');
            var keyword = $('#keyword').val();
            if (src_img == 'img/imagen_profile_default.png') {
                //alert("Seleccione una Imagen Porfavor");
                $('#error_img').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Por favor Seleccione una Imagen!</strong></div>');
                $('#msj_respuesta_ingreso_usuarios').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Por favor Seleccione una Imagen!</strong></div>');
            } else {
                $.ajax({
                    type: "POST",
                    url: "./modelo/ingreso_usuario.php",
                    data: $(form).serialize() + "&src_img=" + src_img,
                    success: function (data) {
                        //alert(data);
                        //$('#msj_respuesta_ingreso_usuarios').fadeIn(1000).html(data);
                        if (data == "1") {
                            $('#msj_respuesta_ingreso_usuarios').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> IDBand ha sido Activado exitosamente !</strong></div>');
                            $('#error_img').fadeIn(1000).html(' ');
                            //$('#form_registro_Usuario')[0].reset();
                            //$('#form_registro_Usuario').trigger("reset");
                            //$('#fotografia').removeAttr('scr');
                            //$('#fotografia').attr('src', 'img/imagen_profile_default.png');
                            alert("IDBand ha sido Activado exitosamente !");
                            window.location.href = "http://hqr.cl/" + keyword;
                        } else if (data == "0") {
                            $('#msj_respuesta_ingreso_usuarios').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                        } else if (data == "2") {
                            $('#msj_respuesta_ingreso_usuarios').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error al registrar Usuario!</strong></div>');
                        }

                    }
                });
            }
        }
    });
    $("#form_Update_DatosUsuarioSistema").validate({
        rules: {
            UpdateNombreUsuario: {required: true},
            UpdateApellidoPaterno: {required: true},
            UpdateApellidoMaterno: {required: true},
            UpdateFechaNacimiento: {required: true},
            UpdateContrasena: {
                required: false,
                minlength: 5,
                maxlength: 12
            },
            UpdateConfirmContrasena: {
                required: false,
                minlength: 5,
                maxlength: 12,
                equalTo: "#UpdateContrasena"
            },
            UpdateCorreoElectronico: {required: true},
            UpdateTelMovil: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            UpdateNombreUsuario: {required: "Campo obligatorio."},
            UpdateApellidoPaterno: {required: "Campo obligatorio."},
            UpdateApellidoMaterno: {required: "Campo obligatorio."},
            UpdateFechaNacimiento: {required: "Campo obligatorio."},
            UpdateContrasena: {
                required: "Campo obligatorio.",
                minlength: "Introduzca una contraseña entre 5 y 12 caracteres",
                maxlength: "Introduzca una contraseña entre 5 y 12 caracteres"
            },
            UpdateConfirmContrasena: {
                required: "Campo obligatorio.",
                minlength: "Introduzca una contraseña entre 5 y 12 caracteres",
                maxlength: "Introduzca una contraseña entre 5 y 13 caracteres"
            },
            UpdateCorreoElectronico: {required: "Campo obligatorio."},
            UpdateTelMovil: {required: "Campo obligatorio."}

        },
        submitHandler: function (form) {
            $('#msj_respuesta_update_usuarios').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/update_usuario.php",
                data: $(form).serialize(),
                success: function (data) {
                    //alert(data);
                    $('#msj_respuesta_update_usuarios').html(data);
                }
            });
        }
    });
    $("#form_Update_usuarios_sistema_adminis_admin").validate({
        rules: {
            Update_user_nombre: {required: true},
            Update_user_apell_paterno: {required: true},
            Update_user_apell_materno: {required: true},
            Update_user_tel_movil: {required: true},
            Update_user_email: {required: true},
            Update_username: {required: true},
            Update_user_contrasena: {required: false},
            Update_user_confirm_contrasena: {required: false},
            select_tag_user: {required: false},
            select_carrera_usuario: {required: false},
            select_Permiso_usuario: {required: false},
            'switch-state_estado_usuario': {required: true},
            'switch-state_estado_tag': {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'has-error',
        messages: {
            Update_user_nombre: {required: "Campo obligatorio."},
            Update_user_apell_paterno: {required: "Campo obligatorio."},
            Update_user_apell_materno: {required: "Campo obligatorio."},
            Update_user_tel_movil: {required: "Campo obligatorio."},
            Update_user_email: {required: "Campo obligatorio."},
            Update_username: {required: "Campo obligatorio."},
            Update_user_contrasena: {required: "Campo obligatorio."},
            Update_user_confirm_contrasena: {required: "Campo obligatorio."},
            select_tag_user: {required: "Campo obligatorio."},
            select_carrera_usuario: {required: "Campo obligatorio."},
            select_Permiso_usuario: {required: "Campo obligatorio."},
            'switch-state_estado_usuario': {required: ""},
            'switch-state_estado_tag': {required: ""}
        }
    });
    $("#form_numero_secreto").validate({
        rules: {
            num_secreto_key: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            num_secreto_key: {required: "Campo obligatorio."}
        },
        submitHandler: function (form) {
            $('#alerta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/verificar_key.php",
                data: $(form).serialize(),
                success: function (data) {
                    //alert(data);
                    //$('#msj_respuesta_login_usuario').fadeIn(1000).html('<div class="alert alert-danger"><strong>SI EXISTE</strong></div>');
                    if (data == "1") {
                        //Todo Correcto Para Registrar
                        $('#alerta').hide();
                        $("#id_panel_form").removeClass('hide')
                        $("#id_panel_form").show();
                    }
                    if (data == "2") {
                        //Usuario Ya Registrado.
                        $("#id_panel_form").hide();
                        $('#alerta').fadeIn(1000).html('<div class="alert alert-danger"><strong>Usuario Ya Registrado.</strong></div>');
                    }
                    if (data == "3") {
                        //No Existe la Contraseña, verifiquela.
                        $("#id_panel_form").hide();
                        $('#alerta').fadeIn(1000).html('<div class="alert alert-danger"><strong>No Existe la Contraseña, verifiquela.</strong></div>');
                    }
                    if (data == "4") {
                        //Por favor Ingresar Clave Secreta.
                        $("#id_panel_form").hide();
                        $('#alerta').fadeIn(1000).html('<div class="alert alert-danger"><strong>Por favor Ingresar Clave Secreta.</strong></div>');
                    }
                    if (data == "5") {
                        //Por favor Ingresar Clave Secreta.
                        $("#id_panel_form").hide();
                        $('#alerta').fadeIn(1000).html('<div class="alert alert-danger"><strong>Clave Secreta No Asociada.</strong></div>');
                    }
                    if (data == "6") {
                        //Por favor Ingresar Clave Secreta.
                        $("#id_panel_form").hide();
                        $('#alerta').fadeIn(1000).html('<div class="alert alert-danger"><strong>Número Secreto Incorrecto.</strong></div>');
                    }
                }
            });
        }
    });
    $("#form_login_mod_data").validate({
        rules: {
            username_modificar: {required: true, validacion_email: true},
            password_modificar: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            username_modificar: {required: "Campo Obligatorio.", validacion_email: "Correo eletrónico es inválido."},
            password_modificar: {required: "Campo Obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_login_mod_data').html('<img src="img/ajax-loader.gif" alt="" />');
            var keyword_mod = $('#keyword_mod').val();
            $.ajax({
                type: "POST",
                url: "./modelo/veri_mod_data_idband.php",
                data: $(form).serialize(),
                success: function (data) {
                    if (data == "1") {
                        location.href = "mod_data_idband.php";
                    }
                    if (data == "0") {
                        $('#msj_respuesta_login_mod_data').fadeIn(1000).html('<div class="alert alert-danger"><strong>Error en el Usuario o Contraseña</strong></div>');
                    }
                }
            });

        }
    });
    $("#form_activacion_idband").validate({
        rules: {
            keyword_act: {required: true},
            num_serie_act: {required: true},
            key_act: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            keyword_act: {required: "Campo Obligatorio."},
            num_serie_act: {required: "Campo Obligatorio."},
            key_act: {required: "Campo Obligatorio."}
        },
        submitHandler: function (form) {
            $('#msj_respuesta_login_mod_data').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                data: $(form).serialize(),
                type: "POST",
                dataType: "json",
                url: "./modelo/veri_data_act_idband.php",
            }).done(function (data, textStatus, jqXHR) {
                if (console && console.log) {
                    if (data.token == '0' && data.keyword_act == '0' && data.n_serie_act == '0' && data.key_act == '0') {
                        $('#msj_respuesta_login_mod_data').html('<div class="alert alert-danger"><strong>Error, verificar datos ingresados.</strong></div>');
                    } else {
                        //window.location.href = SERVER+"registroIDBand.php?token=" + data.token + '&keyword_act=' + data.keyword_act + '&n_serie_act=' + data.n_serie_act + '&key_act=' + data.key_act;
                        $.ajax({
                            type: "POST",
                            url: "./registroIDBand.php",
                            data: "token=" + data.token + "&keyword_act=" + data.keyword_act + "&n_serie_act=" + data.n_serie_act + "&key_act=" + data.key_act,
                            success: function (data2) {
                                if (data2 == "0") {
                                    window.location.href = SERVER + "error500.php";
                                } else {
                                    window.location.href = SERVER + "registroIDBand.php";
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {

                            }
                        });
                    }
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (console && console.log) {
                    console.log("La solicitud a fallado: " + textStatus);
                }
            });

        }
    });
    $("#form_update_usuario").validate({
        rules: {
            mod_keyword: {required: true},
            mod_num_serie_tag: {required: true},
            mod_user_run: {required: true, rut_required: true},
            mod_username_registro: {required: true, validacion_email: true},
            mod_password_registro: {required: false},
            mod_repet_password: {required: false, equalTo: "#mod_password_registro"},
            mod_user_nombre: {required: true, letras_espacios_required: true},
            mod_user_segundo_nombre: {required: true},
            mod_user_ap: {required: true, letras_espacios_required: true},
            mod_user_am: {required: true, letras_espacios_required: true},
            mod_select_genero_user: {required: true},
            mod_user_fecha_nacimiento: {required: true},
            mod_select_grup_sanguineo_user: {required: true},
            mod_input_enfermedad_1: {required: false},
            mod_descrip_enfermedad_1: {required: false},
            mod_input_enfermedad_2: {required: false},
            mod_descrip_enfermedad_2: {required: false},
            mod_input_enfermedad_3: {required: false},
            mod_descrip_enfermedad_3: {required: false},
            mod_input_enfermedad_4: {required: false},
            mod_descrip_enfermedad_4: {required: false},
            mod_user_email_1: {required: true, validacion_email: true},
            mod_user_email_2: {required: false, validacion_email: false},
            mod_user_tel_movil_1: {required: true, numero_celular_chileno: true},
            mod_user_tel_movil_2: {required: false, numero_celular_chileno: true}

        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
            mod_keyword: {required: "Campo obligatorio."},
            mod_num_serie_tag: {required: "Campo obligatorio."},
            mod_user_run: {required: "Campo obligatorio.", rut_required: "Campo obligatorio."},
            mod_username_registro: {required: "Campo obligatorio.", validacion_email: "Correo eletrónico es inválido."},
            mod_password_registro: {required: "Campo obligatorio."},
            mod_repet_password: {required: "Campo obligatorio."},
            mod_user_nombre: {required: "Campo obligatorio.", letras_espacios_required: "Correo eletrónico es inválido."},
            mod_user_segundo_nombre: {required: "Campo obligatorio."},
            mod_user_ap: {required: "Campo obligatorio.", letras_espacios_required: "Correo eletrónico es inválido."},
            mod_user_am: {required: "Campo obligatorio.", letras_espacios_required: "Correo eletrónico es inválido."},
            mod_select_genero_user: {required: "Campo obligatorio."},
            mod_user_fecha_nacimiento: {required: "Campo obligatorio."},
            mod_select_grup_sanguineo_user: {required: "Campo obligatorio."},
            mod_input_enfermedad_1: {required: "Campo obligatorio."},
            mod_descrip_enfermedad_1: {required: "Campo obligatorio."},
            mod_input_enfermedad_2: {required: "Campo obligatorio."},
            mod_descrip_enfermedad_2: {required: "Campo obligatorio."},
            mod_input_enfermedad_3: {required: "Campo obligatorio."},
            mod_descrip_enfermedad_3: {required: "Campo obligatorio."},
            mod_input_enfermedad_4: {required: "Campo obligatorio."},
            mod_descrip_enfermedad_4: {required: "Campo obligatorio."},
            user_email_1: {required: "Campo obligatorio.", validacion_email: "Correo eletrónico es inválido."},
            user_email_2: {required: "Campo obligatorio.", validacion_email: "Correo eletrónico es inválido."},
            user_tel_movil_1: {required: "Campo obligatorio.", numero_celular_chileno: "Número inválido."},
            user_tel_movil_2: {required: "Campo obligatorio.", numero_celular_chileno: "Número inválido."}
        },
        submitHandler: function (form) {
            if (confirm('¿ Esta Seguro de Modificar la Información ?')) {
                $('#msj_respuesta_ingreso_usuarios').html('<img src="img/ajax-loader.gif" alt="" />');
                var src_img = $('#fotografia').attr('src');
                var mod_keyword = $('#mod_keyword').val();
                var mod_num_serie_tag = $('#mod_num_serie_tag').val();
                var mod_user_run = $('#mod_user_run').val();

                if (src_img == 'img/technology.svg') {
                    //alert("Seleccione una Imagen Porfavor");
                    $('#error_img').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Por favor Seleccione una Imagen!</strong></div>');
                    $('#msj_respuesta_ingreso_usuarios').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Por favor Seleccione una Imagen!</strong></div>');
                } else {
                    $.ajax({
                        type: "POST",
                        url: "./modelo/mod_cliente_idband.php",
                        data: $(form).serialize() +
                                "&src_img=" + src_img +
                                "&mod_keyword=" + mod_keyword +
                                "&mod_num_serie_tag=" + mod_num_serie_tag +
                                "&mod_user_run=" + mod_user_run,
                        success: function (data) {
                            //alert(data);
                            //$('#msj_respuesta_ingreso_usuarios').html(function(){ alert(data); });
                            $('#msj_respuesta_ingreso_usuarios').html(data);
                            window.location.href = SERVER + 'id.php?id=' + mod_keyword;
                        }
                    });
                }
            }
        }
    });
    $("#ADMIN_form_registro_Usuario").validate({
        rules: {
            input_buscar_tid: {required: true},
            ADMIN_user_N_SERIE_PULSERA_NFC: {required: true},
            ADMIN_user_run: {required: true},
            ADMIN_user_nombre: {required: true},
            ADMIN_user_segundo_nombre: {required: true},
            ADMIN_user_apell_paterno: {required: true},
            ADMIN_user_am: {required: true},
            select_genero_user: {required: true},
            ADMIN_user_fecha_nacimiento: {required: true},
            select_Permiso_usuario: {required: true},
            user_nota: {required: true},
            ADMIN_user_username: {required: true, validacion_email: true},
            ADMIN_user_contrasena: {required: true},
            ADMIN_user_confirm_contrasena: {required: true, equalTo: "#ADMIN_user_contrasena"},
            select_grup_sanguineo_user: {required: true},
            input_enfermedad_1: {required: true},
            input_enfermedad_2: {required: false},
            input_enfermedad_3: {required: false},
            input_enfermedad_4: {required: false},
            descrip_enfermedad_1: {required: true},
            descrip_enfermedad_2: {required: false},
            descrip_enfermedad_3: {required: false},
            descrip_enfermedad_4: {required: false},
            ADMIN_user_tel_movil_1: {required: true, numero_celular_chileno: true},
            ADMIN_user_tel_movil_2: {required: false},
            ADMIN_user_email_1: {required: true},
            ADMIN_user_email_2: {required: false}

        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
        },
        submitHandler: function (form) {
            $('#msj_respuesta_ingreso_usuarios').html('<img src="img/ajax-loader.gif" alt="" />');
            var src_img = $('#fotografia').attr('src');
            var keyword = $('#keyword').val();
            if (src_img == 'img/imagen_profile_default.png') {
                //alert("Seleccione una Imagen Porfavor");
                $('#error_img').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Por favor Seleccione una Imagen!</strong></div>');
                $('#msj_respuesta_ingreso_usuarios').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Por favor Seleccione una Imagen!</strong></div>');
            } else {
                $.ajax({
                    type: "POST",
                    url: "./modelo/ADMIN_ingreso_usuario.php",
                    data: $(form).serialize() + "&src_img=" + src_img,
                    success: function (data) {
                        //alert(data);
                        //$('#msj_respuesta_ingreso_usuarios').fadeIn(1000).html(data);
                        if (data == "1") {
                            $('#msj_respuesta_ingreso_usuarios').fadeIn(1000).html('<div class="alert alert-success"><strong><i class="fa fa-check-circle-o fa-lg  fa-2x" style="color: green;"></i> IDBand ha sido Activado exitosamente !</strong></div>');
                            $('#error_img').fadeIn(1000).html(' ');
                            //$('#form_registro_Usuario')[0].reset();
                            //$('#form_registro_Usuario').trigger("reset");
                            //$('#fotografia').removeAttr('scr');
                            //$('#fotografia').attr('src', 'img/imagen_profile_default.png');
                            alert("IDBand ha sido Activado exitosamente !");
                            //window.location.href = "http://hqr.cl/" + keyword;
                        } else if (data == "0") {
                            $('#msj_respuesta_ingreso_usuarios').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                        } else if (data == "2") {
                            $('#msj_respuesta_ingreso_usuarios').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                        } else if (data == "3") {
                            $('#msj_respuesta_ingreso_usuarios').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> IDBand se encuentra en Uso</strong></div>');
                        }

                    }
                });
            }
        }
    });

    /* EMPRESA */
    $("#JSV_AddEmpresa").validate({
        rules: {
            emp_rut: {required: true, rut_required: true},
            emp_razon: {required: true},
            emp_giro: {required: true},
            emp_direccion: {required: true},
            emp_tel: {required: true, numero_celular_chileno: true},
            emp_fijo: {required: true, digits: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        submitHandler: function (form) {
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.AddEmpresa.php",
                data: $(form).serialize(),
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        location.href = "./VIEW.ListaEmpresas.php";
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> RUT Registrado.</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error, Intente más tarde.</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Formato del RUT Incorrecto,</strong></div>');
                    } else if (data == "4") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Formato de Número Teléfono Incorrecto.</strong></div>');
                    } else if (data == "5") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> *Revisar Campos Obligatorios.</strong></div>');
                    }
                }
            });
        }
    });
    $("#JSV_ModificarEmpresa").validate({
        rules: {
            emp_rut: {required: true, rut_required: true},
            emp_razon: {required: true},
            emp_giro: {required: true},
            emp_direccion: {required: true},
            emp_tel: {required: true, numero_celular_chileno: true},
            emp_fijo: {required: true, digits: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        submitHandler: function (form) {
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.ModificarEmpresa.php",
                data: $(form).serialize(),
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        location.href = "./VIEW.ListaEmpresas.php";
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> RUT Registrado.</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error, Intente más tarde.</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Formato del RUT Incorrecto,</strong></div>');
                    } else if (data == "4") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Formato de Número Teléfono Incorrecto.</strong></div>');
                    } else if (data == "5") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> *Revisar Campos Obligatorios.</strong></div>');
                    }
                }
            });
        }
    });

    /* EMPRESA */

    /* SUCURSAL */
    $("#JSV_AddSucursal").validate({
        rules: {
            socur_ref: {required: true},
            socur_nombre: {required: true, letras_required: true},
            socur_direccion: {required: true},
            socur_tel: {required: true, numero_celular_chileno: true},
            socur_encargado: {required: true},
            selecc_empresa: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        submitHandler: function (form) {
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.AddSucursal.php",
                data: $(form).serialize(),
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o fa-lg fa-2x" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        window.location.replace("./VIEW.ListaSucursales.php");
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Sucursal ya se encuentra registrada.</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error en Registrar Sucursarl, intentar más tarde.</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error en el Nombre de Sucursal</strong></div>');
                    } else if (data == "5") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> *Revisar Campos Obligatorios.</strong></div>');
                    }
                }
            });
        }
    });
    $("#JSV_ModificarSucursal").validate({
        rules: {
            Update_socur_ref: {required: true},
            Update_socur_nombre: {required: true, letras_espacios_required: true},
            Update_socur_direccion: {required: true, letras_numeros_espacios_guion_required: true},
            Update_socur_tel: {required: true},
            Update_socur_encargado: {required: true},
            Update_selecc_empresa: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        submitHandler: function (form) {
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.ModificarSucursal.php",
                data: $(form).serialize(),
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o fa-lg fa-2x" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        location.href = './VIEW.ListaSucursales.php'
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Sucursal no existe.</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> No se ha realizado ningún cambio.</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error en el Nombre de Sucursal</strong></div>');
                    } else if (data == "5") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> *Revisar Campos Obligatorios.</strong></div>');
                    }
                }
            });
        }
    });
    /* SUCURSAL */

    /* EMPLEADO */
    $("#JSV_AddEmpleado").validate({
        rules: {
            emple_run: {required: true, rut_required: true},
            emple_nombre: {required: true, letras_required: true},
            emple_segundo_nombre: {required: true, letras_required: true},
            emple_ap: {required: true, letras_required: true},
            emple_am: {required: true, letras_required: true},
            emple_genero: {required: true, letras_required: true},
            emple_fecha_nacimiento: {required: true},
            emple_perfil: {required: true},
            emple_username: {required: true, validacion_email: true},
            emple_contrasena: {required: true},
            emple_confir_contrasena: {required: true},
            emple_nota: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        submitHandler: function (form) {
            var src_img = $('#fotografia').attr('src');
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.AddEmpleado.php",
                data: $(form).serialize() + "&src_img=" + src_img,
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        window.location.replace("./VIEW.Usuario_Lista.php");
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> IDBand se encuentra en Uso</strong></div>');
                    } else if (data == "4") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Rut Incorrecto</strong></div>');
                    } else if (data == "5") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Faltan campos por rellenar</strong></div>');
                    }
                }
            });
        }
    });
    $("#JSV_ModificarEmpleado").validate({
        rules: {
            emple_run: {required: true, rut_required: true},
            emple_nombre: {required: true, letras_required: true},
            emple_segundo_nombre: {required: true, letras_required: true},
            emple_ap: {required: true, letras_required: true},
            emple_am: {required: true, letras_required: true},
            emple_genero: {required: true, letras_required: true},
            emple_fecha_nacimiento: {required: true},
            emple_perfil: {required: true},
            emple_username: {required: true, validacion_email: true},
            emple_contrasena: {required: true},
            emple_confir_contrasena: {required: true},
            emple_nota: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        submitHandler: function (form) {
            var src_img = $('#fotografia').attr('src');
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.ModificarEmpleado.php",
                data: $(form).serialize() + "&src_img=" + src_img,
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        window.location.replace("./VIEW.Usuario_Lista.php");
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!!</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!!!</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> IDBand se encuentra en Uso</strong></div>');
                    } else if (data == "4") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Rut Incorrecto</strong></div>');
                    } else if (data == "5") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Faltan campos por rellenar</strong></div>');
                    }
                }
            });
        }
    });
    /* EMPLEADO */

    /* BODEGA */
    $("#JSV_AddBodega").validate({
        rules: {
            bod_ref: {required: true},
            bod_nom: {required: true},
            bod_admin: {required: true},
            bod_localizacion: {required: true},
            bod_num_tel_movil: {required: false},
            bod_num_tel_fijo: {required: true},
            bod_direccion: {required: true},
            bod_direccion2: {required: true},
            bod_cod_postal: {required: true},
            bod_sucursal: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        submitHandler: function (form) {
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.AddBodega.php",
                data: $(form).serialize(),
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        window.location.replace("./VIEW.Bodega_Lista.php");
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> IDBand se encuentra en Uso</strong></div>');
                    } else if (data == "4") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Nombre incorrecto (Solo letras, minimo: 3)</strong></div>');
                    } else if (data == "5") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Telefono movil o fijo debe tener entre 7 y 8 digitos</strong></div>');
                    } else if (data == "6") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Codigo postal solo acepta numeros</strong></div>');
                    } else if (data == "7") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Alguno de los datos no fue ingresado</strong></div>');
                    }

                }
            });
        }
    });
    $("#JSV_ModificarBodega").validate({
        rules: {
            bod_ref: {required: true},
            bod_nom: {required: true},
            bod_admin: {required: true},
            bod_localizacion: {required: true},
            bod_num_tel_movil: {required: false},
            bod_num_tel_fijo: {required: true},
            bod_direccion: {required: true},
            bod_direccion2: {required: true},
            bod_cod_postal: {required: true},
            bod_sucursal: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        submitHandler: function (form) {
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.Bodega_Modificar.php",
                data: $(form).serialize(),
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        window.location.replace("./VIEW.Bodega_Lista.php");
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!!</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> IDBand se encuentra en Uso</strong></div>');
                    } else if (data == "4") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Nombre incorrecto (Solo letras, minimo: 3)</strong></div>');
                    } else if (data == "5") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Telefono movil o fijo debe tener entre 7 y 8 digitos</strong></div>');
                    } else if (data == "6") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Codigo postal solo acepta numeros</strong></div>');
                    } else if (data == "7") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Alguno de los datos no fue ingresado</strong></div>');
                    }

                }
            });
        }
    });
    /* BODEGA */

    /* CATEGORIA */
    $("#JSV_AddCategoria").validate({
        rules: {
            cate_ref: {required: true},
            cate_nom: {required: true, letras_required: true},
            cate_descrip: {required: true, letras_espacios_required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
        },
        submitHandler: function (form) {
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.Categoria_Add.php",
                data: $(form).serialize(),
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        window.location.replace("./VIEW.Categorias_Lista.php");
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Nombre invalido</strong></div>');
                    } else if (data == "4") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Faltan campos por rellenar</strong></div>');
                    }

                }
            });
        }
    });
    $("#JSV_ModificarCategoria").validate({
        rules: {
            cate_ref: {required: true},
            cate_nom: {required: true, letras_required: true},
            cate_descrip: {required: true, letras_espacios_required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
        },
        submitHandler: function (form) {
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.Categoria_Modificar.php",
                data: $(form).serialize(),
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        window.location.replace("./VIEW.Categorias_Lista.php");
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Nombre invalido</strong></div>');
                    } else if (data == "4") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Faltan campos por rellenar</strong></div>');
                    }

                }
            });
        }
    });
    /* CATEGORIA */

    /* PRODUCTO */
    $("#JSV_AddProducto").validate({
        rules: {
            product_nom: {required: true},
            product_marca: {required: true},
            product_cod_ean13: {required: true},
            product_cod_JAN: {required: true},
            product_cod_upc: {required: true},
            product_cod_ref: {required: true},
            product_descrip_corta: {required: true, letras_espacios_required: true},
            product_descrip_larga: {required: true, letras_espacios_required: true},
            product_imagen_ref: {required: false},
            product_catego: {required: true},
            product_precio: {required: true},
            product_bodega: {required: true},
            product_status: {required: true},
            product_cantidad: {required: true}

        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
        },
        submitHandler: function (form) {
            var src_img = $('#fotografia').attr('src');
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.Producto_Add.php",
                data: $(form).serialize() + "&src_img=" + src_img,
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        window.location.replace("./VIEW.Productos_Lista.php");
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Nombre invalido</strong></div>');
                    } else if (data == "4") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Codigo de barras debe contener solo numeros y caracteres</strong></div>');
                    } else if (data == "5") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Faltan campos por rellenar</strong></div>');
                    }

                }
            });
        }
    });
    $("#JSV_ModificarProducto").validate({
        rules: {
            product_nom: {required: true},
            product_marca: {required: true},
            product_cod_ean13: {required: true},
            product_cod_JAN: {required: true},
            product_cod_upc: {required: true},
            product_cod_ref: {required: true},
            product_descrip_corta: {required: true, letras_espacios_required: true},
            product_descrip_larga: {required: true, letras_espacios_required: true},
            product_imagen_ref: {required: false},
            product_catego: {required: true},
            product_precio: {required: true},
            product_bodega: {required: true},
            product_status: {required: true},
            product_cantidad: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
        },
        submitHandler: function (form) {
            var src_img = $('#fotografia').attr('src');
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.Producto_Modificar.php",
                data: $(form).serialize() + "&src_img=" + src_img,
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o" style="color: green;"></i> Registro Modificado Exitosamente.</strong></div>');
                        window.location.replace("./VIEW.Productos_Lista.php");
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Nombre invalido</strong></div>');
                    } else if (data == "4") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Codigo de barras debe contener solo numeros y caracteres</strong></div>');
                    } else if (data == "5") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Faltan campos por rellenar</strong></div>');
                    }

                }
            });
        }
    });
    /* PRODUCTO */

    /* EXISTENCIAS */
    $("#JSV_AddExistencias").validate({
        rules: {
            producto_nombre: {required: true},
            producto_ref: {required: true},
            producto_cantidad: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
        },
        submitHandler: function (form) {
            var src_img = $('#fotografia').attr('src');
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.Existencias_Add.php",
                data: $(form).serialize() + "&src_img=" + src_img,
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        window.location.replace("./VIEW.Productos_Lista.php");
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Nombre invalido</strong></div>');
                    } else if (data == "4") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Codigo de barras debe contener solo numeros y caracteres</strong></div>');
                    } else if (data == "5") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Faltan campos por rellenar</strong></div>');
                    }

                }
            });
        }
    });
    /* EXISTENCIAS */

    /* PROVEEDOR */
    $("#JSV_AddProveedor").validate({
        rules: {
            provee_razon_social: {required: true},
            provee_rut: {required: true, rut_required: true},
            provee_giro: {required: true},
            provee_direccion: {required: true},
            provee_tel_fijo: {required: true, digits: true, minlength: 7, maxlength: 8},
            provee_tel_movil: {required: true, numero_celular_chileno: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
        },
        submitHandler: function (form) {
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.AddProveedor.php",
                data: $(form).serialize(),
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        window.location.replace("./proveedores.php");
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Rut invalido</strong></div>');
                    } else if (data == "4") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Numero de telefono invalido</strong></div>');
                    } else if (data == "5") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Faltan campos por rellenar</strong></div>');
                    } else {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Error</strong></div>');
                    }
                }
            });
        }
    });
    /* PROVEEDOR */

    /* FACTURA */
    $("#JSV_AddFactura").validate({
        rules: {
            factu_folio: {required: true, digits: true},
            factu_fecha_emision: {required: true},
            factu_iva: {required: true, digits: true},
            factu_monto_neto: {required: true, digits: true},
            factu_imp_adc: {required: true, digits: true},
            factu_total: {required: true, digits: true},
            factu_condicion_pago: {required: true}
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorClass: 'help-block',
        messages: {
        },
        submitHandler: function (form) {
            $('#msj_respuesta').html('<img src="img/ajax-loader.gif" alt="" />');
            $.ajax({
                type: "POST",
                url: "./modelo/MOD.AddFactura.php",
                data: $(form).serialize(),
                success: function (data) {
                    if (data == "1") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-success no-margin" style="font-size: 12px;"><strong><i class="fa fa-check-circle-o" style="color: green;"></i> Registro Guardado Exitosamente.</strong></div>');
                        window.location.replace("./facturas.php");
                    } else if (data == "0") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "2") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Usuario ya se encuentra registrado!</strong></div>');
                    } else if (data == "3") {
                        $('#msj_respuesta').fadeIn(1000).html('<div class="alert alert-danger"><strong><i class="fa fa-times-circle-o fa-lg fa-2x" style="color: red;"></i> Faltan campos por rellenar</strong></div>');
                    }
                }
            });
        }
    });
    /* FACTURA */





});
