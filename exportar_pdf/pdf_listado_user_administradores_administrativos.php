<?php

require_once('./PDF_MySQL_Table.php');

class PDF extends PDF_MySQL_Table {

    function Header() {
        //Title
        $this->SetFont('Arial', 'B', 18);
        $this->Cell(200, 30, "Listado de Usuarios Administradores y Administrativos", 0, 0, 'C');
        $this->Ln(16);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(0, 23, "Fecha: " . date('d/m/Y'), 0, 0, 'C');
        //$this->Image("../img/logo_citt.png", 10, 5, 40);
        $this->Ln(25);
        //Ensure table header is output
        parent::Header();
    }

    function Footer() {
        // Go to 1.5 cm from bottom
        $this->SetY(-15);
        // Select Arial italic 8
        $this->SetFont('Arial', 'B', 8);
        // Print centered page number
        $this->Cell(0, 10, utf8_decode('Página ') . $this->PageNo(), 0, 0, 'C');
        $this->Ln(5);
        //$this->Cell(0,10, 'EFICIENCIA Y SEGURIDAD PARA TU '.utf8_decode('INFORMACIÓN'),0,0,'C');
        //$this->Cell(0, 10, 'CONTROL DE ACCESO C.I.T.T.', 0, 0, 'C');
        $this->SetTextColor(0, 0, 255);
        $this->SetFont('', 'U');
        //$this->Write(2,'NFConnection','http://www.nfconnection.cl');
    }

}

require_once '../controlador/paramentros_conexion_pdf_excel.php';
$pdf = new PDF();
$pdf->AddPage('P', 'Letter');

$pdf->AddCol('user_rut', 27, 'RUT', 'C');
$pdf->AddCol('user_nombre', 30, 'Nombre', 'C');
$pdf->AddCol('user_ap', 40, 'Apellido Paterno', 'C');
$pdf->AddCol('user_am', 40, 'Apellido Materno', 'C');
$pdf->AddCol('user_genero', 22, 'Genero', 'C');

//$pdf->AddCol('user_ano_ingreso',30, utf8_decode('Año Ingreso'),'C');
//$pdf->AddCol('user_carrera',40,'Carrera','C');

$prop = array('HeaderColor' => array(248, 148, 6),
    'color1' => array(255, 255, 255),
    'color2' => array(220, 220, 220,),
    'padding' => 2);
$sentencia_sql_llena_tabla = "SELECT * FROM Usuarios U,Perfiles P, Tag T, Login L WHERE T.tag_id = U.Tag_tag_id AND P.perfil_id = U.Perfiles_perfil_id AND L.Usuarios_user_id = U.user_id";

$pdf->Table($sentencia_sql_llena_tabla, $prop);
$pdf->Output('Listado de Usuarios Inscritos - ' . date('d/m/Y'), 'I');
mysql_close();
?>