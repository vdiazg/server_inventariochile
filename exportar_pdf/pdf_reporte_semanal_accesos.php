<?php
require_once('./PDF_MySQL_Table.php');
require_once('./jpgraph/src/jpgraph.php');
require_once('./jpgraph/src/jpgraph_bar.php');
require_once '../controlador/paramentros_conexion_pdf_excel.php';
session_start();
$fecha_inicio = $_SESSION["fecha_inicio"];
$fecha_termino = $_SESSION["fecha_termino"];
class PDF extends PDF_MySQL_Table {

    function Header() {
        //Title
        $this->SetFont('Arial', 'B', 18);
        $this->Cell(0,23, "Reporte Semanal", 0, 0, 'C');
        $this->Ln(16);
        $this->SetFont('Arial', 'B', 10);
        //$this->Cell(0, 23, "Fecha: " . date('d/m/Y'), 0, 0, 'C');


        //$this->Cell(0, 23, "zzzz: " . $_SESSION["reporte_mensual_numero_mes"], 0, 0, 'C');
        $this->Image("../img/logo_citt.png", 10, 10, -700);
        $this->Ln(25);
        //Ensure table header is output
        parent::Header();
    }

    function Footer() {
        // Go to 1.5 cm from bottom
        $this->SetY(-15);
        // Select Arial italic 8
        $this->SetFont('Arial', 'B', 8);
        // Print centered page number
        $this->Cell(0, 10, utf8_decode('Página ') . $this->PageNo(), 0, 0, 'C');
        $this->Ln(5);
        //$this->Cell(0,10, 'EFICIENCIA Y SEGURIDAD PARA TU '.utf8_decode('INFORMACIÓN'),0,0,'C');
        //$this->Cell(0, 10, 'CONTROL DE ACCESO C.I.T.T.', 0, 0, 'C');
        $this->SetTextColor(0, 0, 255);
        $this->SetFont('', 'U');
        //$this->Write(2,'NFConnection','http://www.nfconnection.cl');
    }

    function CreaGraficoPDF() {
        require_once '../controlador/Db.class.php';
        require_once '../modelo/array_carreras.php';
        global $fecha_inicio;
        global $fecha_termino;
        
        $db = new Db();
        reset($carreras);
        foreach ($carreras as $valor):

            /*$resultado = $db->query("SELECT COUNT(registro_id) as count_registros, user_carrera, registro_acceso FROM Registros R, Usuarios U WHERE R.Usuarios_user_id = U.user_id AND U.user_carrera = '$valor' AND registro_fecha >= '$fecha_inicio' AND registro_fecha <= '$fecha_termino'");

            foreach ($resultado as $row):
                $cant_registros_por_carrera = $row["count_registros"];
                $nombre_de_carrera = $row["user_carrera"];*/
                $CantidadAcessos = $db->single("SELECT COUNT(registro_acceso) as count_AcessosEntradas FROM Registros R, Usuarios U WHERE R.registro_acceso = 'Entrada' AND U.user_carrera = '$valor' AND R.Usuarios_user_id = U.user_id AND R.registro_fecha >= '$fecha_inicio' AND R.registro_fecha <= '$fecha_termino'");
                if ($CantidadAcessos != 0) {
                    $nombre_carrera_grafico[] = $db->single("SELECT user_carrera FROM Registros R, Usuarios U WHERE R.registro_acceso = 'Entrada' AND U.user_carrera = '$valor' AND R.Usuarios_user_id = U.user_id");
                    $CantidadAcessosEntrada[] = $db->single("SELECT COUNT(registro_acceso) as count_AcessosEntradas FROM Registros R, Usuarios U WHERE R.registro_acceso = 'Entrada' AND U.user_carrera = '$valor' AND R.Usuarios_user_id = U.user_id AND R.registro_fecha >= '$fecha_inicio' AND R.registro_fecha <= '$fecha_termino'");
                    $CantidadAcessosSalida[] = $db->single("SELECT COUNT(registro_acceso) as count_AcessosSalida FROM Registros R, Usuarios U WHERE R.registro_acceso = 'Salida' AND U.user_carrera = '$valor' AND R.Usuarios_user_id = U.user_id AND R.registro_fecha >= '$fecha_inicio' AND R.registro_fecha <= '$fecha_termino'");
                }
            //endforeach;
        endforeach;

        // Creamos el grafico
        $grafico = new Graph(550, 500, 'auto');
        $grafico->SetScale("textint");
        $grafico->title->Set("Accesos Semanal");
        $grafico->xaxis->title->Set("Carreras");
        $grafico->xaxis->SetTickLabels($nombre_carrera_grafico);
        $grafico->yaxis->title->Set("Catidad de Accesos");
        $barplot1 = new BarPlot($CantidadAcessosEntrada);
        $barplot2 = new BarPlot($CantidadAcessosSalida);
        $gbplot = new GroupBarPlot(array($barplot1, $barplot2));
        $grafico->Add($gbplot);
        $barplot1->SetColor("white");
        $barplot1->SetFillColor("#f89406");
        $barplot2->SetColor("white");
        $barplot2->SetFillColor("#bdc3c7");
        // 30 pixeles de ancho para cada barra
        $barplot1->SetWidth(30);
        $barplot2->SetWidth(30);
        $grafico->Stroke("Grafico_reporte_mensual.png");
        $this->Image("Grafico_reporte_mensual.png", 35, 50);
    }

}

$pdf = new PDF();
$pdf->AddPage('L', 'Letter');

$pdf->AddCol('user_rut', 27, 'RUT', 'C');
$pdf->AddCol('user_nombre', 30, 'Nombre', 'C');
$pdf->AddCol('user_ap', 40, 'Apellido Paterno', 'C');
$pdf->AddCol('user_am', 40, 'Apellido Materno', 'C');
$pdf->AddCol('user_genero', 22, 'Genero', 'C');
$pdf->AddCol('user_carrera',60,'Carrera','C');
//$pdf->AddCol('user_ano_ingreso',30, utf8_decode('Año Ingreso'),'C');


$prop = array('HeaderColor' => array(248, 148, 6),
    'color1' => array(255, 255, 255),
    'color2' => array(220, 220, 220,),
    'padding' => 2);
$sentencia_sql_llena_tabla = "SELECT DISTINCT user_rut, user_nombre, user_ap, user_am, user_genero, user_carrera FROM Perfiles P, Registros R, Usuarios U WHERE P.perfil_id = U.Perfiles_perfil_id AND P.perfil_nombre = 'ALUMNO' AND R.Usuarios_user_id = U.user_id AND R.registro_fecha >= '$fecha_inicio' AND R.registro_fecha <= '$fecha_termino' ORDER BY user_ap";

$pdf->Table($sentencia_sql_llena_tabla, $prop);
$pdf->Text(128,48,"* Registros de personas que accesaron en la semana, entre ".$fecha_inicio." y ".$fecha_termino);
$pdf->AddPage('P', 'Letter');
$pdf->CreaGraficoPDF();
date_default_timezone_set("America/Santiago");
$pdf->Output('Reporte Semanal de Accesos por Carrera_' . date('d/m/Y_H:i:s').'.pdf', 'I');
mysql_close();
?>