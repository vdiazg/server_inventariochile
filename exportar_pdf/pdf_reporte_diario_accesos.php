<?php
require_once('./PDF_MySQL_Table.php');
require_once('./jpgraph/src/jpgraph.php');
require_once('./jpgraph/src/jpgraph_bar.php');
require_once '../controlador/paramentros_conexion_pdf_excel.php';

session_start();
$numero_mes = $_SESSION["reporte_diario_numero_mes"];
        
class PDF extends PDF_MySQL_Table {
    
    
    function Header() {
        //Title
        $this->SetFont('Arial', 'B', 18);
        $this->Cell(0,23, "Reporte Diario", 0, 0, 'C');
        $this->Ln(16);
        $this->SetFont('Arial', 'B', 10);
        //$this->Cell(0, 23, "Fecha: " . date('d/m/Y'), 0, 0, 'C');


        //$this->Cell(0, 23, "zzzz: " . $_SESSION["reporte_mensual_numero_mes"], 0, 0, 'C');
        $this->Image("../img/logo_citt.png", 10, 10, -700);
        $this->Ln(25);
        //Ensure table header is output
        parent::Header();
    }

    function Footer() {
        // Go to 1.5 cm from bottom
        $this->SetY(-15);
        // Select Arial italic 8
        $this->SetFont('Arial', 'B', 8);
        // Print centered page number
        $this->Cell(0, 10, utf8_decode('Página ') . $this->PageNo(), 0, 0, 'C');
        $this->Ln(5);
        //$this->Cell(0,10, 'EFICIENCIA Y SEGURIDAD PARA TU '.utf8_decode('INFORMACIÓN'),0,0,'C');
        //$this->Cell(0, 10, 'CONTROL DE ACCESO C.I.T.T.', 0, 0, 'C');
        $this->SetTextColor(0, 0, 255);
        $this->SetFont('', 'U');
        //$this->Write(2,'NFConnection','http://www.nfconnection.cl');
    }

    function CreaGraficoPDF() {
        require_once '../controlador/Db.class.php';
        require_once '../modelo/array_carreras.php';
        /*session_start();
        
        $numero_mes = $_SESSION["reporte_diario_numero_mes"];*/
        global $numero_mes; 
        
        $db = new Db();
        reset($carreras);
        foreach ($carreras as $valor):

            /*$resultado = $db->query("SELECT *,COUNT(registro_id) as count_registros FROM Registros R, Usuarios U WHERE R.Usuarios_user_id = U.user_id AND U.user_carrera = '$valor' AND registro_fecha = STR_TO_DATE('$numero_mes','%d-%m-%Y')");

            foreach ($resultado as $row):
                $cant_registros_por_carrera = $row["count_registros"];
                $nombre_de_carrera = $row["user_carrera"];
                $registro_hora = $row["registro_hora"];*/
                $CantidadAcessos = $db->single("SELECT COUNT(registro_acceso) as count_AcessosEntradas FROM Registros R, Usuarios U WHERE R.registro_acceso = 'Entrada' AND U.user_carrera = '$valor' AND R.Usuarios_user_id = U.user_id AND R.registro_fecha = STR_TO_DATE('$numero_mes','%d-%m-%Y')");
                if ($CantidadAcessos != 0) {
                    //$registro_hora_array[] = $db->single("SELECT registro_hora FROM Registros R, Usuarios U WHERE R.registro_acceso = 'Entrada' AND U.user_carrera = '$valor' AND R.Usuarios_user_id = U.user_id AND R.registro_fecha = STR_TO_DATE('$numero_mes','%d-%m-%Y')");
                    $nombre_carrera_grafico[] = $db->single("SELECT user_carrera FROM Registros R, Usuarios U WHERE R.registro_acceso = 'Entrada' AND U.user_carrera = '$valor' AND R.Usuarios_user_id = U.user_id AND R.Usuarios_user_id = U.user_id AND R.registro_fecha = STR_TO_DATE('$numero_mes','%d-%m-%Y')");
                    $CantidadAcessosEntrada[] = $db->single("SELECT COUNT(registro_acceso) as count_AcessosEntradas FROM Registros R, Usuarios U WHERE R.registro_acceso = 'Entrada' AND U.user_carrera = '$valor' AND R.Usuarios_user_id = U.user_id AND R.registro_fecha = STR_TO_DATE('$numero_mes','%d-%m-%Y')");
                    $CantidadAcessosSalida[] = $db->single("SELECT COUNT(registro_acceso) as count_AcessosSalida FROM Registros R, Usuarios U WHERE R.registro_acceso = 'Salida' AND U.user_carrera = '$valor' AND R.Usuarios_user_id = U.user_id AND R.registro_fecha = STR_TO_DATE('$numero_mes','%d-%m-%Y')");
                }
            //endforeach;
        endforeach;

        // Creamos el grafico
        $grafico = new Graph(550, 500, 'auto');
        $grafico->SetScale("textint");
        $grafico->title->Set("Acceso Diario");
        $grafico->xaxis->title->Set("Carreras");
        $grafico->xaxis->SetTickLabels($nombre_carrera_grafico);
        $grafico->yaxis->title->Set("Catidad de Accesos");
        $barplot1 = new BarPlot($CantidadAcessosEntrada);
        $barplot2 = new BarPlot($CantidadAcessosSalida);
        $gbplot = new GroupBarPlot(array($barplot1, $barplot2));
        $grafico->Add($gbplot);
        $barplot1->SetColor("white");
        $barplot1->SetFillColor("#f89406");
        $barplot2->SetColor("white");
        $barplot2->SetFillColor("#bdc3c7");
        // 30 pixeles de ancho para cada barra
        $barplot1->SetWidth(30);
        $barplot2->SetWidth(30);
        $grafico->Stroke("Grafico_reporte_mensual.png");
        $this->Image("Grafico_reporte_mensual.png", 35, 50);
    }


}
$pdf = new PDF();

$pdf->AddPage('L', 'Letter');
$pdf->AddCol('user_rut', 27, 'RUT', 'C');
$pdf->AddCol('user_nombre', 30, 'Nombre', 'C');
$pdf->AddCol('user_ap', 40, 'Apellido Paterno', 'C');
$pdf->AddCol('user_am', 40, 'Apellido Materno', 'C');
$pdf->AddCol('user_genero', 22, 'Genero', 'C');
$pdf->AddCol('user_carrera',60,'Carrera','C');
//$pdf->AddCol('registro_acceso',20,'Acceso','C');
$prop = array('HeaderColor' => array(248, 148, 6),
    'color1' => array(255, 255, 255),
    'color2' => array(220, 220, 220,),
    'padding' => 2);

$sentencia_sql_llena_tabla_ENTRADAS = "SELECT DISTINCT user_rut, user_nombre, user_ap, user_am, user_genero, user_carrera FROM Perfiles P, Registros R, Usuarios U WHERE P.perfil_id = U.Perfiles_perfil_id AND P.perfil_nombre = 'ALUMNO' AND R.Usuarios_user_id = U.user_id AND R.registro_fecha = STR_TO_DATE('$numero_mes','%d-%m-%Y') ORDER BY user_ap";
//$sentencia_sql_llena_tabla = "SELECT * FROM Registros R, Usuarios U WHERE R.registro_acceso = 'Entrada'  AND R.Usuarios_user_id = U.user_id AND R.registro_fecha = STR_TO_DATE('27-11-2014','%d-%m-%Y')";
$pdf->Table($sentencia_sql_llena_tabla_ENTRADAS, $prop);
$pdf->Text(168,48,"* Registros de personas que accesaron el dia ".$numero_mes);
///////////////////////////////////////////////////////////////////////
/*$sentencia_sql_llena_tabla_SALIDAS = "SELECT * FROM Registros R, Usuarios U WHERE R.registro_acceso = 'Salida' AND R.Usuarios_user_id = U.user_id AND R.registro_fecha = STR_TO_DATE('$numero_mes','%d-%m-%Y') ORDER BY user_ap";
//$pdf->AddPage('L', 'Letter');
$pdf->AddCol('user_rut', 27, 'RUT', 'C');
$pdf->AddCol('user_nombre', 30, 'Nombre', 'C');
$pdf->AddCol('user_ap', 40, 'Apellido Paterno', 'C');
$pdf->AddCol('user_am', 40, 'Apellido Materno', 'C');
$pdf->AddCol('user_genero', 22, 'Genero', 'C');
$pdf->AddCol('user_carrera',60,'Carrera','C');
$pdf->AddCol('registro_acceso',20,'Acceso','C');
$prop2 = array('HeaderColor' => array(248, 148, 6),
    'color1' => array(255, 255, 255),
    'color2' => array(220, 220, 220,),
    'padding' => 2);
//$pdf->AddPage('L', 'Letter');
$pdf->Table($sentencia_sql_llena_tabla_SALIDAS, $prop2);
*/
$pdf->AddPage('P', 'Letter');
$pdf->CreaGraficoPDF();
date_default_timezone_set("America/Santiago");
$pdf->Output('Reporte Menusal de Accesos por Carrera_' . date('YmdHis').'.pdf', 'I');
mysql_close();

?>