<?php
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UserNameUsuario = $_SESSION["datos_usuario_logueado"][10];
// tomo la id a modificar
$id = filter_input(INPUT_GET, 'id');
if (!isset($IDUsuario)) {
    header('Location: login.php');
}

require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	

        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="dashboard.php"> Home</a></li>
                        <li class="active">Editar Información de Empresa</li>	 
                    </ul>
                </div>
                <form class="form-login paddingTB-md" id="JSV_ModificarEmpresa" method="POST">
                    <!-- INFORMACION PERSONAL -->

                    <div class="panel panel-default table-responsive ">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-4">
                                    <i class="fa fa-plus-circle"></i> EDITAR INFORMACIÓN DE EMPRESA
                                </div>
                                <div class="col-md-8">
                                    <div id="msj_respuesta"></div>
                                </div>
                            </div>
                        </div>
                        <div class="padding-md clearfix">
                            <!-- INICIO Código dentro cuadro blanco -->
                            <div class="panel-body">

                                <?php
                                // copy de empresas
                                $SQL_DATA_SELECT_EMPRESAS = $db->query("SELECT * FROM Empresas WHERE empresa_id = '" . $id . "'");
                                //asignamos las variables de la bd a unas de php
                                foreach ($SQL_DATA_SELECT_EMPRESAS as $row) {
                                    $EMPRESA_RUT = $row["empresa_rut"];
                                    $EMPRESA_RAZON_SOCIAL = $row["empresa_razon_social"];
                                    $EMPRESA_GIRO = $row["empresa_giro"];
                                    $EMPRESA_DIRECCION = $row["empresa_direccion"];
                                    $EMPRESA_TEL_MOVIL = $row["empresa_tel_movil"];
                                    $EMPRESA_TEL_FIJO = $row["empresa_tel_fijo"];
                                }
                                ?>

                                <div class="row">
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" >Rut Empresa</label><input id="emp_rut" name="emp_rut" type="text" placeholder="" class="form-control input-sm" value='<?= $EMPRESA_RUT ?>'></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" >Razon Social</label><input id="emp_razon" name="emp_razon" type="text" placeholder="" class="form-control input-sm" value='<?= $EMPRESA_RAZON_SOCIAL ?>'></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" >Giro</label><input id="emp_giro" name="emp_giro" type="text" placeholder="" class="form-control input-sm" value='<?= $EMPRESA_GIRO ?>'></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" >Dirección Empresa</label><input id="emp_direccion" name="emp_direccion" type="text" placeholder="" class="form-control input-sm" value='<?= $EMPRESA_DIRECCION ?>'></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" >Telefono Movil Empresa</label><input id="emp_tel" name="emp_tel" type="text" placeholder="" class="form-control input-sm" value='<?= $EMPRESA_TEL_MOVIL ?>'></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" >Telefono Fijo Empresa</label><input id="emp_fijo" name="emp_fijo" type="text" placeholder="" class="form-control input-sm" value='<?= $EMPRESA_TEL_FIJO ?>'></div></div>
                                </div>

                                <!-- BOTON GUARDAR -->
                                <div class="row">                                        
                                    <div class="col-md-12">
                                        <button type="button" id="modificar_empresa_boton_cancel" class="btn btn-default pull-left"><i class="fa fa-times fa-2x"></i> <br/>Cancelar</button>
                                        <button type="submit" class="btn btn-default pull-right"><i class="fa fa-save fa-2x"></i> <br/>Guardar</button>
                                    </div>
                                </div>
                                <!-- FIN BOTON GUARDAR -->
                            </div>
                            <!-- FIN  Código dentro cuadro blanco -->
                        </div>
                    </div><!-- /panel -->
                    <!-- FIN INFORMACION PERSONAL -->
                </form>
            </div>
        </div>

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

<?php require_once './includes/footer.inc.php'; ?>

        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!--Bootstrap-->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src="js/js-nfconnection/jquery.validate.min.js"></script>
        <script src="js/js-nfconnection/custom_jquery_validate.js"></script>

        <script src="js/menu-active-class.js"></script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Bootstrap Switch -->
        <!--<script src='js/js-nfconnection/bootstrap-switch.js'></script>-->

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

        <script> //Script que al presionar "cancelar" devuelve a la pagina anterior
            $("#modificar_empresa_boton_cancel").click(function () {
                window.location.replace("./empresas.php");
            });
        </script>
    </body>
</html>