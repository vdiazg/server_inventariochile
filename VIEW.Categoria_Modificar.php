<?php
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UserNameUsuario = $_SESSION["datos_usuario_logueado"][10];
$id = filter_input(INPUT_GET, 'id');
if (!isset($IDUsuario)) {
    header('Location: login.php');
}

require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="dashboard.php"> Home</a></li>
                        <li class="active">Editar Categoria</li>	 
                    </ul>
                </div>
                <form class="form-login paddingTB-md" id="JSV_ModificarCategoria" method="POST">
                    <!-- INFORMACION PERSONAL -->

                    <div class="panel panel-default table-responsive ">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-4">
                                    <i class="fa fa-plus-circle"></i> DATOS DE LA CATEGORÍA
                                </div>
                                <div class="col-md-8">
                                    <div id="msj_respuesta"></div>
                                </div>
                            </div>
                        </div>
                        <div class="padding-md clearfix">
                            <!-- INICIO Código dentro cuadro blanco -->
                            <div class="panel-body">
                                <?php
                                $SQL_DATA_SELECT_BODEGAS = $db->query("SELECT * FROM ProductCategorias WHERE productcategorias_id = '" . $id . "'");

                                foreach ($SQL_DATA_SELECT_BODEGAS as $row):

                                    $id = $row["productcategorias_id"];
                                    $Nombre = $row["productcategorias_nombre"];
                                    $referencia = $row["productcategorias_cod_ref"];
                                    $descripcion = $row["productcategorias_descripcion"];

                                endforeach;
                                ?>
                                <div class="row">
                                    <div class="col-md-4"><div class="form-group"><label class="control-label" fon="cate_ref">Referencia</label><input id="cate_ref" name="cate_ref" type="text" placeholder="" class="form-control input-sm" value='<?= $referencia ?>'></div></div>
                                    <div class="col-md-4"><div class="form-group"><label class="control-label" for="cate_nom">Nombre</label><input id="cate_nom" name="cate_nom" value='<?= $Nombre ?>' type="text" placeholder="" class="form-control input-sm"></div></div>
                                    <div class="col-md-4"><div class="form-group"><label class="control-label" for="cate_descrip">Descripción</label><input id="cate_descrip" name="cate_descrip"  value= '<?= $descripcion ?>' type="text" placeholder="" class="form-control input-sm"></div></div>
                                    <input id="id" name="id"  value='<?= $id ?>' type="hidden" placeholder="" class="form-control input-sm">
                                </div>
                                <hr>
                                <!-- BOTON GUARDAR -->
                                <div class="row">                                        
                                    <div class="col-md-12">
                                        <button type="button" id="modificar_categoria_boton_cancelar" class="btn btn-default pull-left"><i class="fa fa-times fa-2x"></i> <br/>Cancelar</button>
                                        <button type="submit" class="btn btn-default pull-right"><i class="fa fa-save fa-2x"></i> <br/>Guardar</button>
                                    </div>
                                </div>
                                <!-- FIN BOTON GUARDAR -->
                            </div>
                            <!-- FIN  Código dentro cuadro blanco -->
                        </div>
                    </div><!-- /panel -->
                    <!-- FIN INFORMACION PERSONAL -->
                </form>
            </div>
        </div>

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <?php require_once './includes/footer.inc.php'; ?>

        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!--Bootstrap-->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src="js/js-nfconnection/jquery.validate.min.js"></script>
        <script src="js/js-nfconnection/custom_jquery_validate.js"></script>

        <script src="js/menu-active-class.js"></script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Bootstrap Switch -->
        <!--<script src='js/js-nfconnection/bootstrap-switch.js'></script>-->

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

        <script> //Script que al presionar "cancelar" devuelve a la pagina anterior
            $("#modificar_categoria_boton_cancelar").click(function () {
                window.location.replace("./categorias.php");
            });
        </script>

    </body>
</html>