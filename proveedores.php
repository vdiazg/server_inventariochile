<?php
error_reporting( error_reporting() & ~E_NOTICE );
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UsernameUsuario = $_SESSION["datos_usuario_logueado"][10];

if (!isset($IDUsuario)) {
    header('Location: login.php');
}
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="dashboard.php"> Home</a></li>
                        <li class="active">Proveedores</li>	 
                    </ul>
                </div><!-- breadcrumb -->
                <br/>
                <div class="row paddingLR-md">
                    <div class="col-md-12 ">
                        <a href="./add_proveedor.php" class="btn btn-success pull-right"><span class="fa fa-plus-circle"></span> Añadir Nuevo Proveedor</a>
                    </div>                    
                </div>
                <div class="padding-md">
                    <div class="panel panel-default table-responsive">
                        <div class="panel-heading">                            
                            <div class="row">
                                <div class="col-md-6"><i class="fa fa-list-alt"></i> PROVEEDORES <span class="badge badge-info"><?php echo $db->single("SELECT COUNT(*) FROM Proveedores"); ?></span></div>
                                <div class="col-md-6 ">
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown"><span class="fa fa-download"></span> Descargar <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" target="_blank" class=" bounceIn"  ><span class="fa fa-file-pdf-o fa-2x"></span> (.pdf)</a></li>
                                            <li><a href="#" target="_blank" class=" bounceIn"  ><span class="fa fa-file-excel-o fa-2x"></span> (.xlsx)</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="padding-md clearfix">
                            <table class="table table-striped" id="dataTable" style="font-size: 12px;"> 
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">RUT</th>
                                        <th class="text-center">Razon Social</th>
                                        <th class="text-center">Giro</th>
                                        <th class="text-center">Dirección</th>
                                        <th class="text-center">Teléfono Móvil</th>
                                        <th class="text-center">Teléfono Fijo</th>
                                        <th class="text-center">Modificar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    $SQL_DATA_SELECT_PROVEEDOR = $db->query("SELECT * FROM Proveedores");
                                    
                                    foreach ($SQL_DATA_SELECT_PROVEEDOR as $row):
                                        
                                        $PROVEE_ID_BD      = $row["proveedor_id"];
                                        $PROVEE_RUT        = $row["proveedor_rut"];
                                        $PROVEE_RAZON      = $row["proveedor_razon_social"];
                                        $PROVEE_GIRO       = $row["proveedor_giro"];
                                        $PROVEE_DIRECCION  = $row["proveedor_direccion"];
                                        $PROVEE_TEL_MOVIL  = $row["proveedor_tel_movil"];
                                        $PROVEE_TEL_FIJO   = $row["proveedor_tel_fijo"];
                                    
                                        echo '<tr>';
                                        echo '<td class="text-center">' . $PROVEE_ID_BD . '</td>';
                                        echo '<td class="text-center">' . $PROVEE_RUT .'</td>';
                                        echo '<td class="text-center">' . $PROVEE_RAZON . '</td>';
                                        echo '<td class="text-center">' . $PROVEE_GIRO . '</td>';
                                        echo '<td class="text-center">' . $PROVEE_DIRECCION . '</td>';
                                        echo '<td class="text-center">' . $PROVEE_TEL_MOVIL . '</td>';
                                        echo '<td class="text-center">' . $PROVEE_TEL_FIJO . '</td>';
                                        echo "<td class='text-center'>
                                            <div class='btn-group'>
                                                <a href='#' class='btn btn-default dropdown-toggle btn-xs' onclick=\"cargarModalModificarUsuario('$IDUsuarioEmpleado','$TagID','$NTagAsociadoEmpleado','" . $row["tag_tipo"] . "','$NombreEmpleado','$ApellidoPaternoEmpleado','$ApellidoMaternoEmpleado','" . $row["tag_estado"] . "');\"><i class='fa fa-edit fa-lg'></i> Modificar </a>
                                                <a href='#' class='btn btn-default dropdown-toggle btn-xs' data-toggle='dropdown'><span class='caret'></span></a>
                                                <ul class='dropdown-menu slidedown'>
                                                    <li><a href='#'><i class='fa fa-trash-o fa-lg'></i> Eliminar</a></li>
                                                </ul>
                                            </div>
                                        </td>";
                                        echo '</tr>';
                                        endforeach;
                                    ?>         
                                    
                                </tbody>
                            </table>
                        </div><!-- /.padding-md -->
                    </div><!-- /panel -->
                </div>
            </div><!-- /main-container -->
        </div><!-- /wrapper -->
        
        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
        <?php require_once './includes/footer.inc.php'; ?>
        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>
        <!-- Eliminar Tag confirmation -->
        <?php require_once './includes/eliminar_tag_confim.inc.php'; ?>

        
        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        
        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src='js/jquery.dataTables.min.js'></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#dataTable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "asStripClasses": null
                });
                $('#refrescar_pagina').click(function() {
                    location.reload();
                });
            });
            function cargarModalModificarUsuario(IDUsuarioEmpleado, TagID, NTagAsociadoEmpleado, TagTipo, NombreEmpleado, ApellidoPaternoEmpleado, ApellidoMaternoEmpleado, TagEstado) {
                $('#div_estado_tag').show();
                    $('#div_Col_EliminarTag').show();
                    $("#switch-state").bootstrapSwitch();
                /*if(IDUsuarioEmpleado == <?php echo $IDUsuario; ?>){
                    $('#div_estado_tag').hide();
                    $('#div_Col_EliminarTag').hide();                    
                    $("#id_btn_guardar").off('click');
                    UpTag(IDUsuarioEmpleado, TagID, NTagAsociadoEmpleado, TagTipo, NombreEmpleado, ApellidoPaternoEmpleado, ApellidoMaternoEmpleado, TagEstado);
                }else{
                    $('#div_estado_tag').show();
                    $('#div_Col_EliminarTag').show();
                    $("#switch-state").bootstrapSwitch();                    
                }*/
                if(TagEstado == "Habilitado"){
                    //$('#div_estado_tag').show();
                }else{
                    $("#id_btn_guardar").off('click');
                    //$('#div_estado_tag').hide();
                    $('#div_estado_tag').show();
                    $('#div_Col_EliminarTag').show();
                }
                UpTag(IDUsuarioEmpleado, TagID, NTagAsociadoEmpleado, TagTipo, NombreEmpleado, ApellidoPaternoEmpleado, ApellidoMaternoEmpleado, TagEstado);
                $('#usuario_asociado_tag').val(NombreEmpleado+" "+ApellidoPaternoEmpleado+" "+ApellidoMaternoEmpleado);
                $('#Update_num_tag').val(NTagAsociadoEmpleado);
                $('#id_span_elimin_numero_tag').html(NTagAsociadoEmpleado);                
                $('#Update_tipo_tag').val(TagTipo);
                $('#Update_usuarios_sistema').modal('show');
            }
            function UpTag(IDUsuarioEmpleado, TagID, NTagAsociadoEmpleado, TagTipo, NombreEmpleado, ApellidoPaternoEmpleado, ApellidoMaternoEmpleado, TagEstado){
                $("#id_btn_guardar").click(function() {
                        if ($('#switch-state').is(':checked')) {
                            EstadoTag = "Habilitado";
                        }else{
                            EstadoTag = "Deshabilitado";
                        }
                        $.ajax({
                            type: "POST",
                            url: "./modelo/update_tags.php",
                            data: $("#form_Update_tags").serialize()+
                            "&EstadoTag="+EstadoTag+
                            "&TagID="+TagID,
                            success: function (data) {
                                //alert(data);
                                $('#msj_respuesta_update_tag').html(data);
                            }
                        });
                        //alert(EstadoUsuarioCheckbox+" "+EstadoTag);
                    });                
                $("#eliminar_tag").click(function() {
                        if ($('#switch-state').is(':checked')) {
                            EstadoTag = "Habilitado";
                        }else{
                            EstadoTag = "Deshabilitado";
                        }
                        $.ajax({
                            type: "POST",
                            url: "./modelo/eliminar_tags.php",
                            data: $("#form_Update_tags").serialize()+
                            "&EstadoTag="+EstadoTag+
                            "&TagID="+TagID,
                            success: function (data) {
                                //alert(data);
                                $('#msj_respuesta_elminar_tag').html(data);
                            }
                        });
                        //alert(EstadoUsuarioCheckbox+" "+EstadoTag);
                    });
            }
        </script>
        <script src="js/menu-active-class.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#a_btn_genera_pdf').on('click', function (e) {
                    window.open('./exportar_pdf/pdf_keys_tag.php','_blank');
                });
                $('#a_btn_genera_excel').on('click', function (e) {
                    window.open('./exportar_excel/excel_keys_tags.php','_blank');
                });
            });
        </script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Bootstrap Switch -->
        <script src='js/js-nfconnection/bootstrap-switch.js'></script>
    
        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

    </body>
</html>


