<?php
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UserNameUsuario = $_SESSION["datos_usuario_logueado"][10];
$id = filter_input(INPUT_GET, 'id');
if (!isset($IDUsuario)) {
    header('Location: login.php');
}

require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="dashboard.php"> Home</a></li>
                        <li class="active">Modificar Producto</li>	 
                    </ul>
                </div>
                <form class="form-login paddingTB-md" id="JSV_ModificarProducto" method="POST">
                    <!-- INFORMACION PERSONAL -->
                    <div class="panel panel-default table-responsive ">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-4">
                                    <i class="fa fa-plus-circle"></i> EDITAR DATOS DEL PRODUCTO
                                </div>
                                <div class="col-md-8">
                                    <div id="msj_respuesta"></div>
                                </div>
                            </div>
                        </div>
                        <div class="padding-md clearfix">
                            <!-- INICIO Código dentro cuadro blanco -->
                            <div class="panel-producty">


                                <?php
                                $SQL_DATA_SELECt = $db->query("SELECT * FROM Productos WHERE producto_id ='" . $id . "'");

                                foreach ($SQL_DATA_SELECt as $row) {

                                    $id = $row["producto_id"];
                                    $marca = $row["producto_marca"];
                                    $ean13 = $row["producto_ean13"];
                                    $jan = $row["producto_cod_jan"];
                                    $upc = $row["producto_cod_upc"];
                                    $corta = $row["producto_descrip_corta"];
                                    $larga = $row["producto_descrip_larga"];
                                    $bodega = $row["Bodegas_bodega_id"];
                                    $categoria = $row["ProductCategorias_productcategorias_id"];
                                    $nombre = $row["producto_nombre"];
                                    $PRODUCTO_IMG = $row["producto_ruta_img"];
                                    $PRODUCTO_PRECIO = $row["producto_precio"];
                                    $PRODUCTO_CANTIDAD = $row["producto_cantidad"];
                                    $ref = $row["producto_cod_ref"];
                                }
                                ?>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-12">                                        
                                                <div id="error_img"></div>
                                                <div class="form-group">                                        
                                                    <div class="col-md-12">
                                                        <div id="contenedorImagen">
                                                            <div class="row">
                                                                <div class="form-group pull-left">
                                                                    <label class="control-label">Imagen del Producto</label>                                                            
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <img id="fotografia" class="img-thumbnail padding-xs" src="<?php echo $PRODUCTO_IMG; ?>" style="width: 150px; height: 150px;" />
                                                            </div>
                                                            <div class="row" style="margin-top: 15px;">
                                                                <button class="btn btn-default btn-block btn-social" id="addImage"><i class="fa fa-upload"></i><b>Imagen de perfil</b></button>
                                                                <div class="" style="display: none;" id="loaderAjax">
                                                                    <img src="img/ajax-loader.gif">
                                                                    <span>Publicando Fotografía...</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8"></div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="product_nom">Nombre</label>
                                            <input id="product_nom" name="product_nom" value= <?= $nombre ?> type="text" placeholder="" class="form-control input-sm">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" fon="product_marca">Marca</label>
                                            <input id="product_marca" name="product_marca" value=<?= $marca ?> type="text" placeholder="" class="form-control input-sm">
                                        </div>
                                    </div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="product_cod_ean13">EAN-13</label><input id="product_cod_ean13" name="product_cod_ean13" value='<?= $ean13 ?>' type="text" placeholder="" class="form-control input-sm"></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="product_cod_JAN">Código JAN</label><input id="product_cod_JAN" name="product_cod_JAN" value='<?= $jan ?>' type="text" placeholder="" class="form-control input-sm"></div></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="product_cod_upc">Código UPC</label><input id="product_cod_upc" name="product_cod_upc" type="text" value='<?= $upc ?>' placeholder="" class="form-control input-sm"></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="product_cod_ref">Referencia</label><input id="product_cod_ref" name="product_cod_ref" type="text" value='<?= $ref ?>' placeholder="" class="form-control input-sm"></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="product_descrip_corta">Descripción Corta</label><input id="product_descrip_corta" name="product_descrip_corta" value='<?= $corta ?>' type="text" placeholder="" class="form-control input-sm  " ></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="product_descrip_larga">Descripción Larga</label><input id="product_descrip_larga" name="product_descrip_larga" value='<?= $larga ?>' type="text" placeholder="" class="form-control input-sm  " ></div></div>
                                </div>                                
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Categoría </label>
                                            <select class="form-control input-sm" id="product_catego" name="product_catego">
                                                <option value="">Seleccione</option>
                                                <?php
                                                $SQL_ROWS_Categorias = $db->query("SELECT productcategorias_id, productcategorias_nombre FROM ProductCategorias");
                                                foreach ($SQL_ROWS_Categorias as $row):
                                                    echo "<option value=" . $row["productcategorias_id"] . ">" . $row["productcategorias_nombre"] . "</option>";
                                                endforeach;
                                                ?>
                                            </select>

                                        </div>                                
                                    </div>


                                    <div class="col-md-3">
                                        <div class="form-group">   
                                            <label class="control-label">Bodega </label>
                                            <select class="form-control input-sm" id="product_bodega" name="product_bodega">
                                                <option value="">Seleccione</option>
                                                <?php
                                                $SQL_ROWS = $db->query("SELECT bodega_id, bodega_nombre FROM Bodegas");
                                                foreach ($SQL_ROWS as $row):
                                                    echo "<option value=" . $row["bodega_id"] . ">" . $row["bodega_nombre"] . "</option>";
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>                                
                                    </div>

                                    <input id="id" name="id" class="hidden" type="text" value='<?= $id ?>'>


                                    
                                    <div class="col-md-3">
                                        <div class="form-group">   
                                            <label class="control-label">Estado del Producto </label>
                                            <select class="form-control input-sm" id="product_status" name="product_status">
                                                <option value="">Seleccione</option>
                                                <option value="Activado">Activado</option>
                                                <option value="Desactivado">Desactivado</option>
                                                
                                            </select>
                                        </div>                                
                                    </div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="product_precio">Precio Unitario Sin IVA</label><input id="product_precio" name="product_precio" type="text" placeholder="" class="form-control input-sm  " value="<?= $PRODUCTO_PRECIO; ?>"></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="product_cantidad">Cantidad</label><input id="product_cantidad" name="product_cantidad" type="text" placeholder="" class="form-control input-sm  " value="<?= $PRODUCTO_CANTIDAD; ?>"></div></div>

                                </div>
                                <hr>
                                <!-- BOTON GUARDAR -->
                                <div class="row">                                        
                                    <div class="col-md-12">
                                        <button type="button" id="modificar_productos_boton_cancel" class="btn btn-default pull-left"><i class="fa fa-times fa-2x"></i> <br/>Cancelar</button>
                                        <button type="submit" class="btn btn-default pull-right"><i class="fa fa-save fa-2x"></i> <br/>Guardar</button>
                                    </div>
                                </div>
                                <!-- FIN BOTON GUARDAR -->
                            </div>
                            <!-- FIN  Código dentro cuadro blanco -->
                        </div>
                    </div><!-- /panel -->
                    <!-- FIN INFORMACION PERSONAL -->
                </form>
            </div>
        </div>

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <?php require_once './includes/footer.inc.php'; ?>

        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!--Bootstrap-->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                //alert($('#fotografia').attr('src'));
                var Patch_Img;
                // Botón para subir la firma
                var btn_firma = $('#addImage'), interval;
                new AjaxUpload('#addImage', {
                    action: './modelo/uploadFile.php',
                    onSubmit: function (file, ext) {
                        if (!(ext && /^(jpg|png|jpeg)$/.test(ext))) {
                            // extensiones permitidas
                            alert('Sólo se permiten Imagenes .jpg, .png y/o .jpeg');
                            // cancela upload
                            return false;
                        } else {
                            $('#loaderAjax').show();
                            //btn_firma.text('Espere por favor');
                            this.disable();
                        }
                    },
                    onComplete: function (file, response) {
                        //alert(response);
                        //btn_firma.text('Subir Imagen');
                        var respuesta = $.parseJSON(response);
                        if (respuesta.respuesta == 'done') {
                            Patch_Img = './img/avatars_users/' + respuesta.fileName;
                            $('#fotografia').removeAttr('scr');
                            $('#fotografia').attr('src', Patch_Img);
                            $('#loaderAjax').show();
                            //alert(respuesta.mensaje);
                        }
                        else {
                            alert(respuesta.mensaje);
                        }
                        $('#loaderAjax').hide();
                        this.enable();
                    }
                });
            });
        </script>
        <script src="js/js-nfconnection/jquery.validate.min.js"></script>
        <script src="js/js-nfconnection/custom_jquery_validate.js"></script>
        <script src="js/js-nfconnection/AjaxUpload.2.0.min.js"></script>
        <script src="js/menu-active-class.js"></script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Bootstrap Switch -->
        <!--<script src='js/js-nfconnection/bootstrap-switch.js'></script>-->

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

        <script> //Script que al presionar "cancelar" devuelve a la pagina anterior
            $("#modificar_productos_boton_cancel").click(function () {
                window.location.replace("./productos.php");
            });
        </script>
    </body>
</html>