<?php
error_reporting(error_reporting() & ~E_NOTICE);
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UsernameUsuario = $_SESSION["datos_usuario_logueado"][10];

if (!isset($IDUsuario)) {
    header('Location: login.php');
}
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="dashboard.php"> Home</a></li>
                        <li class="active">Categorias</li>	 
                    </ul>
                </div><!-- breadcrumb -->
                <br/>
                <!--
                <div class="row paddingLR-md">
                    <div class="col-md-12 ">
                        <a href="./VIEW.Categoria_Add.php" class="btn btn-success pull-right"><span class="fa fa-plus-circle"></span> Añadir Nueva Categoria</a>
                    </div>                    
                </div>-->
                <div class="padding-md">
                    <div class="panel panel-default table-responsive">
                        <div class="panel-heading">                            
                            <div class="row">
                                <div class="col-md-6"><i class="fa fa-list-alt"></i> CATEGORIAS <span class="badge badge-info"><?php echo $db->single("SELECT COUNT(*) FROM ProductCategorias"); ?> </span></div>
                                
                            </div>
                        </div>
                        <div class="padding-md clearfix">
                            <table class="table table-striped" id="dataTable" style="font-size: 12px;"> 
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Referencia</th>
                                        <th class="text-center">Nombre</th>
                                        <th class="text-center">Descripcion</th>
                                        <th class="text-center">Modificar</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php
                                    $SQL_DATA_SELECT = $db->query("SELECT * FROM ProductCategorias");

                                    foreach ($SQL_DATA_SELECT as $row):

                                        $id = $row["productcategorias_id"];
                                        $Nombre = $row["productcategorias_nombre"];
                                        $referencia = $row["productcategorias_cod_ref"];
                                        $descripcion = $row["productcategorias_descripcion"];

                                        echo '<tr>';
                                        echo '<td class="text-center">' . $id . '</td>';
                                        echo '<td class="text-center">' . $referencia . '</td>';
                                        echo '<td class="text-center">' . $Nombre . '</td>';
                                        echo '<td class="text-center">' . $descripcion . '</td>';

                                        echo "<td class='text-center'>
                                            <div class='btn-group'>
                                                <a href='./VIEW.Categoria_Modificar.php?id=" . $id . "' class='btn btn-default dropdown-toggle btn-xs'><i class='fa fa-edit fa-lg'></i> Modificar </a>
                                                <a href='#' class='btn btn-default dropdown-toggle btn-xs' data-toggle='dropdown'><span class='caret'></span></a>
                                                <ul class='dropdown-menu slidedown'>
                                                    <li><a href='./modelo/MOD.Categoria_Eliminar.php?id=" . $id . "'><i class='fa fa-trash-o fa-lg'></i> Eliminar</a></li>
                                                </ul>
                                            </div>
                                        </td>";
                                        echo '</tr>';

                                    endforeach;
                                    ?>

                                </tbody>
                            </table>
                        </div><!-- /.padding-md -->
                    </div><!-- /panel -->
                </div>
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
            <?php require_once './includes/footer.inc.php'; ?>
        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>
        <!-- Eliminar Tag confirmation -->
        <?php require_once './includes/eliminar_tag_confim.inc.php'; ?>


        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src='js/jquery.dataTables.min.js'></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#dataTable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "asStripClasses": null
                });
                $('#refrescar_pagina').click(function () {
                    location.reload();
                });
                $('#a_btn_genera_pdf').on('click', function (e) {
                    window.open('./exportar_pdf/pdf_keys_tag.php', '_blank');
                });
                $('#a_btn_genera_excel').on('click', function (e) {
                    window.open('./exportar_excel/excel_keys_tags.php', '_blank');
                });
            });
        </script>
        <script src="js/menu-active-class.js"></script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Bootstrap Switch -->
        <script src='js/js-nfconnection/bootstrap-switch.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

    </body>
</html>


