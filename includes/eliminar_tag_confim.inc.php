<div class="custom-popup width-100" id="eliminar_tag_confim">
    <div class="padding-md">
        <h4 class="m-top-none" style="color: red;">¿Estas Seguro(a) de que quieres eliminar el Tag: <strong><span id="id_span_elimin_numero_tag"></span></strong>?</h4>
    </div>
    <div class="text-center">
    	<div class="" id="msj_respuesta_elminar_tag"></div>
        <button class="btn btn-danger m-right-sm" id="eliminar_tag">Eliminar</button>
        <button class="btn btn-primary eliminar_tag_confim_close" id="refrescar_pagina">Cancelar</button>
    </div>
</div>
