<?php
require_once './controlador/Db.class.php';
$db = new Db();
?>
<aside class="fixed skin-5">
    <div class="sidebar-inner scrollable-sidebar">        
        <div class="size-toggle">
            <a class="clearfix" href="dashboard.php" >
                <img class="img-responsive bounceIn" style="" src="img/logo_inventory.png" alt="User Avatar">
            </a>            
            <a class="btn btn-sm" id="sizeToggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="btn btn-sm pull-right logoutConfirm_open"  href="#logoutConfirm">
                <i class="fa fa-power-off"></i>
            </a>
        </div><!-- /size-toggle -->	
        <div class="user-block clearfix">
            <img src="<?php
            $RutaImgUsuarioUpdate = $db->single("SELECT user_ruta_img FROM Usuarios WHERE user_id = '$IDUsuario'");
            if ($RutaImgUsuarioUpdate == "") {
                $RutaImgUsuarioUpdate = "./img/imagen_profile_default.png";
                echo $RutaImgUsuarioUpdate;
            } else {
                echo $RutaImgUsuarioUpdate;
            }
            ?>" alt="User Avatar">
            <div class="detail">
                <strong><?php echo $NombreUsuario . ' ' . $SegundoNombreUsuario . '' . "</br>" . $ApellidoPaterno . " " . $ApellidoMaterno; ?></strong>
                <!--<ul class="list-inline">
                    <li><a href="profile.php">Perfil</a></li>
                </ul>-->
            </div>
        </div><!-- /user-block -->
        <!--<div class="search-block">
            <div class="input-group">
                <input type="text" class="form-control input-sm" placeholder="Buscar Aquí...">
                <span class="input-group-btn">
                    <button class="btn btn-default btn-sm" type="button"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>--><!-- /search-block -->
        <div class="main-menu">
            <ul>
                <li class="">
                    <a href="dashboard.php"><span class="menu-icon"><i class="fa fa-tachometer fa-lg"></i></span><span class="text">Tablero Principal</span><span class="menu-hover"></span></a>
                </li>
                <li class="">
                    <a href="VIEW.OrdenDespacho.Lista.php"><span class="menu-icon"><i class="fa fa-truck fa-lg"></i></span><span class="text">Despacho</span><span class="menu-hover"></span></a>
                </li>
                <li class="openable">
                    <a href="#"><span class="menu-icon"><i class="fa fa-cog fa-lg"></i></span><span class="text">Administración</span><span class="menu-hover"></span></a>
                    <ul class="submenu">
                        <li><a href="VIEW.ListaEmpresas.php"><span class="submenu-label"> Empresas</span></a></li>     
                        <li><a href="VIEW.ListaSucursales.php"><span class="submenu-label"> Sucursales</span></a></li>     
                        <li><a href="VIEW.Usuario_Lista.php"><span class="submenu-label"> Empleados</span></a></li>

                    </ul>
                </li>
                <!--<li class="openable">
                    <a href="#"><span class="menu-icon"><i class="fa fa-building-o fa-lg"></i></span><span class="text">Compras</span><span class="menu-hover"></span></a>
                    <ul class="submenu">
                        <li><a href="proveedores.php"><span class="submenu-label"> Proveedores</span></a></li>
                        <li><a href="facturas.php"><span class="submenu-label"> Facturas</span></a></li>
                    </ul>
                </li>-->
                <li class="openable">
                    <a href="#"><span class="menu-icon"><i class="fa fa-archive fa-lg"></i></span><span class="text">Existencias</span><span class="menu-hover"></span></a>
                    <ul class="submenu">
                        <li><a href="VIEW.Bodega_Lista.php"><span class="submenu-label"> Bodegas</span></a></li>
                        <li><a href="VIEW.Categorias_Lista.php"><span class="submenu-label"> Categorias</span></a></li>
                        <li><a href="VIEW.Productos_Lista.php"><span class="submenu-label"> Productos</span></a></li>
                        <!--<li><a href="VIEW.Existencias_Lista.php"><span class="submenu-label"> Gestión de Existencias</span></a></li>
                        <li><a href="mov_stock.php"><span class="submenu-label"> Movimiento de Stock</span></a></li>
                        <li><a href="status_stock.php"><span class="submenu-label"> Estado Actual de Stock</span></a></li>--
                    <!--<li><a href="#"><span class="submenu-label"> Cobertura de Stock</span></a></li>
                        <li><a href="#"><span class="submenu-label"> Pedidos de Materiales</span></a></li>
                        <li><a href="#"><span class="submenu-label"> Configuración</span></a></li>-->                        
                    </ul>
                </li>
<!--
                <li class="openable">
                    <a href="#"><span class="menu-icon"><i class="fa fa-wrench fa-lg"></i></span><span class="text">Preferencias</span><span class="menu-hover"></span></a>
                    <ul class="submenu">
                        <li><a href="./profile_company.php"><span class="submenu-label"> Configuración</span></a></li>
                        <li><a href="./informacion.php"><span class="submenu-label"> Información</span></a></li>
                    </ul>
                </li>-->
                <!--<li class="openable">
                    <a href="#">
                        <span class="menu-icon">
                            <i class="fa fa-magic fa-lg"></i> 
                        </span>
                        <span class="text">
                            Administración
                        </span>
                        <span class="menu-hover"></span>
                    </a>
                    <ul class="submenu">
                        <li class="openable">
                            <a href="#">
                                <span class="submenu-label">Usuarios </span>
                                <!--<span class="badge badge-danger bounceIn animation-delay1 pull-right">3</span>
                            </a>
                            <ul class="submenu third-level">
                                <li>asdasd</li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="submenu">
                        <li class="openable">
                            <a href="#">
                                <span class="submenu-label">Reportes </span>
                                <span class="badge badge-danger bounceIn animation-delay1 pull-right">3</span>
                            </a>
                            <ul class="submenu third-level">
                                <li><a href="reporte_mensual.php"><span class="submenu-label"></i> Reporte Mensual</span></a></li>
                        <li><a href="reporte_semanal.php"><span class="submenu-label"></i> Ingreso Semanal</span></a></li>
                        <li><a href="reporte_diario.php"><span class="submenu-label"></i> Ingreso Diario</span></a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="submenu">
                        <li class="openable">
                            <a href="#">
                                <span class="submenu-label">Chips NFC </span>
                                <span class="badge badge-danger bounceIn animation-delay1 pull-right">3</span>
                            </a>
                            <ul class="submenu third-level">
                                <li><a href="tags.php"><span class="submenu-label"><i class="fa fa-tags"></i> Listar Tag's</span></a></li>
                                <li><a href="keys_tags.php"><span class="submenu-label"><i class="fa fa-tags"></i> Listar Key's - Tag's</span></a></li>
                                <li><a href="urls.php"><span class="submenu-label"><i class="fa fa-tags"></i> Listar URLS</span></a></li>
                            </ul>
                        </li>
                    </ul>-->
                </li>
                <!--<li class="openable open">
                    <a href="#"><span class="menu-icon"><i class="fa fa-file-text fa-lg"></i></span><span class="text">Page</span><span class="menu-hover"></span></a>
                    <ul class="submenu">
                        <li><a href="login.php"><span class="submenu-label">Sign in</span></a></li>
                        <li><a href="register.php"><span class="submenu-label">Sign up</span></a></li>
                        <li><a href="lock_screen.php"><span class="submenu-label">Lock Screen</span></a></li>
                        <li><a href="profile.php"><span class="submenu-label">Profile</span></a></li>
                        <li><a href="blog.php"><span class="submenu-label">Blog</span></a></li>
                        <li><a href="single_post.php"><span class="submenu-label">Single Post</span></a></li>
                        <li><a href="landing.php"><span class="submenu-label">Landing</span></a></li>
                        <li><a href="search_result.php"><span class="submenu-label">Search Result</span></a></li>
                        <li><a href="chat.php"><span class="submenu-label">Chat Room</span></a></li>
                        <li><a href="movie.php"><span class="submenu-label">Movie Gallery</span></a></li>
                        <li><a href="pricing.php"><span class="submenu-label">Pricing</span></a></li>
                        <li><a href="invoice.php"><span class="submenu-label">Invoice</span></a></li>
                        <li><a href="faq.php"><span class="submenu-label">FAQ</span></a></li>
                        <li><a href="contact.php"><span class="submenu-label">Contact</span></a></li>
                        <li><a href="error404.php"><span class="submenu-label">Error404</span></a></li>
                        <li><a href="error500.php"><span class="submenu-label">Error500</span></a></li>
                        <li><a href="blank.php"><span class="submenu-label">Blank</span></a></li>
                    </ul>
                </li>
                <li class="openable">
                    <a href="#">
                        <span class="menu-icon">
                            <i class="fa fa-tag fa-lg"></i> 
                        </span>
                        <span class="text">
                            Component
                        </span>
                        <span class="badge badge-success bounceIn animation-delay5">9</span>
                        <span class="menu-hover"></span>
                    </a>
                    <ul class="submenu">
                        <li><a href="ui_element.php"><span class="submenu-label">UI Features</span></a></li>
                        <li><a href="button.php"><span class="submenu-label">Button & Icons</span></a></li>
                        <li><a href="tab.php"><span class="submenu-label">Tab</span></a></li>
                        <li><a href="nestable_list.php"><span class="submenu-label">Nestable List</span></a></li>
                        <li><a href="calendar.php"><span class="submenu-label">Calendar</span></a></li>
                        <li><a href="table.php"><span class="submenu-label">Table</span></a></li>
                        <li><a href="widget.php"><span class="submenu-label">Widget</span></a></li>
                        <li><a href="form_element.php"><span class="submenu-label">Form Element</span></a></li>
                        <li><a href="form_wizard.php"><span class="submenu-label">Form Wizard</span></a></li>
                    </ul>
                </li>

                <li>
                    <a href="timeline.php">
                        <span class="menu-icon">
                            <i class="fa fa-clock-o fa-lg"></i> 
                        </span>
                        <span class="text">
                            Timeline
                        </span>
                        <span class="menu-hover"></span>
                    </a>
                </li>
                <li>
                    <a href="gallery.php">
                        <span class="menu-icon">
                            <i class="fa fa-picture-o fa-lg"></i> 
                        </span>
                        <span class="text">
                            Gallery
                        </span>
                        <span class="menu-hover"></span>
                    </a>
                </li>
                <li>
                    <a href="inbox.php">
                        <span class="menu-icon">
                            <i class="fa fa-envelope fa-lg"></i> 
                        </span>
                        <span class="text">
                            Inbox
                        </span>
                        <span class="badge badge-danger bounceIn animation-delay6">4</span>
                        <span class="menu-hover"></span>
                    </a>
                </li>
                <li>
                    <a href="email_selection.php">
                        <span class="menu-icon">
                            <i class="fa fa-tasks fa-lg"></i> 
                        </span>
                        <span class="text">
                            Email Template
                        </span>
                        <small class="badge badge-warning bounceIn animation-delay7">New</small>
                        <span class="menu-hover"></span>
                    </a>
                </li>
                <li class="openable">
                    <a href="#">
                        <span class="menu-icon">
                            <i class="fa fa-magic fa-lg"></i> 
                        </span>
                        <span class="text">
                            Multi-Level menu
                        </span>
                        <span class="menu-hover"></span>
                    </a>
                    <ul class="submenu">
                        <li class="openable">
                            <a href="#">
                                <span class="submenu-label">menu 2.1</span>
                                <span class="badge badge-danger bounceIn animation-delay1 pull-right">3</span>
                            </a>
                            <ul class="submenu third-level">
                                <li><a href="#"><span class="submenu-label">menu 3.1</span></a></li>
                                <li><a href="#"><span class="submenu-label">menu 3.2</span></a></li>
                                <li class="openable">
                                    <a href="#">
                                        <span class="submenu-label">menu 3.3</span>
                                        <span class="badge badge-danger bounceIn animation-delay1 pull-right">2</span>
                                    </a>
                                    <ul class="submenu fourth-level">
                                        <li><a href="#"><span class="submenu-label">menu 4.1</span></a></li>
                                        <li><a href="#"><span class="submenu-label">menu 4.2</span></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="openable">
                            <a href="#">
                                <span class="submenu-label">menu 2.2</span>
                                <span class="badge badge-success bounceIn animation-delay2 pull-right">3</span>
                            </a>
                            <ul class="submenu third-level">
                                <li class="openable">
                                    <a href="#">
                                        <span class="submenu-label">menu 3.1</span>
                                        <span class="badge badge-success bounceIn animation-delay1 pull-right">2</span>
                                    </a>
                                    <ul class="submenu fourth-level">
                                        <li><a href="#"><span class="submenu-label">menu 4.1</span></a></li>
                                        <li><a href="#"><span class="submenu-label">menu 4.2</span></a></li>
                                    </ul>
                                </li>
                                <li><a href="#"><span class="submenu-label">menu 3.2</span></a></li>
                                <li><a href="#"><span class="submenu-label">menu 3.3</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>-->
            </ul>

        </div><!-- /main-menu -->
    </div><!-- /sidebar-inner -->
</aside>