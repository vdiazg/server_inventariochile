<div class="custom-popup width-100" id="mod_data">
    <div class="padding-md">
        <h2>Atención</h2>
        <h4 class="m-top-none">¿Esta Seguro de Modificar la Información?</h4>
    </div>

    <div class="text-center">
        <a class="btn btn-success m-right-sm" href="#">MODIFICAR</a>
        <a class="btn btn-danger mod_data_close">CANCELAR</a>
    </div>
</div>