<div class="custom-popup width-100" id="logoutConfirm">
    <div class="padding-md">
        <h4 class="m-top-none">¿Esta seguro de que quieres salir del sistema?</h4>
    </div>

    <div class="text-center">
        <a class="btn btn-success m-right-sm" href="./controlador/cerrar_sesion.php">Cerrar Sesión</a>
        <a class="btn btn-danger logoutConfirm_close">Cancelar</a>
    </div>
</div>