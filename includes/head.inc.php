<meta charset="utf-8">
<title>(Inventario) NFConnection LTDA.</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="NFConnection LTDA. es una empresa dedicada a generar información relevante para la toma de desiciones a nivel gerencial, además de dar soluciones a los cambios tecnológicos que van surguiendo en esta, una Nueva Era de la tecnología.">
<meta name="author" content="NFConnection LTDA.">
<link rel="shortcut icon" href="./img/logo_inventory2.jpg">
<!-- Bootstrap core CSS -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">	
<link href="css/css-nfconnection/css_editado.css" rel="stylesheet">	
<!-- Font Awesome -->
<link href="fonts/css/font-awesome.min.css" rel="stylesheet">
<!-- Font FlatIcon -->
<link href="fonts-flaticon/flaticon.css" rel="stylesheet">
<link href="css/redesociales/redesociales.css" rel="stylesheet">
<!-- Pace -->
<link href="css/pace.css" rel="stylesheet">	
<!-- Chosen -->
<link href="css/chosen/chosen.min.css" rel="stylesheet"/>
<!-- Datepicker -->
<link href="css/css-nfconnection/datepicker3.css" rel="stylesheet"/>
<!-- Bootstrap Switch -->
<link href="css/css-nfconnection/bootstrap-switch.css" rel="stylesheet"/>
<!-- Timepicker -->
<link href="css/bootstrap-timepicker.css" rel="stylesheet"/>	
<!-- Slider -->
<link href="css/slider.css" rel="stylesheet"/>	
<!-- Tag input -->
<link href="css/jquery.tagsinput.css" rel="stylesheet"/>
<!-- WYSIHTML5 -->
<link href="css/bootstrap-wysihtml5.css" rel="stylesheet"/>
<!-- jcarousel -->
<link href="css/jcarousel.responsive.css" rel="stylesheet">
<!-- Color box -->
<link href="css/colorbox/colorbox.css" rel="stylesheet">
<!-- Datatable -->
<link href="css/jquery.dataTables_themeroller.css" rel="stylesheet">
<!-- Gritter -->
<link href="css/gritter/jquery.gritter.css" rel="stylesheet">
<!-- Google Code Prettify -->
<link href="css/prettify.css" rel="stylesheet">
<!-- Dropzone -->
<!--<link href='css/dropzone/dropzone.css' rel="stylesheet"/>-->
<!-- Endless -->
<link href="css/endless.min.css" rel="stylesheet">
<link href="css/endless-skin.css" rel="stylesheet">