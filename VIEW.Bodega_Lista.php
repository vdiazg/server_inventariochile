<?php
error_reporting( error_reporting() & ~E_NOTICE );
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UsernameUsuario = $_SESSION["datos_usuario_logueado"][10];

if (!isset($IDUsuario)) {
    header('Location: login.php');
}
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="dashboard.php"> Home</a></li>
                        <li class="active">Bodegas</li>	 
                    </ul>
                </div><!-- breadcrumb -->
                <br/>
                <!--
                <div class="row paddingLR-md">
                    <div class="col-md-12 ">
                        <a href="VIEW.Bodega.Add.php" class="btn btn-success pull-right"><span class="fa fa-plus-circle"></span> Añadir Nueva Bodega</a>
                    </div>                    
                </div>-->
                <div class="padding-md">
                    <div class="panel panel-default table-responsive">
                        <div class="panel-heading">                            
                            <div class="row">
                                <div class="col-md-6"><i class="fa fa-list-alt"></i> BODEGAS <span class="badge badge-info"> <?php echo $db->single("SELECT COUNT(*) FROM Bodegas"); ?></span></div>
                            </div>
                        </div>
                        <div class="padding-md clearfix">
                            <table class="table table-striped" id="dataTable" style="font-size: 12px;"> 
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Referencia</th>
                                        <th class="text-center">Nombre</th>
                                        <th class="text-center">Administrador</th>
                                        <th class="text-center">Dirección</th>
                                        <th class="text-center">Teléfono</th>
                                        <th class="text-center">Modificar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    $SQL_DATA_SELECT_BODEGAS = $db->query("SELECT * FROM Bodegas");
                                    
                                    foreach ($SQL_DATA_SELECT_BODEGAS as $row):
                                        
                                        $BODEGA_ID_BD           = $row["bodega_id"];
                                        $BODEGA_NOMBRE          = $row["bodega_nombre"];
                                        $BODEGA_REF             = $row["bodega_ref"];
                                        $BODEGA_TELEFONO        = $row["bodega_telefono"];
                                        $BODEGA_TELEFONO_MOVIL  = $row["bodega_telefono_movil"];
                                        $BODEGA_DIRECCION       = $row["bodega_direccion"];
                                        $BODEGA_DIRECCION2      = $row["bodega_direccion2"];
                                        $BODEGA_CODIGO_POSTAL   = $row["bodega_codigopostal"];
                                        $BODEGA_FORANEAUSUARIO = $row["Usuarios_user_id"];
                                        $BODEGA_ENCARGADO_NOMBRE = $db->single("SELECT user_nombre FROM Usuarios WHERE user_id = '$BODEGA_FORANEAUSUARIO'");
                                        $BODEGA_ENCARGADO_AP = $db->single("SELECT user_ap FROM Usuarios WHERE user_id = '$BODEGA_FORANEAUSUARIO'");
                                        
                                        echo '<tr>';
                                        echo '<td class="text-center">' . $BODEGA_ID_BD . '</td>';
                                        echo '<td class="text-center">' . $BODEGA_REF .'</td>';
                                        echo '<td class="text-center">' . $BODEGA_NOMBRE . '</td>';
                                        echo '<td class="text-center">' . $BODEGA_ENCARGADO_NOMBRE. ' '.$BODEGA_ENCARGADO_AP . '</td>';
                                        echo '<td class="text-center">' . $BODEGA_DIRECCION . '</td>';
                                        echo '<td class="text-center">' . $BODEGA_TELEFONO_MOVIL . '</td>';
                                        echo "<td class='text-center'>
                                            <div class='btn-group'>
                                                <a href='VIEW.Bodega_Modificar.php?id=" . $BODEGA_ID_BD . "' class='btn btn-default dropdown-toggle btn-xs'><i class='fa fa-edit fa-lg'></i> Modificar </a>
                                                <a href='#' class='btn btn-default dropdown-toggle btn-xs' data-toggle='dropdown'><span class='caret'></span></a>
                                                <ul class='dropdown-menu slidedown'>
                                                    <li><a href='./modelo/MOD.Bodega_Eliminar.php?id=" . $BODEGA_ID_BD . "'><i class='fa fa-trash-o fa-lg'></i> Eliminar</a></li>
                                                </ul>
                                            </div>
                                        </td>";
                                        echo '</tr>';

                                        endforeach;
                                    ?>
                                    
                                </tbody>
                            </table>
                        </div><!-- /.padding-md -->
                    </div><!-- /panel -->
                </div>
            </div><!-- /main-container -->
        </div><!-- /wrapper -->
        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
        
        <?php require_once './includes/footer.inc.php'; ?>
        
        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>
        <!-- Eliminar Tag confirmation -->
        <?php require_once './includes/eliminar_tag_confim.inc.php'; ?>

        
        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        
        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src='js/jquery.dataTables.min.js'></script>
        <script type="text/javascript">
            function exportarExcel(){
                alasql('SELECT ID, Referencia, Nombre, Administrador, Localizacion, Telefono  INTO XLSX("myinquires.xlsx",{headers:true}) \
                    FROM HTML("#dataTable",{headers:true})');
           }
            
            $(document).ready(function () {
                $('#dataTable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "asStripClasses": null
                });
                $('#refrescar_pagina').click(function() {
                    location.reload();
                });
            });
            function cargarModalModificarUsuario(id) {
            		window.location.href =  "modificar_bodega.php?id="+id;

            }

            function eliminar(id){
               var Resp = confirm("Eliminar Registro para el ID:" + id);
                
              //  window.location.href = "./modelo/eliminar_tags.php?id="+ id;
                if (Resp) {
                 $.ajax({
                            type: "POST",
                            url: "./modelo/eliminar_bodega.php",
                            data: { id_bodega: id} ,
                            success: function (data) {
                                alert(data);
                                $('#msj_respuesta_elminar_tag').html(data);
                            }
                        });
                     location.reload();

             }
                        
            }

            function UpTag(IDUsuarioEmpleado, TagID, NTagAsociadoEmpleado, TagTipo, NombreEmpleado, ApellidoPaternoEmpleado, ApellidoMaternoEmpleado, TagEstado){
                $("#id_btn_guardar").click(function() {
                        if ($('#switch-state').is(':checked')) {
                            EstadoTag = "Habilitado";
                        }else{
                            EstadoTag = "Deshabilitado";
                        }
                        $.ajax({
                            type: "POST",
                            url: "./modelo/update_tags.php",
                            data: $("#form_Update_tags").serialize()+
                            "&EstadoTag="+EstadoTag+
                            "&TagID="+TagID,
                            success: function (data) {
                                //alert(data);
                                $('#msj_respuesta_update_tag').html(data);
                            }
                        });
                        //alert(EstadoUsuarioCheckbox+" "+EstadoTag);
                    });                
                $("#eliminar_tag").click(function() {
                   
                        if ($('#switch-state').is(':checked')) {
                            EstadoTag = "Habilitado";
                        }else{
                            EstadoTag = "Deshabilitado";
                        }
                        $.ajax({
                            type: "POST",
                            url: "./modelo/eliminar_tags.php",
                            data: $("#form_Update_tags").serialize()+
                            "&EstadoTag="+EstadoTag+
                            "&TagID="+TagID,
                            success: function (data) {
                                //alert(data);
                                $('#msj_respuesta_elminar_tag').html(data);
                            }
                        });
                        //alert(EstadoUsuarioCheckbox+" "+EstadoTag);
                    });
            }
        </script>
        <script src="js/menu-active-class.js"></script>
        <script type="text/javascript">
//            $(document).ready(function () {
//                $('#a_btn_genera_pdf').on('click', function (e) {
//                    window.open('./exportar_pdf/pdf_keys_tag.php','_blank');
//                });
//                $('#a_btn_genera_excel').on('click', function (e) {
//                    window.open('./exportar_excel/excel_keys_tags.php','_blank');
//                });
//            });
         $(document).ready(function () {
                $('#a_btn_genera_pdf').on('click', function (e) {
                    window.open('./exportar_pdf/pdf_keys_tag.php','_blank');
                });
                
                    
                $('#excel').on('click', function (e) {
//                   a_btn_genera_excel
                    window.open('./exportar_excel/excel_keys_tags.php','_blank');
                });
            });
        </script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Bootstrap Switch -->
        <script src='js/js-nfconnection/bootstrap-switch.js'></script>
    
        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>
        
        <!-- libreria alasql -->
        <script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
        
        <!-- libreria xlsx -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.core.min.js"></script>

    </body>
</html>


