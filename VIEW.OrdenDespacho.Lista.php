<?php
error_reporting(error_reporting() & ~E_NOTICE);
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UsernameUsuario = $_SESSION["datos_usuario_logueado"][10];

if (!isset($IDUsuario)) {
    header('Location: login.php');
}
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="dashboard.php"> Home</a></li>
                        <li class="active">Productos</li>	 
                    </ul>
                </div><!-- breadcrumb -->
                <br/>
                <div class="row paddingLR-md">
                    <div class="col-md-12 ">
                        <a href="./VIEW.OrdenDespacho_Create.php" class="btn btn-success pull-right"><span class="fa fa-plus-circle"></span> Crear Nueva Orden de Despacho</a>
                    </div>                    
                </div>
                <div class="padding-md">
                    <div class="panel panel-default table-responsive">
                        <div class="panel-heading">                            
                            <div class="row">
                                <div class="col-md-6"><i class="fa fa-list-alt"></i> PRODUCTOS <span class="badge badge-info"><?php echo $db->single("SELECT COUNT(*) FROM Orden_Despacho"); ?></span></div>
                                
                            </div>
                        </div>
                        <div class="padding-md clearfix">
                            <table class="table table-bordered table-condensed table-hover table-striped" id="dataTable" style="font-size: 12px;"> 
                                <thead>
                                    <tr>
                                        <th class="text-center">Fecha y Hora</th>
                                        <th class="text-center">N° ORDEN</th>
                                        <th class="text-center">Emitida por</th>
                                        <th class="text-center">Dirigido a sucursal</th>
                                        <th class="text-center">Subtotal</th>
                                        <th class="text-center">IVA</th>
                                        <th class="text-center">Total</th>
                                    </tr>
                                </thead>
                                <tbody>


                                    <?php
                                    //$SQL_DATA_SELECt = $db->query("SELECT * FROM Orden_Despacho ODO, Orden_Detalle ODE, Usuarios U, Sucursal U WHERE U.user_id = ODO.Usuarios_user_id AND S.sucursal_id = ODO.Sucursales_sucursal_id AND ODO.orden_despacho_id = ODE.Orden_Despacho_orden_despacho_id");
                                    //$SQL_DATA_SELECt = $db->query("SELECT * FROM Orden_Detalle ODE, Orden_Despacho ODO WHERE ODO.orden_despacho_id = ODE.Orden_Despacho_orden_despacho_id");
                                    $SQL_DATA_SELECt = $db->query("SELECT * FROM Orden_Despacho order by orden_despacho_fecha_hora ASC");

                                    foreach ($SQL_DATA_SELECt as $row):

                                        $ORDEN_ID = $row['orden_despacho_id'];
                                        $ORDEN_NUMERO = $row['orden_despacho_num'];
                                        $ORDEN_FECHA_HORA = $row['orden_despacho_fecha_hora'];
                                        $ORDEN_SUBTOTAL = $row['orden_despacho_subtotal'];
                                        $ORDEN_IVA = $row['orden_despacho_iva'];
                                        $ORDEN_TOTAL = $row['orden_despacho_total'];

                                        $ORDEN_FK_IDSUCURSAL = $row['Sucursales_sucursal_id'];
                                        $NOMBRE_SUCURSAL = $db->single("SELECT sucursal_nombre FROM Sucursales WHERE sucursal_id = '" . $ORDEN_FK_IDSUCURSAL . "'");

                                        $ORDEN_FK_IDUSUARIO = $row['Usuarios_user_id'];
                                        $NOMBRE_USUARIO_nombre = $db->single("SELECT user_nombre FROM Usuarios WHERE user_id = '" . $ORDEN_FK_IDUSUARIO . "'");
                                        $NOMBRE_USUARIO_ap = $db->single("SELECT user_ap FROM Usuarios WHERE user_id = '" . $ORDEN_FK_IDUSUARIO . "'");

                                        $ORDEN_DETALLE_PRODUCTO_NOMBRE = $row['orden_detalle_product_nombre'];
                                        $ORDEN_DETALLE_PRODUCTO_EAN13 = $row['orden_detalle_product_ean13'];
                                        $ORDEN_DETALLE_PRODUCTO_CANTIDAD = $row['orden_detalle_product_cantidad'];
                                        $ORDEN_DETALLE_PRODUCTO_PRECIO = $row['orden_detalle_product_precio'];



                                        echo '<tr>';
                                        echo '<td class="text-center">' . $ORDEN_FECHA_HORA . '</td>';
                                        echo '<td class="text-center"><a href="./VIEW.OrdenDespacho.Detail.php?id=' . $ORDEN_ID . '" class="btn btn-link btn-xs">' . $ORDEN_NUMERO . '</a></td>';
                                        echo '<td class="text-center">' . $NOMBRE_USUARIO_nombre . ' ' . $NOMBRE_USUARIO_ap . '</td>';
                                        echo '<td class="text-center">' . $NOMBRE_SUCURSAL . '</td>';
                                        echo '<td class="text-center">$' . number_format($ORDEN_SUBTOTAL, 0, ',', '.') . '</td>';
                                        echo '<td class="text-center">$' . number_format($ORDEN_IVA, 0, ',', '.') . '</td>';
                                        echo '<td class="text-center">$' . number_format($ORDEN_TOTAL, 0, ',', '.') . '</td>';
                                        /*
                                          echo "<td class='text-center'>
                                          <div class='btn-group'>
                                          <a href='VIEW.Producto_Modificar.php?id=" . $PRODUCTO_ID_BD . "' class='btn btn-default dropdown-toggle btn-xs'><i class='fa fa-edit fa-lg'></i> Modificar </a>
                                          <a href='#' class='btn btn-default dropdown-toggle btn-xs' data-toggle='dropdown'><span class='caret'></span></a>
                                          <ul class='dropdown-menu slidedown'>
                                          <li><a href='./modelo/MOD.Producto_Eliminar.php?id=" . $PRODUCTO_ID_BD . "'><i class='fa fa-trash-o fa-lg'></i> Eliminar</a></li>
                                          </ul>
                                          </div>
                                          </td>"; */
                                        echo '</tr>';

                                    endforeach;
                                    ?>




                                </tbody>
                            </table>
                        </div><!-- /.padding-md -->
                    </div><!-- /panel -->
                </div>
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>
        <!-- Eliminar Tag confirmation -->
        <?php require_once './includes/eliminar_tag_confim.inc.php'; ?>


        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src='js/jquery.dataTables.min.js'></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#dataTable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "asStripClasses": null
                });
                $('#refrescar_pagina').click(function () {
                    location.reload();
                });
                $('#a_btn_genera_pdf').on('click', function (e) {
                    window.open('./exportar_pdf/pdf_keys_tag.php', '_blank');
                });
                $('#a_btn_genera_excel').on('click', function (e) {
                    window.open('./exportar_excel/excel_keys_tags.php', '_blank');
                });
            });
        </script>
        <script src="js/menu-active-class.js"></script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Bootstrap Switch -->
        <script src='js/js-nfconnection/bootstrap-switch.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

    </body>
</html>