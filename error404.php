<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from minetheme.com/Endless1.5.1/error404.php by HTTrack Website Copier/3.x [XR&CO'2010], Mon, 05 Jan 2015 15:17:05 GMT -->
    <head>
        <meta charset="utf-8">
        <title>Endless Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Bootstrap core CSS -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Font Awesome -->
        <link href="fonts/css/font-awesome.min.css" rel="stylesheet">

        <!-- Endless -->
        <link href="css/endless.min.css" rel="stylesheet">
        <link href="css/endless-skin.css" rel="stylesheet">

    </head>

    <body>
        <div id="wrapper">
            <div class="padding-md" style="margin-top:50px;">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center">
                        <div class="h5">Oops, esta página no se pudo encontrar!</div>
                        <h1 class="m-top-none error-heading">404</h1>
                        <!--
                        <h4>Search Our Website</h4>
                        <div>Can't find what you need?</div>
                        <div class="m-bottom-md">Try searching for the page here</div>
                        <div class="input-group m-bottom-md">
                                <input type="text" class="form-control input-sm" placeholder="search here...">
                                <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button"><i class="fa fa-search"></i></button>
                                </span>
                        </div>-->
                        <a class="btn btn-success m-bottom-sm" onClick="history.go(-1);return true;"><i class="fa fa-arrow-left"></i> Volver Atras</a>
                        <a class="btn btn-success m-bottom-sm" href="index.php"><i class="fa fa-home"></i> Ir al Home</a>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.padding-md -->
        </div><!-- /wrapper -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

    </body>

    <!-- Mirrored from minetheme.com/Endless1.5.1/error404.php by HTTrack Website Copier/3.x [XR&CO'2010], Mon, 05 Jan 2015 15:17:05 GMT -->
</html>
