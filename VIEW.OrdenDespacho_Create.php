<?php
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UserNameUsuario = $_SESSION["datos_usuario_logueado"][10];

if (!isset($IDUsuario)) {
    header('Location: login.php');
}

require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
        <style>
            .invoice-title h2, .invoice-title h3 {
                display: inline-block;
            }

            .table > tbody > tr > .no-line {
                border-top: none;
            }

            .table > thead > tr > .no-line {
                border-bottom: none;
            }

            .table > tbody > tr > .thick-line {
                border-top: 2px solid;
            }
        </style>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="dashboard.php"> Home</a></li>
                        <li class="active">Orden de Despacho</li>	 
                    </ul>
                </div>

                <?php
                //Obtener datos dia despacho
                $fecha = getdate();
                //$order = $fecha['mday'] . $fecha['mon'] . substr($fecha['year'], -2) . $fecha['hours'] . $fecha['minutes'];
                $order = date("YmdHis");
                $fecha2 = $fecha['mday'] . '/' . $fecha['mon'] . '/' . $fecha['year'];
                ?>

                <form id="form1">
                    <div class="padding-md">
                        <div id="dvContainer" class="col-md-12" style="border-radius: 25px; border: 1.5px solid #73AD21;">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="invoice-title">
                                        <h2>Orden de Despacho</h2><h3 class="pull-right">N°: <span id="num_orden"><?php echo $order; ?></span></h3>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <address>
                                                <strong>De:</strong><br>
                                                <?= $NombreUsuario . ' ' . $ApellidoPaterno ?><br>
                                            </address>
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <div class="form-group">
                                                <label class="control-label"><strong>Dirigido a:</strong></label>
                                                <select class="form-control input-sm" id="bod_sucursal" name="bod_sucursal" required="">
                                                    <option value="">Seleccione</option>
                                                    <?php
                                                    $DATOS_SQL_SUCURSAL = $db->query("SELECT sucursal_id, sucursal_nombre FROM Sucursales");
                                                    foreach ($DATOS_SQL_SUCURSAL as $row):
                                                        echo "<option value=" . $row["sucursal_id"] . ">" . $row["sucursal_nombre"] . "</option>";
                                                    endforeach
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <!--<address>
                                                <strong>Payment Method:</strong><br>
                                                Visa ending **** 4242<br>
                                                jsmith@email.com
                                            </address>-->
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <address>
                                                <strong>Fecha de Emisión</strong><br>
                                                <span id="fecha_hora"><?php echo date("Y-m-d H:i:s"); ?></span>
                                            </address>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><strong>Resumen de la orden</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table id="myTableX" class="table table-bordered table-condensed table-hover table-striped" style="font-size: 12px;">
                                                    <thead>
                                                        <tr>
                                                            <td><strong>EAN13</strong></td>
                                                            <td><strong>Item</strong></td>
                                                            <td class="text-center"><strong>Precio</strong></td>
                                                            <td class="text-center"><strong>Cantidad</strong></td>
                                                            <td class="text-right"><strong>Total</strong></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr id="lastDataX">
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="row">
                                                    <div class="col-md-12 text-right">
                                                        <div><strong>SUBTOTAL: </strong><span id="cantidadSubTotal"></span></div>
                                                        <div><strong>IVA(19%): </strong><span id="cantidadIVA"></span></div>
                                                        <div><strong>TOTAL: </strong><span id="cantidadTotal"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-12">
                                <hr>
                                <a href="VIEW.OrdenDespacho.Lista.php" class="btn btn-default quick-btn pull-left margin-sm"><i class="fa fa-reply"></i><span>Volver</span></a>
                                <a id="btnGuardar" class="btn btn-default quick-btn pull-right margin-sm"><i class="fa fa-print"></i><span>Guardar</span></a>
                            </div>
                        </div>
                        <div class="row">
                            <div id="respuesta" class="col-md-12"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <?php require_once './includes/footer.inc.php'; ?>

        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!--Bootstrap-->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src="js/js-nfconnection/jquery.validate.min.js"></script>
        <script src="js/js-nfconnection/custom_jquery_validate.js"></script>

        <script src="js/menu-active-class.js"></script>
        <script src="js/js-nfconnection/jquery.json.js"></script>
        <script>
            $(document).ready(function () {

                /* IMPRESION */
                $("#btnPrint").click(function () {
                    var divContents = $("#dvContainer").html();
                    var printWindow = window.open('', 'my div', 'height=400,width=600');
                    printWindow.document.write('<html><head><title>DIV Contents</title>');
                    printWindow.document.write('<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">');
                    printWindow.document.write('</head><body>');
                    printWindow.document.write(divContents);
                    printWindow.document.write('</body></html>');
                    printWindow.print();
                    printWindow.close();
                });
                /* IMPRESION */
                var pressed = false;
                var chars = [];
                var SUBTOTAL_SEND;
                var IVA = 0;
                var TOTAL = 0;
                var barcode;
                $(window).keypress(function (e) {
                    if (e.which >= 48 && e.which <= 57) {
                        chars.push(String.fromCharCode(e.which));
                    }
                    if (pressed == false) {
                        setTimeout(function () {
                            if (chars.length >= 10) {
                                barcode = chars.join("");
                                $.post("./modelo/MOD.OrdenDespacho.Verificar_Producto.php", {ean_13: barcode}, function (data) {
                                    data = jQuery.parseJSON(data);
                                    if (data !== " NULL") {

                                        var existe = false;
                                        $('#myTableX tr').each(function () {
                                            if ($(this).find("td").eq(0).html() === data.ean13) {

                                                var cant = $(this).find("td").eq(3).text(); //Posicion de Cantidad
                                                cant = Number(cant);
                                                cant++;
                                                $(this).find("td").eq(3).text(cant); //NUeva Cantidad
                                                $(this).find("td").eq(4).text("$" + cant * data.precio); // Total 
                                                existe = true;
                                                return false;
                                            }
                                        });
                                        if (existe === false) {
                                            $('#lastDataX').before(
                                                    '<tr id="id_tr_remove">' +
                                                    ' <td>' + data.ean13 + '</td>' +
                                                    ' <td>' + data.nombre + '</td>' +
                                                    ' <td class="text-center">' + '$' + data.precio + '</td>' +
                                                    ' <td class="text-center">1</td>' +
                                                    ' <td class="text-right">' + '$' + data.precio + '</td>' +
                                                    ' </tr>'

                                                    );
                                        }
                                        calculos();
                                    }
                                });
                            }
                            chars = [];
                            pressed = false;
                        }, 100);
                    }
                    pressed = true;
                });
                $('#myTableX').on('click', 'tr', function () {
                    var dato = $(this).find("td").eq(2).html(); // Precio
                    var dato2 = $(this).find("td").eq(3).html(); //Cantidad

                    if (dato !== "" && dato !== "<strong>Precio</strong>" && dato2 !== "<strong>Cantidad</strong>") {

                        var d = prompt("Ingrese la cantidad que desea");
                        d = parseInt(d);
                        if (d === null || d === "null") {

                        } else if (!isNaN(d) && d > 0) {

                            $(this).find("td").eq(3).text(d); // Cantidad

                            var dato = $(this).find("td").eq(2).html(); // Precio
                            var dato2 = $(this).find("td").eq(3).html(); // Cantidad

                            dato = Number(dato.substr(1));
                            dato2 = Number(dato2);
                            $(this).find("td").eq(4).text("$" + (dato * dato2)); // Total

                            calculos();
                        } else if (d === 0 || d === "0") {
                            $('#id_tr_remove').remove();
                            //calculos();
                            var SUBTOTAL = 0;
                            $('#myTableX tr').each(function () {
                                var total = 0;

                                total = Number(total.substr(1));// Le saca el Signo peso a precio

                                //alert(total);

                                SUBTOTAL_SEND = Math.round(SUBTOTAL += total);
                                //Math.round(SUBTOTAL += cantidad * precio);

                                $('#cantidadSubTotal').html("$" + formatNumber.new(SUBTOTAL));

                                IVA = Math.round(SUBTOTAL * 19 / 100);
                                $('#cantidadIVA').html("$" + formatNumber.new(IVA));

                                TOTAL = Math.round(SUBTOTAL * 19 / 100 + SUBTOTAL);
                                $('#cantidadTotal').html("$" + formatNumber.new(TOTAL));


                            });
                        }
                    }
                });
                $("#barcode").keypress(function (e) {
                    if (e.which === 13) {
                        console.log("No se envia.");
                        e.preventDefault();
                    }
                });
                function calculos() {
                    var SUBTOTAL = 0;
                    $('#myTableX tr').each(function () {
                        //alert($(this).find("td").eq(3).html());

                        var precio = $(this).find("td").eq(2).html(); // precio
                        var cantidad = $(this).find("td").eq(3).html(); // Cantidad
                        var total = $(this).find("td").eq(4).html(); // Cantidad
                        //alert(total);
                        if (precio !== "" && precio !== "<strong>Precio</strong>" && cantidad !== "<strong>Cantidad</strong>") {

                            precio = Number(precio.substr(1)); // Le saca el Signo peso a precio

                            total = Number(total.substr(1));// Le saca el Signo peso a precio

                            cantidad = Number(cantidad);  // Cantidad 

                            //alert(total);

                            SUBTOTAL_SEND = Math.round(SUBTOTAL += total);
                            //Math.round(SUBTOTAL += cantidad * precio);

                            $('#cantidadSubTotal').html("$" + formatNumber.new(SUBTOTAL));

                            IVA = Math.round(SUBTOTAL * 19 / 100);
                            $('#cantidadIVA').html("$" + formatNumber.new(IVA));

                            TOTAL = Math.round(SUBTOTAL * 19 / 100 + SUBTOTAL);
                            $('#cantidadTotal').html("$" + formatNumber.new(TOTAL));
                        }

                    });
                }
                $('#btnGuardar').click(function () {
                    var TableData;
                    TableData = storeTblValues()
                    TableData = $.toJSON(TableData);
                    function storeTblValues() {
                        var TableData = new Array();
                        $('#myTableX tr').each(function (row, tr) {
                            TableData[row] = {
                                "ean13": $(tr).find('td:eq(0)').text(),
                                "item": $(tr).find('td:eq(1)').text(),
                                "precio": $(tr).find('td:eq(2)').text(),
                                "cantidad": $(tr).find('td:eq(3)').text(),
                                "total": $(tr).find('td:eq(4)').text()
                            }
                        });
                        TableData.shift(); // first row will be empty - so remove
                        return TableData;
                    }
                    var IDSUCURSAL = $("#bod_sucursal").val();
                    var NUM_ORDEN = $("#num_orden").html();
                    var FECHA_HORA = $("#fecha_hora").html();


                    if (IDSUCURSAL !== "") {
                        if (TableData !== "[]") {
                            $.ajax({
                                type: "POST",
                                url: './modelo/MOD.OrdenDespacho.Add.php',
                                data: "pTableData=" + TableData + '&SUBTOTAL=' + SUBTOTAL_SEND + '&IVA=' + IVA + '&TOTAL=' + TOTAL + '&IDUSER=' + <?php echo $IDUsuario; ?> + '&IDSUCURSAL=' + IDSUCURSAL + '&NUM_ORDEN=' + NUM_ORDEN + '&FECHA_HORA=' + FECHA_HORA,
                                success: function (data) {
                                    //$("#respuesta").html(data);
                                    alert(data);
                                    window.location.href = './VIEW.OrdenDespacho.Lista.php';
                                }, error: function () {
                                }
                            });
                        } else {
                            alert("Debe Ingresar un Producto.");
                        }
                    } else {
                        alert("Debe Seleccionar una Sucusal.");
                    }
                });
            });

            var formatNumber = {
                separador: ".", // separador para los miles
                sepDecimal: ',', // separador para los decimales
                formatear: function (num) {
                    num += '';
                    var splitStr = num.split('.');
                    var splitLeft = splitStr[0];
                    var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
                    var regx = /(\d+)(\d{3})/;
                    while (regx.test(splitLeft)) {
                        splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
                    }
                    return this.simbol + splitLeft + splitRight;
                },
                new : function (num, simbol) {
                    this.simbol = simbol || '';
                    return this.formatear(num);
                }
            }
        </script>

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

    </body>
</html>