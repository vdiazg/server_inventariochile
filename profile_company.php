<?php
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UserNameUsuario = $_SESSION["datos_usuario_logueado"][10];

if (!isset($IDUsuario)) {
    header('Location: login.php');
}
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- ######################################## -->
        <?php require_once './includes/head.inc.php'; ?>
        <!-- ######################################## -->
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <div id="wrapper" class="preload">
            <!-- ######################################## -->
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <!-- ######################################## -->
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="index.php"> Home</a></li>
                        <li class="active">Editar Datos de Empresa</li>  
                    </ul>
                </div><!--breadcrumb--><!--
                <ul class="tab-bar grey-tab">
                    <li class="active">
                        <a href="#overview" data-toggle="tab"><span class="block text-center"><i class="fa fa-pencil-square-o fa-2x"></i></span>Editar Perfil</a>
                    </li>
                </ul>-->

                <div class="padding-md">
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <div class="row padding-md text-center">
                                <a href="#formModal_UpdateIMG" role="button" data-toggle="modal"><img src="<?php 
                                $RutaImgUsuarioUpdate = $db->single("SELECT user_ruta_img FROM Usuarios WHERE user_id = '$IDUsuario'");
                                    if($RutaImgUsuarioUpdate == ""){
                                        $RutaImgUsuarioUpdate = "./img/imagen_profile_default.png";
                                        echo $RutaImgUsuarioUpdate;
                                    }else{
                                        echo $RutaImgUsuarioUpdate;
                                    }
                                ?>" alt="User Avatar" class="img-thumbnail" style="width:200px; height:200px"></a>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <strong class="font-14"><?php echo $NombreUsuario . " " . $ApellidoPaterno . " ". $ApellidoMaterno; ?></strong>
                                    <small class="block text-muted"><?php echo $CorreoElectronico; ?></small> 
                                    <small class="block text-muted"><?php echo $TelMovil; ?></small> 
                                    <small class="block text-muted"><?php echo $PerfilUsuario; ?></small> 
                                    <div class="seperator"></div>
                                    <div class="seperator"></div>
                                    <div class="seperator"></div>
                                    <div class="seperator"></div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.col -->
                        <div class="col-md-9 col-sm-9">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="overview">
                                    <div class="panel panel-default">
                                        <form class="form-horizontal form-border" id="form_Update_DatosUsuarioSistema" method="POST">
                                            <div class="panel-heading">
                                                Modificar Datos Personales
                                            </div>
                                            <div class="panel-body">
                                                <input type="text" id="IDUpdateUsuario" name="IDUpdateUsuario" class="hide" value="<?php echo $IDUsuario; ?>">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Nombre de Usuario</label>                                             
                                                    <div class="col-md-10">
                                                        <input type="text" disabled="" class="form-control input-sm" placeholder="Username" value="<?php echo $UserNameUsuario; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">RUT</label>                                             
                                                    <div class="col-md-10">
                                                        <input type="text" disabled="" class="form-control input-sm" placeholder="RUT" value="<?php echo $RutUsuario; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Nombre o Nombres</label>                                              
                                                    <div class="col-md-10">
                                                        <input type="text" id="UpdateNombreUsuario" name="UpdateNombreUsuario" class="form-control input-sm" placeholder="Nombre" value="<?php echo $NombreUsuario; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Apellido Paterno</label>                                              
                                                    <div class="col-md-10">
                                                        <input type="text" id="UpdateApellidoPaterno" name="UpdateApellidoPaterno" class="form-control input-sm" placeholder="Apellido Paterno" value="<?php echo $ApellidoPaterno; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Apellido Materno</label>                                              
                                                    <div class="col-md-10">
                                                        <input type="text" id="UpdateApellidoMaterno" name="UpdateApellidoMaterno" class="form-control input-sm" placeholder="Apellido Materno" value="<?php echo $ApellidoMaterno; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Fecha Nacimiento</label>                                              
                                                    <div class="col-md-10">
                                                        <input id="UpdateFechaNacimiento" name="UpdateFechaNacimiento" readonly="" type="text" class="form-control input-sm" placeholder="Fecha Nacimiento" value="<?php echo $FechaNacimiento; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Contraseña</label>
                                                    <div class="col-md-10">
                                                        <input type="password" id="UpdateContrasena" name="UpdateContrasena" class="form-control input-sm" value="">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Reingrese Contraseña</label>
                                                    <div class="col-md-10">
                                                        <input type="password" id="UpdateConfirmContrasena" name="UpdateConfirmContrasena" class="form-control input-sm" value="">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Correo Electrónico</label>
                                                    <div class="col-md-10">
                                                        <input type="text" id="UpdateCorreoElectronico" name="UpdateCorreoElectronico" class="form-control input-sm" value="<?php echo $CorreoElectronico; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Teléfono Móvil</label>
                                                    <div class="col-md-10">
                                                        <input type="text" id="UpdateTelMovil" name="UpdateTelMovil" class="form-control input-sm" value="<?php echo $TelMovil; ?>">
                                                    </div><!-- /.col -->
                                                </div><!-- /form-group -->
                                            </div>
                                            <div class="panel-footer">
                                                <div class="row">                                        
                                                    <div class="col-md-8 text-left">
                                                        <div id="msj_respuesta_update_usuarios"></div>
                                                    </div>
                                                    <div class="col-md-4 text-right">
                                                        <button type="submit" id="btn_actualizar_datos" class="btn btn-sm btn-warning btn-block">Actualizar</button>
                                                    </div>                                                
                                            </div>
                                        </form>
                                    </div><!-- /panel -->
                                </div><!-- /tab1 -->
                            </div><!-- /tab-content -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->            
                </div><!-- /.padding-md -->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>
        <!-- ########################################################### -->
        <div id="formModal_UpdateIMG" class="modal fade in" role="dialog" style="">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="form_Update_img_usuario_inventario" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Modificar Descripción a Actualizar</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 text-center" style="border-right: 1px solid #D2D2D2;"><img id="img_user" class=" img-thumbnail img-responsive" style="width:200px; height:200px" src="<?php 
                                    $RutaImgUsuarioUpdate = $db->single("SELECT user_ruta_img FROM Usuarios WHERE user_id = '$IDUsuario'");
                                    if($RutaImgUsuarioUpdate == ""){
                                        $RutaImgUsuarioUpdate = "./img/imagen_profile_default.png";
                                        echo $RutaImgUsuarioUpdate;
                                    }else{
                                        echo $RutaImgUsuarioUpdate;
                                    }
                                ?>" alt="User Avatar" ></br><span class="label label-warning"><font color="#000000;">Imagen 200px * 200px</font></span></div>
                                <div class="col-md-6 text-center" id="div_archivos_img"><input class="btn btn-sm" type="file" id="archivos_img" name="archivos_img"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left" id="msj_respuesta_update_img_usuario_inventario">
                                </div>
                                <div class="col-md-4 text-right">
                                    <button id="btn_cerrar_modal" type="button" class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm" type="button">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- ########################################################### -->
        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        
        <script src="js/js-nfconnection/jquery.validate.min.js"></script>
        <script src="js/js-nfconnection/custom_jquery_validate.js"></script>

        <script src='js/js-nfconnection/datepicker_js/bootstrap-datepicker.js'></script>
        <script src='js/js-nfconnection/datepicker_js/locales/bootstrap-datepicker.es.js'></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#UpdateFechaNacimiento').datepicker({
                    language: "es",
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    todayHighlight: true
                });


                $('#btn_cerrar_modal').click(function() {
                    location.reload();
                });
            });
            $(function () {
                $('#enviar').click(SubirFotos); //Capturamos el evento click sobre el boton con el id=enviar    y ejecutamos la función seleccionado.
                //var a = $("#archivos_img").attr("src");
            });

            function SubirFotos() {
                var archivos_img = document.getElementById("archivos_img");//Creamos un objeto con el elemento que contiene los archivos_img: el campo input file, que tiene el id = 'archivos_img'
                var archivo = archivos_img.files; //Obtenemos los archivos_img seleccionados en el imput
                //Creamos una instancia del Objeto FormDara.
                var archivos_img = new FormData();
                /* Como son multiples archivos_img creamos un ciclo for que recorra la el arreglo de los archivos_img seleccionados en el input
                 Este y añadimos cada elemento al formulario FormData en forma de arreglo, utilizando la variable i (autoincremental) como 
                 indice para cada archivo, si no hacemos esto, los valores del arreglo se sobre escriben*/
                for (i = 0; i < archivo.length; i++) {
                    archivos_img.append('archivo_img' + i, archivo[i]); //Añadimos cada archivo a el arreglo con un indice direfente
                }
                $.ajax({
                    url: 'modelo/img_update_avatar_usuario.php', //Url a donde la enviaremos
                    type: 'POST', //Metodo que usaremos
                    contentType: false, //Debe estar en false para que pase el objeto sin procesar
                    data: archivos_img, //Le pasamos el objeto que creamos con los archivos_img
                    processData: false, //Debe estar en false para que JQuery no procese los datos a enviar
                    cache: false //Para que el formulario no guarde cache
                }).done(function (msg) { //Escuchamos la respuesta y capturamos el mensaje msg
                    //$("#msj_respuesta_update_img_usuario_inventario").html(msg);
                    $('#img_user').attr("src", 'img/ajax-loader.gif');
                    $("#img_user").attr("src", msg);
                });
            }
        </script>
        <!--<script src="js/menu-active-class.js"></script>-->

        <!-- Colorbox -->
        <script src='js/jquery.colorbox.min.js'></script>   

        <!-- Sparkline -->
        <script src='js/jquery.sparkline.min.js'></script>

        <!-- Pace -->
        <script src='js/uncompressed/pace.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <!--<script src="js/endless/endless_dashboard.js"></script>-->
        <script src="js/endless/endless.js"></script>

    </body>
</html>
