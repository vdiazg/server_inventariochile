<?php
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UserNameUsuario = $_SESSION["datos_usuario_logueado"][10];

$id = filter_input(INPUT_GET, 'id');

if (!isset($IDUsuario)) {
    header('Location: login.php');
}

require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="dashboard.php"> Home</a></li>
                        <li class="active">Enrolamiento de Usuario</li>	 
                    </ul>
                </div>
                <?php
                $SQL_DATA_SELECT_USUARIOS = $db->query("SELECT * FROM Usuarios U, Login L WHERE U.user_id ='" . $id . "' AND U.user_id = L.Usuarios_user_id");
                foreach ($SQL_DATA_SELECT_USUARIOS as $row):

                    $USUARIO_ID_BD = $row["user_id"];
                    $USUARIO_RUT = $row["user_rut"];
                    $USUARIO_PNOMBRE = $row["user_nombre"];
                    $USUARIO_SNOMBRE = $row["user_segundo_nombre"];
                    $USUARIO_PAPELLIDO = $row["user_ap"];
                    $USUARIO_SAPELLIDO = $row["user_am"];
                    $USUARIO_GENERO = $row["user_genero"];
                    $USUARIO_FECHA_NACIMIENTO = $row["user_fecha_nacimiento"];
                    $USUARIO_PERFIL = $row["Perfiles_perfil_nombre"];
                    $USUARIO_NOTA = $row["user_nota"];
                    $USUARIO_IMG = $row["user_ruta_img"];
                    $USUARIO_Username = $row["login_username"];
                    $USUARIO_Contrasena = $row["login_password"];

                endforeach;
                ?>
                <form class="form-login paddingTB-md" id="JSV_ModificarEmpleado" method="POST">
                    <!-- INFORMACION PERSONAL -->                    
                    <div class="panel panel-default table-responsive ">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-4">
                                    <i class="fa fa-plus-circle"></i> INFORMACIÓN PERSONAL
                                </div>
                                <div class="col-md-8">
                                    <div id="msj_respuesta"></div>
                                </div>
                            </div>
                        </div>
                        <div class="padding-md clearfix">
                            <!-- INICIO Código dentro cuadro blanco -->
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="emple_run">RUN</label><input value="<?php echo $USUARIO_RUT ?>" id="emple_run" name="emple_run" type="text" placeholder="ej. 17945182-3" class="form-control input-sm "  ></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="emple_nombre">Primer Nombre</label><input value="<?php echo $USUARIO_PNOMBRE ?>" id="emple_nombre" name="emple_nombre" value="" type="text" placeholder="ej. Víctor, Marta Ester" class="form-control input-sm  " ></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="emple_segundo_nombre">Segundo Nombre</label><input value="<?php echo $USUARIO_SNOMBRE ?>" id="emple_segundo_nombre" name="emple_segundo_nombre" type="text" placeholder="ej. Víctor, Marta Ester" class="form-control input-sm  " ></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="emple_ap">Apellido Paterno</label><input value="<?php echo $USUARIO_PAPELLIDO ?>" id="emple_ap" name="emple_ap" type="text" placeholder="ej. Díaz" class="form-control input-sm  " ></div></div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="emple_am">Apellido Materno</label><input value="<?php echo $USUARIO_SAPELLIDO ?>" id="emple_am" name="emple_am" type="text" value="" placeholder="ej. Martínez" class="form-control input-sm  " ></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="emple_genero">Género del Usuario (Masculino/Femenino)</label><select class="form-control input-sm" id="emple_genero" name="emple_genero"><option value="">Seleccione</option><option value="Masculino">Masculino</option><option value="Femenino">Femenino</option></select></div></div>
                                    <div class="col-md-3"><div class="form-group"><label class="control-label" for="emple_fecha_nacimiento">Fecha Nacimiento</label><input value="<?php echo $USUARIO_FECHA_NACIMIENTO ?>" readonly="false" id="emple_fecha_nacimiento" name="emple_fecha_nacimiento" type="text" placeholder="dd-mm-aaaa" class="form-control input-sm  " ></div></div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Permiso que tendra el Usuario </label>
                                            <select class="form-control input-sm" id="emple_perfil" name="emple_perfil">
                                                <option value="">Seleccione</option>
                                                <?php
                                                $datos_sql_select_permisos = $db->query("SELECT perfil_nombre FROM Perfiles");
                                                foreach ($datos_sql_select_permisos as $row):
                                                    echo "<option value=" . $row["perfil_nombre"] . ">" . $row["perfil_nombre"] . "</option>";
                                                endforeach
                                                ?>
                                            </select>
                                            <!--
                                            <div class="row">
                                                <div class="col-md-6 text-left"><a href="#formModal_AgregarComponente" role="button" data-toggle="modal" class="btn-link btn-sm"><i class="fa fa-plus"></i> Agregar Permiso</a></div>
                                                <div class="col-md-6 text-right"><a href="#formModal_EliminarComponente" role="button" data-toggle="modal" class="btn-link btn-sm"><i class="fa fa-minus"></i> Eliminar Permiso</a></div>
                                            </div>-->
                                        </div>                                
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6 no-padding">
                                        <div class="alert alert-animated"> Ingreso de Credenciales para el Acceso a la Plataforma</div>
                                        <div class="col-md-4"><div class="form-group"><label class="control-label" for="emple_username">Email del Usuario</label><input value="<?php echo $USUARIO_Username ?>" id="emple_username" name="emple_username" type="email" placeholder="" class="form-control input-sm  " ></div></div>
                                        <div class="col-md-4"><div class="form-group"><label class="control-label" for="emple_contrasena">Contraseña</label><input value="<?php echo $USUARIO_Contrasena ?>"  id="emple_contrasena" name="emple_contrasena" type="password" placeholder="" class="form-control input-sm  " ></div></div>
                                        <div class="col-md-4"><div class="form-group"><label class="control-label" for="emple_confir_contrasena">Confirmar Contraseña</label><input value="<?php echo $USUARIO_Contrasena?>" id="emple_confir_contrasena" name="emple_confir_contrasena" type="password" placeholder="" class="form-control input-sm " ></div></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">                                        
                                                        <div id="error_img"></div>
                                                        <div class="form-group text-center">                                        
                                                            <div class="col-md-12">
                                                                <div id="contenedorImagen">
                                                                    <div class="row">
                                                                        <div class="form-group pull-left">
                                                                            <label class="control-label">Imagen de Perfil</label>                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <img id="fotografia" class="img-thumbnail padding-xs" src="<?php echo $USUARIO_IMG; ?>" style="width: 100px; height: 100px;" />
                                                                    </div>
                                                                    <div class="row" style="margin-top: 15px;">
                                                                        <button class="btn btn-default btn-block btn-social" id="addImage"><i class="fa fa-upload"></i><b>Imagen de perfil</b></button>
                                                                        <div class="" style="display: none;" id="loaderAjax">
                                                                            <img src="img/ajax-loader.gif">
                                                                            <span>Publicando Fotografía...</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label" for="emple_nota">Nota</label>
                                                <textarea value="" class="form-control input-sm" id="emple_nota" name="emple_nota" placeholder="Solo se Permiten 200 Caracteres" maxlength="200" rows="6"><?php echo $USUARIO_NOTA ?></textarea>
                                            </div>                                
                                        </div>
                                    </div>                                        
                                </div>
                                <hr>
                                <!-- BOTON GUARDAR -->
                                <div class="row">                                        
                                    <div class="col-md-12">
                                        <button type="button" id="add_empleado_boton_cancelar" class="btn btn-default pull-left"><i class="fa fa-times fa-2x"></i> <br/>Cancelar</button>
                                        <button type="submit" class="btn btn-default pull-right"><i class="fa fa-save fa-2x"></i> <br/>Guardar</button>
                                    </div>
                                </div>
                                <!-- FIN BOTON GUARDAR -->
                            </div>
                            <!-- FIN  Código dentro cuadro blanco -->
                        </div>
                    </div><!-- /panel -->
                    <!-- FIN INFORMACION PERSONAL -->
                </form>
            </div>
        </div>

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <?php require_once './includes/footer.inc.php'; ?>

        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>
        <!-- MODALS -->
        <!-- ########################################################### -->
        <form id="form_Insert_AgregaComponente" method="POST">
            <div class="modal fade" id="formModal_AgregarComponente">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Ingresar un Nuevo Permiso</h4>
                        </div>
                        <div class="modal-body">
                            <label class="control-label">Nombre de permiso</label>                                            
                            <input type="text" placeholder="" id="ingreso_permiso" name="ingreso_permiso" class="form-control input-sm" >
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-8 text-left" id="msj_respuesta_ingreso_componente">

                                </div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                    <button id="enviar" class="btn btn-success btn-sm">Guardar</button>
                                </div>
                            </div>                            
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <!-- ########################################################### -->
        <form id="form_Insert_AgregaComponente" method="POST">
            <div class="modal fade" id="formModal_VerTablaGrupoSanguineo">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Tabla de Grupo Sanguineos</h4>
                        </div>
                        <div class="modal-body text-center  ">
                            <img src="img/grupos_sanguineos/tabla_grupos_sanguineos.jpg" />
                        </div>
                        <div class="modal-footer">

                            <button class="btn btn-success btn-sm text-left" data-dismiss="modal" aria-hidden="true">Cerrar</button>                          
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
        <!-- ########################################################### -->
        <!-- MODALS -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!--Bootstrap-->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                //alert($('#fotografia').attr('src'));
                var Patch_Img;
                // Botón para subir la firma
                var btn_firma = $('#addImage'), interval;
                new AjaxUpload('#addImage', {
                    action: './modelo/uploadFile.php',
                    onSubmit: function (file, ext) {
                        if (!(ext && /^(jpg|png|jpeg)$/.test(ext))) {
                            // extensiones permitidas
                            alert('Sólo se permiten Imagenes .jpg, .png y/o .jpeg');
                            // cancela upload
                            return false;
                        } else {
                            $('#loaderAjax').show();
                            //btn_firma.text('Espere por favor');
                            this.disable();
                        }
                    },
                    onComplete: function (file, response) {
                        //alert(response);
                        //btn_firma.text('Subir Imagen');
                        var respuesta = $.parseJSON(response);
                        if (respuesta.respuesta == 'done') {
                            Patch_Img = './img/avatars_users/' + respuesta.fileName;
                            $('#fotografia').removeAttr('scr');
                            $('#fotografia').attr('src', Patch_Img);
                            $('#loaderAjax').show();
                            //alert(respuesta.mensaje);
                        }
                        else {
                            alert(respuesta.mensaje);
                        }
                        $('#loaderAjax').hide();
                        this.enable();
                    }
                });
            });
        </script>
        <script src="js/js-nfconnection/jquery.validate.min.js"></script>
        <script src="js/js-nfconnection/custom_jquery_validate.js"></script>

        <script src='js/js-nfconnection/datepicker_js/bootstrap-datepicker.js'></script>
        <script src='js/js-nfconnection/datepicker_js/locales/bootstrap-datepicker.es.js'></script>
        <script src="js/js-nfconnection/AjaxUpload.2.0.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#btn_agregar_otro_campo1').click(function () {
                    $("#row_enfermedad2").removeClass("hide");
                    $("#hide_btn1").addClass("hide");
                });
                $('#btn_agregar_otro_campo2').click(function () {
                    $("#row_enfermedad3").removeClass("hide");
                    $("#hide_btn2").addClass("hide");
                });
                $('#btn_agregar_otro_campo3').click(function () {
                    $("#row_enfermedad4").removeClass("hide");
                    $("#hide_btn3").addClass("hide");
                });
                $('#btn_agregar_otro_campo4').click(function () {
                    $("#row_enfermedad5").removeClass("hide");
                    $("#hide_btn4").addClass("hide");
                });
                $("#div_id_username_pass").hide();
                $("#row_id_empleado").hide();
                $('#emple_fecha_nacimiento').datepicker({
                    language: "es",
                    startView: 2,
                    startDate: "01/01/1910",
                    endDate: "31/12/2015",
                    /*endDate: "today",*/
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    todayHighlight: true
                });

                $("#div_id_username_pass").hide();
                $("#row_id_empleado").hide();
                //Seleccion de perfiles
                $('#select_Permiso_usuario').on('click', function (e) {
                    var user_perfil = $("#select_Permiso_usuario").val();
                    if (user_perfil == '') {
                        //vacio
                        $("#select_ano_ingreso_user").val("");
                        $("#select_carrera_usuario").val("");
                        $("#select_jornada_user").val("");
                        $('#form_group_ano').removeClass('has-success');
                        $('#form_group_carrera').removeClass('has-success');
                        $('#form_group_jornada').removeClass('has-success');
                        $("#div_id_username_pass").hide();
                        $("#row_id_empleado").show();
                    }
                    //Administrador
                    if (user_perfil == 'ADMINISTRADOR') {
                        //Administrador
                        $("#select_ano_ingreso_user").val("");
                        $("#select_carrera_usuario").val("");
                        $("#select_jornada_user").val("");
                        $('#form_group_ano').removeClass('has-success');
                        $('#form_group_carrera').removeClass('has-success');
                        $('#form_group_jornada').removeClass('has-success');
                        $("#div_id_username_pass").show();
                        $("#row_id_empleado").hide();
                    }
                    //Administrativo
                    if (user_perfil == 'SUPERADMIN') {
                        //Administrativo
                        $("#select_ano_ingreso_user").val("");
                        $("#select_carrera_usuario").val("");
                        $("#select_jornada_user").val("");
                        $('#form_group_ano').removeClass('has-success');
                        $('#form_group_carrera').removeClass('has-success');
                        $('#form_group_jornada').removeClass('has-success');
                        $("#div_id_username_pass").show();
                        $("#row_id_empleado").hide();
                    }
                    if (user_perfil == 'CLIENTE') {
                        //Empleado
                        $("#select_ano_ingreso_user").val("");
                        $("#select_carrera_usuario").val("");
                        $("#select_jornada_user").val("");
                        $('#form_group_ano').removeClass('has-success');
                        $('#form_group_carrera').removeClass('has-success');
                        $('#form_group_jornada').removeClass('has-success');
                        $("#div_id_username_pass").hide();
                        $("#row_id_empleado").show();
                    }

                });

                // BUSQUEDA DE TID
                $('#btn_buscar_tid').on('click', function (e) {
                    var dataString = 'input_buscar_tid=' + $("#input_buscar_tid").val();
                    //$('#ADMIN_user_N_SERIE_PULSERA_NFC').html('<img src="./img/ajax-loader.gif" alt="" />');

                    $.ajax({
                        type: "POST",
                        url: "./modelo/registro_user_buscar_tid.php",
                        data: dataString,
                        success: function (data) {
                            //alert(data);
                            $("#ADMIN_user_N_SERIE_PULSERA_NFC").val(data);
                        }
                    });
                });
                // FIN BUSQUEDA DE TID

                /*var dataString = 'input_uid=' + input_uid;
                 $('#btn_uid').on('click', function (e) {
                 $.ajax({
                 type: "POST",
                 url: "./modelo/escanear_tag.php",
                 data: dataString,
                 success: function (data) {
                 //alert(data);
                 $("#input_uid").val(data);
                 }
                 });
                 });*/
            });
        </script>
        <script src="js/menu-active-class.js"></script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Bootstrap Switch -->
        <!--<script src='js/js-nfconnection/bootstrap-switch.js'></script>-->

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

        <script> //Script que al presionar "cancelar" devuelve a la pagina anterior
            $("#add_empleado_boton_cancelar").click(function () {
                window.location.replace("./users_empleados.php");
            });
        </script>

        <script>
            
        </script>

    </body>
</html>