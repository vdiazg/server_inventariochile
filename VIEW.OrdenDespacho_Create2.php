<?php
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UserNameUsuario = $_SESSION["datos_usuario_logueado"][10];

if (!isset($IDUsuario)) {
    header('Location: login.php');
}

require_once './controlador/Db.class.php';
$db = new Db();

//Obtener datos dia despacho
$fecha = getdate();
//$order = $fecha['mday'] . $fecha['mon'] . substr($fecha['year'], -2) . $fecha['hours'] . $fecha['minutes'];
$order = date("YmdHis");
$fecha2 = $fecha['mday'] . '/' . $fecha['mon'] . '/' . $fecha['year'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
        <style>
            .invoice-title h2, .invoice-title h3 {
                display: inline-block;
            }

            .table > tbody > tr > .no-line {
                border-top: none;
            }

            .table > thead > tr > .no-line {
                border-bottom: none;
            }

            .table > tbody > tr > .thick-line {
                border-top: 2px solid;
            }
        </style>
    </head>

    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>

        <div id="wrapper" class="bg-white preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>

            <div id="main-container">
                <div class="padding-md">
                    <div class="clearfix">
                        <div class="pull-left">
                            <span class="img-demo">
                                Logo
                            </span>
                            <div class="pull-left m-left-sm">
                                <h3 class="m-bottom-xs m-top-xs">Endless Admin</h3>
                                <span class="text-muted">endless.themes@gmail.com</span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <h5><strong>#<?php echo $order; ?></strong></h5>
                            <strong><?php echo date("Y-m-d H:i:s"); ?></strong>
                        </div>
                    </div>
                    <hr>
                    <div class="clearfix">
                        <div class="pull-left"> 
                            <h4>De:</h4> 
                            <address> 
                                <strong><?= $NombreUsuario . ' ' . $ApellidoPaterno ?></strong><br>
                            </address> 
                        </div>
                        <div class="pull-right text-right">
                            <label class="control-label"><strong><h4>Dirigido a:</h4> </strong></label>
                            <select class="form-control input-sm" id="bod_sucursal" name="bod_sucursal">
                                <option value="">Seleccione</option>
                                <?php
                                $DATOS_SQL_SUCURSAL = $db->query("SELECT sucursal_id, sucursal_nombre FROM Sucursales");
                                foreach ($DATOS_SQL_SUCURSAL as $row):
                                    echo "<option value=" . $row["sucursal_id"] . ">" . $row["sucursal_nombre"] . "</option>";
                                endforeach
                                ?>
                            </select>

                        </div>
                    </div>
                    <div id="myTableX">
                        <table class="table table-striped m-top-md" id="">
                            <thead>
                                <tr class="bg-theme">
                                    
                                    <th>Producto</th>
                                    <th>Precio Unitario</th>
                                    <th class="hidden-xs">Cantidad</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="lastDataX">
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"><strong>Subtotal</strong></td>
                                    <td class="thick-line text-right" id="cantidadSubTotal"></td>
                                </tr>
                                <!--
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"><strong>IVA</strong></td>
                                    <td class="no-line text-right" id="cantidadIVA"></td>
                                </tr>
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"><strong>Total</strong></td>
                                    <td class="no-line text-right" id="cantidadTotal"></td>
                                </tr>-->
                            </tbody>
                        </table>

                        <div class="padding-sm bg-grey">
                            <p>Note : Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eros nibh, viverra a dui a, gravida varius velit. Nunc vel tempor nisi. Aenean id pellentesque mi, non placerat mi. Integer luctus accumsan tellus. Vivamus quis elit sit amet nibh lacinia suscipit eu quis purus. Vivamus tristique est non ipsum dapibus lacinia sed nec metus.</p> 
                        </div>

                        <div class="pull-right">
                            <table class="table m-top-md">	
                                <tbody>
                                    <tr class="no-border">
                                        <td class="no-border"></td>
                                        <td class="no-border"></td>
                                        <td class="no-border"></td>
                                        <td class="text-right no-border"><strong>Subtotal</strong></td>
                                        
                                    </tr>
                                    <tr class="no-border">
                                        <td class="no-border"></td>
                                        <td class="no-border"></td>
                                        <td class="no-border"></td>
                                        <td class="text-right no-border"><strong>Shipping</strong></td>
                                        <td><strong>$5.00</strong></td>
                                    </tr>
                                    <tr class="no-border">
                                        <td class="no-border"></td>
                                        <td class="no-border"></td>
                                        <td class="no-border"></td>
                                        <td class="text-right no-border"><strong>Vat</strong></td>
                                        <td><strong>$50.00</strong></td>
                                    </tr>
                                    <tr class="no-border">
                                        <td class="no-border"></td>
                                        <td class="no-border"></td>
                                        <td class="no-border"></td>
                                        <td class="text-right no-border"><strong>Total</strong></td>
                                        <td><strong class="text-danger">$545.00</strong></td>
                                    </tr>
                                </tbody>
                            </table>

                            <a class="btn btn-success hidden-print" id="invoicePrint"><i class="fa fa-print"></i> Print</a>
                            <a class="btn btn-success hidden-print"><i class="fa fa-usd"></i> Proceed to Payment</a>
                        </div>
                    </div>
                </div><!-- /.padding20 -->
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <?php require_once './includes/footer.inc.php'; ?>

        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!--Bootstrap-->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src="js/js-nfconnection/jquery.validate.min.js"></script>
        <script src="js/js-nfconnection/custom_jquery_validate.js"></script>

        <script src="js/menu-active-class.js"></script>

        <script>
            $(document).ready(function () {
                /* IMPRESION */
                $('#invoicePrint').click(function () {
                    window.print();
                });
                $("#btnPrint").click(function () {
                    var divContents = $("#dvContainer").html();

                    var printWindow = window.open('', 'my div', 'height=400,width=600');

                    printWindow.document.write('<html><head><title>DIV Contents</title>');
                    printWindow.document.write('<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">');
                    printWindow.document.write('</head><body>');
                    printWindow.document.write(divContents);
                    printWindow.document.write('</body></html>');
                    printWindow.print();
                    printWindow.close();

                });
                /* IMPRESION */
                var pressed = false;
                var chars = [];
                $(window).keypress(function (e) {
                    if (e.which >= 48 && e.which <= 57) {
                        chars.push(String.fromCharCode(e.which));
                    }
                    if (pressed == false) {
                        setTimeout(function () {
                            if (chars.length >= 10) {
                                var barcode = chars.join("");

                                $.post("./modelo/MOD.OrdenDespacho.Verificar_Producto.php", {ean_13: barcode}, function (data) {
                                    data = jQuery.parseJSON(data);
                                    if (data !== " NULL") {
                                        var existe = false;

                                        $('#myTableX tr').each(function () {
                                            if ($(this).find("td").eq(0).html() === data.nombre) {

                                                var cant = $(this).find("td").eq(2).text();
                                                cant = Number(cant);
                                                cant++;
                                                $(this).find("td").eq(2).text(cant);
                                                $(this).find("td").eq(3).text("$" + cant * data.precio);
                                                existe = true;
                                                return false;
                                            }
                                        });

                                        if (existe === false) {
                                            var num = 1;
                                            $('#lastDataX').before(
                                                    '<tr>' +
                                                    ' <td>' + data.nombre + '</td>' +
                                                    ' <td class="">' + '$' + data.precio + '</td>' +
                                                    ' <td class="">1</td>' +
                                                    ' <td class="text-right">' + '$' + data.precio + '</td>' +
                                                    ' </tr>'

                                                    );
                                        }
                                        calculos();
                                    }
                                });

                            }
                            chars = [];
                            pressed = false;
                        }, 100);
                    }
                    pressed = true;

                });

                $('#myTableX').on('click', 'tr', function () {
                    var dato = $(this).find("td").eq(1).html();
                    var dato2 = $(this).find("td").eq(2).html();
                    
                    if (dato !== "" && dato !== "<strong>Precio</strong>" && dato2 !== "<strong>Cantidad</strong>") {

                        var d = prompt("Ingrese la cantidad que desea");

                        if (!isNaN(d) && d > 0) {
                            $(this).find("td").eq(2).text(d);
                            
                            var dato = $(this).find("td").eq(1).html();
                            var dato2 = $(this).find("td").eq(2).html();
                            
                            dato = Number(dato.substr(1));
                            dato2 = Number(dato2);
                            
                            $(this).find("td").eq(3).text("$" + (dato * dato2));
                            
                            calculos();
                            
                        } else {
                            alert(d + " es invalido");
                        }
                    }
                });
                $("#barcode").keypress(function (e) {
                    if (e.which === 13) {
                        console.log("No se envia.");
                        e.preventDefault();
                    }
                });

                function calculos() {

                    var subtotal = 0;

                    $('#myTableX tr').each(function () {

                        var dato = $(this).find("td").eq(1).html();
                        var dato2 = $(this).find("td").eq(2).html();
                        if (dato !== "" && dato !== "<strong>Precio</strong>" && dato2 !== "<strong>Cantidad</strong>") {

                            dato = Number(dato.substr(1));
                            dato2 = Number(dato2);
                            subtotal += dato * dato2;
                        }

                    });

                    $('#cantidadSubTotal').html("$" + subtotal);

                    $('#cantidadIVA').html("$" + subtotal * 19 / 100);

                    $('#cantidadTotal').html("$" + (subtotal * 19 / 100 + subtotal));

                }
            });
        </script>

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

        <script>
            $(function () {

            });
        </script>

    </body>
</html>

