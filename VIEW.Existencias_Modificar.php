<?php
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UserNameUsuario = $_SESSION["datos_usuario_logueado"][10];
$id = filter_input(INPUT_GET, 'id');
if (!isset($IDUsuario)) {
    header('Location: login.php');
}

require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="dashboard.php"> Home</a></li>
                        <li class="active">Añadir Nueva Existencia</li>	 
                    </ul>
                </div>
                <form class="form-login paddingTB-md" id="JSV_AddExistencias" method="POST">
                    <!-- INFORMACION PERSONAL -->
                    <div class="panel panel-default table-responsive ">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-4">
                                    <i class="fa fa-plus-circle"></i> AÑADIR EXISTENCIA                           
                                </div>
                                <div class="col-md-8">
                                    <div id="msj_respuesta"></div>
                                </div>
                            </div>
                        </div>
                        <div class="padding-md clearfix">
                            <!-- INICIO Código dentro cuadro blanco -->
                            <div class="panel-body">
                                <?php
                                $SQL_DATA_SELECT = $db->query("SELECT * FROM Productos WHERE producto_id  = '" . $id . "'");

                                foreach ($SQL_DATA_SELECT as $row):

                                    $PRODUCTO_ID_BD = $row["producto_id"];
                                    $PRODUCTO_IMG = $row["producto_ruta_img"];
                                    $PRODUCTO_NOMBRE = $row["producto_nombre"];
                                    $PRODUCTO_REF = $row["producto_cod_ref"];
                                    $PRODUCTO_EAN13 = $row["producto_ean13"];
                                    $PRODUCTO_DESCRIP = $row["producto_descrip_corta"];
                                    $PRODUCTO_Cantidad = $db->single("SELECT productcant_fisica FROM ProductCant WHERE Productos_producto_id = '" . $PRODUCTO_ID_BD . "'");
                                    $PRODUCTO_CATEGORIA = $row["ProductCategorias_productcategorias_id"];
                                    $NOMBRE_CATEGORIA = $db->single("SELECT productcategorias_nombre FROM ProductCategorias WHERE productcategorias_id = '" . $PRODUCTO_CATEGORIA . "'");
                                    $precio = $row['producto_precio'];
                                endforeach;
                                ?>
                                <!-- Inicio: Inputs -->
                                <div class="row">
                                    <div class="col-md-4"><div class="form-group"><label class="control-label" fon="producto_nombre">Nombre Producto</label><input id="producto_nombre" name="producto_nombre" type="text" placeholder="" class="form-control input-sm" readonly="" value="<?= $PRODUCTO_NOMBRE; ?>"></div></div>
                                    <div class="col-md-4"><div class="form-group"><label class="control-label" fon="producto_ref">Referencia Producto</label><input id="producto_ref" name="producto_ref" type="text" placeholder="" class="form-control input-sm" readonly="" value="<?= $PRODUCTO_REF; ?>"></div></div>
                                    <div class="col-md-4"><div class="form-group"><label class="control-label" for="producto_cantidad">Cantidad</label><input id="producto_cantidad" name="producto_cantidad" value="<?= $PRODUCTO_Cantidad; ?>" type="text" placeholder="" class="form-control input-sm"></div></div>
                                </div>
                                <!-- Fin: Inputs -->
                                <hr>
                                <!-- BOTON GUARDAR -->
                                <div class="row">                                        
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-default pull-left"><i class="fa fa-times fa-2x"></i> <br/>Cancelar</button>
                                        <button type="submit" class="btn btn-default pull-right"><i class="fa fa-save fa-2x"></i> <br/>Guardar</button>
                                    </div>
                                </div>
                                <!-- FIN BOTON GUARDAR -->
                            </div>
                            <!-- FIN  Código dentro cuadro blanco -->
                        </div>
                    </div><!-- /panel -->
                    <!-- FIN INFORMACION PERSONAL -->
                </form>
            </div>
        </div>

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <?php require_once './includes/footer.inc.php'; ?>

        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!--Bootstrap-->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src="js/js-nfconnection/jquery.validate.min.js"></script>
        <script src="js/js-nfconnection/custom_jquery_validate.js"></script>

        <script src="js/menu-active-class.js"></script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Bootstrap Switch -->
        <!--<script src='js/js-nfconnection/bootstrap-switch.js'></script>-->

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

    </body>
</html>