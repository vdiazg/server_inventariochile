<?php
error_reporting(error_reporting() & ~E_NOTICE);
session_start();
$IDUsuario = $_SESSION["datos_usuario_logueado"][0];
$NombreUsuario = $_SESSION["datos_usuario_logueado"][1];
$SegundoNombreUsuario = $_SESSION["datos_usuario_logueado"][2];
$ApellidoPaterno = $_SESSION["datos_usuario_logueado"][3];
$ApellidoMaterno = $_SESSION["datos_usuario_logueado"][4];
$RutUsuario = $_SESSION["datos_usuario_logueado"][5];
$FechaNacimiento = $_SESSION["datos_usuario_logueado"][6];
$GeneroUsuario = $_SESSION["datos_usuario_logueado"][7];
$RutaImgUsuario = $_SESSION["datos_usuario_logueado"][8];
$PerfilUsuario = $_SESSION["datos_usuario_logueado"][9];
$UsernameUsuario = $_SESSION["datos_usuario_logueado"][10];

if (!isset($IDUsuario)) {
    header('Location: login.php');
}
require_once './controlador/Db.class.php';
$db = new Db();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once './includes/head.inc.php'; ?>
    </head>
    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>	
        <div id="wrapper" class="preload">
            <?php require_once './includes/topnav.inc.php'; ?>
            <?php require_once './includes/menubar.inc.php'; ?>
            <div id="main-container">
                <div id="breadcrumb">
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="dashboard.php"> Home</a></li>
                        <li class="active">Gestión de Existencias</li>	 
                    </ul>
                </div><!-- breadcrumb -->
                <div class="padding-md">
                    <div class="panel panel-default table-responsive">
                        <div class="panel-heading">                            
                            <div class="row">
                                <div class="col-md-6"><i class="fa fa-list-alt"></i> GESTIÓN DE EXISTENCIAS <span class="badge badge-info"><?php //echo $db->single("SELECT COUNT(*) FROM Usuarios U,Perfiles P, Tag T, Login L WHERE T.tag_id = U.Tag_tag_id AND P.perfil_id = U.Perfiles_perfil_id AND L.Usuarios_user_id = U.user_id");  ?> 2</span></div>
                                
                            </div>
                        </div>
                        <div class="padding-md clearfix">
                            <table class="table table-striped" id="dataTable" style="font-size: 12px;"> 
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Referencia de Producto</th>
                                        <th class="text-center">EAN-13 o código de barra JAN</th>
                                        <th class="text-center">Nombre</th>
                                        <th class="text-center">Cantidad Total</th>
                                        <th class="text-center">Añadir existencias</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $SQL_DATA_SELECT = $db->query("SELECT * FROM Productos");

                                    foreach ($SQL_DATA_SELECT as $row):
                                        
                                        $PRODUCTO_ID_BD = $row["producto_id"];
                                        $PRODUCTO_IMG = $row["producto_ruta_img"];
                                        $PRODUCTO_NOMBRE = $row["producto_nombre"];
                                        $PRODUCTO_REF = $row["producto_cod_ref"];
                                        $PRODUCTO_EAN13 = $row["producto_ean13"];
                                        $PRODUCTO_DESCRIP = $row["producto_descrip_corta"];
                                        $PRODUCTO_Cantidad = $db->single("SELECT productcant_fisica FROM ProductCant WHERE Productos_producto_id = '".$PRODUCTO_ID_BD."'");
                                        $PRODUCTO_CATEGORIA = $row["ProductCategorias_productcategorias_id"];
                                        $NOMBRE_CATEGORIA = $db->single("SELECT productcategorias_nombre FROM ProductCategorias WHERE productcategorias_id = '".$PRODUCTO_CATEGORIA."'");
                                        $precio = $row['producto_precio'];
                                        
                                        /*
                                        $PRODUCT_CANT_ID = $row["productcant_id"];
                                        $PRODUCT_CANT_FISICA = $row["productcant_fisica"];
                                        $PRODUCT_CANT_USABLE = $row["productcant_usable"];
                                        $PRODUCT_CANT_REAL = $row["productcant_real"];
                                        $PRODUCT_FOREANEA_Product = $row["Productos_producto_id"];
                                        
                                        
                                        $PRODUCTO_IMG = $db->single("SELECT producto_ruta_img FROM Productos WHERE producto_id = '".$PRODUCT_FOREANEA_Product."'");
                                        $PRODUCTO_NOMBRE = $db->single("SELECT producto_nombre FROM Productos WHERE producto_id = '".$PRODUCT_FOREANEA_Product."'");
                                        $PRODUCTO_REF = $db->single("SELECT producto_cod_ref FROM Productos WHERE producto_id = '".$PRODUCT_FOREANEA_Product."'");
                                        $PRODUCTO_DESCRICION = $db->single("SELECT producto_descrip_corta FROM Productos WHERE producto_id = '".$PRODUCT_FOREANEA_Product."'");
                                        $PRODUCTO_CATEGORIA = $db->single("SELECT ProductCategorias_productcategorias_id FROM Productos WHERE producto_id = '".$PRODUCT_FOREANEA_Product."'");
                                        $NOMBRE_CATEGORIA = $db->single("SELECT productcategorias_nombre FROM ProductCategorias WHERE productcategorias_id = '".$PRODUCTO_CATEGORIA."'");
                                        $precio = $db->single("SELECT producto_precio FROM Productos WHERE producto_id = '".$PRODUCT_FOREANEA_Product."'");
                                        */
                                        
                                        echo '<tr>';
                                        echo '<td class="text-center">' . $PRODUCTO_ID_BD . '</td>';
                                        echo '<td class="text-center">' . $PRODUCTO_REF . '</td>';
                                        echo '<td class="text-center">' . $PRODUCTO_EAN13 . '</td>';
                                        echo '<td class="text-center">' . $PRODUCTO_NOMBRE . '</td>';
                                        echo '<td class="text-center">' . $PRODUCTO_Cantidad . '</td>';
                                        echo "<td class='text-center'>
                                            <div class='btn-group'>
                                                <a href='./VIEW.Existencias_Modificar.php?id=" . $PRODUCTO_ID_BD . "' class='btn btn-default dropdown-toggle btn-xs'><i class='fa fa-upload fa-lg'></i> Añadir Existencias </a>
                                                <a href='#' class='btn btn-default dropdown-toggle btn-xs' data-toggle='dropdown'><span class='caret'></span></a>
                                                <ul class='dropdown-menu slidedown'>
                                                    <li><a href='./modelo/MOD.Existencias_Eliminar.php?id=" . $PRODUCTO_ID_BD . "'><i class='fa fa-download fa-lg'></i> Reducir Existencias</a></li>
                                                </ul>
                                            </div>
                                        </td>";
                                        echo '</tr>';

                                    endforeach;
                                    ?>                                   
                                </tbody>
                            </table>
                        </div><!-- /.padding-md -->
                    </div><!-- /panel -->
                </div>
            </div><!-- /main-container -->
        </div><!-- /wrapper -->

        <a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
            <?php require_once './includes/footer.inc.php'; ?>
        <!-- Logout confirmation -->
        <?php require_once './includes/logout_confim.inc.php'; ?>
        <!-- Eliminar Tag confirmation -->
        <?php require_once './includes/eliminar_tag_confim.inc.php'; ?>


        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src='js/jquery.dataTables.min.js'></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#dataTable').dataTable({
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "asStripClasses": null
                });
                $('#refrescar_pagina').click(function () {
                    location.reload();
                });
            });
        </script>
        <script src="js/menu-active-class.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#a_btn_genera_pdf').on('click', function (e) {
                    window.open('./exportar_pdf/pdf_keys_tag.php', '_blank');
                });
                $('#a_btn_genera_excel').on('click', function (e) {
                    window.open('./exportar_excel/excel_keys_tags.php', '_blank');
                });
            });
        </script>
        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Bootstrap Switch -->
        <script src='js/js-nfconnection/bootstrap-switch.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Endless -->
        <script src="js/endless/endless.js"></script>

    </body>
</html>